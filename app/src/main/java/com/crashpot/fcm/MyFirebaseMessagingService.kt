package com.crashpot.fcm

import android.util.Log
import com.crashpot.CrashPotApplication
import com.crashpot.activity.ChatActivity
import com.crashpot.util.NotificationUtils
import com.crashpot.util.PREF_FCM_TOKEN
import com.crashpot.util.Utility.Companion.showLog
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class MyFirebaseMessagingService : FirebaseMessagingService() {

    companion object {
        const val TAG = "FCMService"
    }

    override fun onNewToken(refreshedToken: String) {
        super.onNewToken(refreshedToken)
        showLog("newToken", refreshedToken)
        CrashPotApplication.instance.preferenceData?.setValue(PREF_FCM_TOKEN, refreshedToken)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        showLog(TAG, "From :: " + remoteMessage.from!!)
        showLog(TAG, "Message Data Body :: " + remoteMessage.data)

        if (remoteMessage.data.isNotEmpty()) {
            if (!ChatActivity.isChatScreenOpen) {
                NotificationUtils.instance.showNotification(
                    this,
                    remoteMessage.data["type"].toString() == "2",
                    remoteMessage.data["imageurl"].toString(),
                    remoteMessage.data["title"].toString(),
                    remoteMessage.data["message"].toString()
                )
            }
        }

    }
}
