package com.crashpot.util.mpchartlib.interfaces.dataprovider;


import com.crashpot.util.mpchartlib.components.YAxis;
import com.crashpot.util.mpchartlib.data.LineData;

public interface LineDataProvider extends BarLineScatterCandleBubbleDataProvider {

    LineData getLineData();

    YAxis getAxis(YAxis.AxisDependency dependency);
}
