package com.crashpot.util.mpchartlib.interfaces.dataprovider;


import com.crashpot.util.mpchartlib.data.ScatterData;

public interface ScatterDataProvider extends BarLineScatterCandleBubbleDataProvider {

    ScatterData getScatterData();
}
