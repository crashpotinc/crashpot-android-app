package com.crashpot.util.mpchartlib.interfaces.dataprovider;


import com.crashpot.util.mpchartlib.data.BubbleData;

public interface BubbleDataProvider extends BarLineScatterCandleBubbleDataProvider {

    BubbleData getBubbleData();
}
