package com.crashpot.util

import android.text.InputFilter
import android.text.Spanned
import java.util.regex.Matcher
import java.util.regex.Pattern


class DecimalDigitsInputFilter(digitsBeforeDecimal: Int, digitsAfterDecimal: Int) : InputFilter {
    var pattern: Pattern = Pattern.compile("(([1-9]{1}[0-9]{0," + (digitsBeforeDecimal - 1) + "})?||[0]{1})((\\.[0-9]{0," + digitsAfterDecimal + "})?)||(\\.)?")

    override fun filter(source: CharSequence, sourceStart: Int, sourceEnd: Int, destination: Spanned, destinationStart: Int, destinationEnd: Int): CharSequence? { // Remove the string out of destination that is to be replaced.
        var newString = destination.toString().substring(0, destinationStart) + destination.toString().substring(destinationEnd, destination.toString().length)
        // Add the new string in.
        newString = newString.substring(0, destinationStart) + source.toString() + newString.substring(destinationStart, newString.length)
        // Now check if the new string is valid.
        val matcher: Matcher = pattern.matcher(newString)
        return if (matcher.matches()) { // Returning null indicates that the input is valid.
            null
        } else ""
        // Returning the empty string indicates the input is invalid.
    }

}