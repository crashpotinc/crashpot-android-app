package com.crashpot.util

import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.view.View

class BubbleAnimationUtils {

    companion object {
        private lateinit var objectAnimatorWheelSpin: ObjectAnimator
        private lateinit var objectAnimatorWatchAndEarn: ObjectAnimator
        private lateinit var objectAnimatorDoubleReward: ObjectAnimator

        //        Wheel Spin
        fun startWheelSpinBubbleAnimation(view: View) {
            objectAnimatorWheelSpin = ObjectAnimator.ofPropertyValuesHolder(
                view,
                PropertyValuesHolder.ofFloat("scaleX", 0.9f),
                PropertyValuesHolder.ofFloat("scaleY", 0.9f)
            )
            objectAnimatorWheelSpin.duration = 800
            objectAnimatorWheelSpin.repeatMode = ValueAnimator.REVERSE
            objectAnimatorWheelSpin.repeatCount = ValueAnimator.INFINITE
            objectAnimatorWheelSpin.start()
        }

        fun stopWheelSpinBubbleAnimation() {
            if (::objectAnimatorWheelSpin.isInitialized && objectAnimatorWheelSpin != null)
                objectAnimatorWheelSpin.cancel()
        }

        //        Watch And Earn
        fun startWatchAndEarnBubbleAnimation(view: View) {
            objectAnimatorWatchAndEarn = ObjectAnimator.ofPropertyValuesHolder(
                view,
                PropertyValuesHolder.ofFloat("scaleX", 0.9f),
                PropertyValuesHolder.ofFloat("scaleY", 0.9f)
            )
            objectAnimatorWatchAndEarn.duration = 800
            objectAnimatorWatchAndEarn.repeatMode = ValueAnimator.REVERSE
            objectAnimatorWatchAndEarn.repeatCount = ValueAnimator.INFINITE
            objectAnimatorWatchAndEarn.start()
        }

        fun stopWatchAndEarnBubbleAnimation() {
            if (::objectAnimatorWatchAndEarn.isInitialized && objectAnimatorWatchAndEarn != null)
                objectAnimatorWatchAndEarn.cancel()
        }

        //        2X Reward
        fun startDoubleRewardBubbleAnimation(view: View) {
            objectAnimatorDoubleReward = ObjectAnimator.ofPropertyValuesHolder(
                view,
                PropertyValuesHolder.ofFloat("scaleX", 0.9f),
                PropertyValuesHolder.ofFloat("scaleY", 0.9f)
            )
            objectAnimatorDoubleReward.duration = 800
            objectAnimatorDoubleReward.repeatMode = ValueAnimator.REVERSE
            objectAnimatorDoubleReward.repeatCount = ValueAnimator.INFINITE
            objectAnimatorDoubleReward.start()
        }

        fun stopDoubleRewardBubbleAnimation() {
            if (::objectAnimatorDoubleReward.isInitialized && objectAnimatorDoubleReward != null)
                objectAnimatorDoubleReward.cancel()
        }
    }
}