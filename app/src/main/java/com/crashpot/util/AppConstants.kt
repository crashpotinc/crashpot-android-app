package com.crashpot.util

import com.crashpot.BuildConfig

// others
const val SPLASH_TIME_OUT: Long = 2500
const val GOOGLE_SIGN_IN_CODE: Int = 0
const val DEVICE_TYPE: Int = 1
const val INTERSTITIAL_AD_MAX_VALUE: Int = 5
const val REVIEW_DIALOG_MAX_VALUE: Int = 10 // 25
const val SPIN_WHEEL_DIALOG_MAX_VALUE: Int = 10
const val DATE_FORMAT_HH_MM: String = "HH:mm"

const val APP_PLAY_STORE_URL: String = "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID

const val MILLISECONDS_10_sec: Int = 10 * 1000 // 3 sec
const val MILLISECONDS_3_HOURS: Int = ((3 * 60) * 60) * 1000
const val MILLISECONDS_6_HOURS: Int = ((6 * 60) * 60) * 1000
const val MILLISECONDS_24_HOURS: Int = ((24 * 60) * 60) * 1000

//const val MILLISECONDS_10_sec: Int = 10 * 1000 // 10 sec
//const val MILLISECONDS_3_HOURS: Int = 30 * 1000 // 1 min
//const val MILLISECONDS_6_HOURS: Int = 3 * 1000 // 3 sec
//const val MILLISECONDS_24_HOURS: Int = 30 * 60 * 1000 // 30 min

//const val MILLISECONDS_24_HOURS: Int = ((1 * 60) * 60) * 60 * 1000 // 1 hour

// Shared preferences
const val PREF_IS_ACCEPT_TOS = "PREF_IS_ACCEPT_TOS"
const val PREF_STRATEGY_VALUE = "pref_strategy_value"
const val PREF_MY_BET_VALUE = "pref_my_bet_value"
const val PREF_FCM_TOKEN = "pref_fcm_token"
const val PREF_IMEI = "pref_imei"
const val PREF_USER_DETAIL = "pref_user_detail"
const val PREF_DASHBOARD_USER_DETAIL = "pref_dashboard_user_detail"
const val PREF_IS_GUEST_USER = "pref_is_guest_user"
const val PREF_IS_USER_LOGGED_IN = "PREF_IS_USER_LOGGED_IN"
const val PREF_TIMER_AD_MOB = "pref_timer_ad_mob"
const val PREF_TIMER_WHEEL_SPIN = "pref_timer_wheel_spin"
const val PREF_TIMER_APP_OPEN = "pref_timer_app_open"
const val PREF_INTERSTITIAL_AD_COUNT = "pref_interstitial_ad_count"
const val PREF_REVIEW_DIALOG_COUNT = "pref_review_dialog_count"
const val PREF_SPIN_WHEEL_COUNT = "pref_spin_wheel_count"
const val PREF_USER_LEVEL = "pref_user_level"
const val PREF_DONT_SHOW_REVIEW_DIALOG = "pref_dont_show_review_dialog"

// Extras
const val EXTRAS_TOS_NAME = "extras_tos_name"
const val EXTRAS_USER_ID = "extras_user_id"

// Arguments

// Socket Params
const val NORMAL_CLOSURE_STATUS = 1000

//const val SOCKET_IO_URL: String = "http://68.183.83.34:3002/" // live
//const val SOCKET_IO_URL: String = "http://192.168.1.240:3002/" // local
//const val SOCKET_IO_URL: String = "http://134.122.24.222:3002/" // Staging

const val SOCKET_IO_URL: String = "http://161.35.100.65:3002/"// Live latest

//SOCKET TYPE
const val SOCKET_TYPE_GRAPH_RUNNING: Int = 1
const val SOCKET_TYPE_CRASH_OUT: Int = 2
const val SOCKET_TYPE_COUNT_DOWN: Int = 3

// SOCKET PARAMS
const val SOCKET_FINAL_VALUE = "finalValue"
const val SOCKET_X_VALUE = "xValue"
const val SOCKET_DISPLAY_VALUE = "displayValue"
const val SOCKET_TYPE = "type"

//Api Params
//const val BASE_URL: String = "http://209.124.73.130/crashpot/public/api/" // live
//const val BASE_URL: String = "http://192.168.1.240/crashpot/public/api/" // local
//const val BASE_URL: String = "http://crashpot.devhostserver.com/public/api/" // Staging

const val BASE_URL: String = "http://161.35.100.65/crashpot/public/api/" // Live latest

const val STATUS = "Status"
const val MESSAGE = "Message"
const val MESSAGE_SMALL = "message"
const val RESPONSE_CODE_SUCCESS_200 = 200
const val RESPONSE_CODE_SUCCESS_201 = 201
const val RESPONSE_CODE_401 = 401
const val RESPONSE_CODE_402 = 402

//const val HEADER_AUTHORIZATION = "Authorization"
//const val HEADER_ACCEPT = "Accept"
//const val HEADER_ACCEPT_VALUE = "application/json"

// Social media Type
const val LOGIN_AS_GUEST: String = "0"
const val LOGIN_WITH_FACEBOOK: String = "1"
const val LOGIN_WITH_GOOGLE: String = "2"

// Add coins API Game Type
const val GAME_TYPE_REWARD_POINTS: String = "1"
const val GAME_TYPE_GOOGLE_ADS: String = "2"
const val GAME_TYPE_RAIN_ON: String = "3"
const val GAME_TYPE_SPINNER_WHEEL: String = "4"
const val GAME_TYPE_GAME_WIN_OR_LOSS: String = "5"
const val GAME_TYPE_IN_APP: String = "8"

// Add coins API status
const val COINS_STATUS_LOSS_OR_DEDUCT: String = "0"
const val COINS_STATUS_WIN_OR_ADD: String = "1"

// Add coins is_xp_or_coin
const val ADD_COINS_COIN: String = "0"
const val ADD_COINS_XP: String = "1"

// Reward dialog from
const val FROM_SPINNER_WHEEL: String = "1"
const val FROM_GOOGLE_ADS: String = "2"
const val FROM_IN_APP: String = "3"

// Ranking
const val RANKING_BY_PROFIT: String = "1"
const val RANKING_BY_LEVEL: String = "2"

// Message Type
const val MESSAGE_TYPE_NORMAL: String = "1"
const val MESSAGE_TYPE_BROADCAST: String = "2"

// Report Type
const val REPORT_TYPE_MUTE_AND_REPORT: String = "1"
const val REPORT_TYPE_MUTE_ONLY: String = "2"
const val REPORT_TYPE_REPORT_ONLY: String = "3"

//Notification TYPE
const val NOTIFICATION_TYPE_CHAT: Int = 0
const val NOTIFICATION_TYPE_REWARDED_VIDEO: Int = 1
const val NOTIFICATION_TYPE_SPIN_WHEEL: Int = 2
const val NOTIFICATION_TYPE_ONE_DAY: Int = 3

// In App Purchase Product ID
const val PURCHASE_ID_BEGINNER_PACK: String = "com.crashpot_0.99"
const val PURCHASE_ID_VALUE_PACK: String = "com.crashpot_1.99"
const val PURCHASE_ID_DELUX_PACK: String = "com.crashpot_4.99"

class AppConstants {
}