package com.crashpot.util

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.crashpot.BuildConfig
import com.crashpot.CrashPotApplication
import com.crashpot.R
import com.crashpot.model.ClsGuestRandomNumberResponse
import com.crashpot.model.ClsProfileDashboardResponse
import com.crashpot.receiver.AdMobBroadcastReceiver
import com.crashpot.receiver.SpinWheelBroadcastReceiver
import com.facebook.CallbackManager
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import java.text.DateFormat
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*


class Utility {
    companion object {

        // Facebook
        private lateinit var callbackManager: CallbackManager
        private lateinit var shareDialog: ShareDialog

        /**
         * get color from resource
         */
        fun getColor(context: Context?, colorOrange: Int): Int {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ContextCompat.getColor(context!!, colorOrange)
            } else context!!.resources.getColor(colorOrange)
        }

        @SuppressLint("ObsoleteSdkInt")
        fun getDrawable(context: Context?, id: Int): Drawable {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ContextCompat.getDrawable(context!!, id)!!
            } else {
                context!!.resources.getDrawable(id)
            }
        }

        /**
         * Show toast of  message
         */
        fun showToast(mContext: Context?, message: String?) {
            val toast = Toast.makeText(mContext, message, Toast.LENGTH_SHORT)
            //            val view = toast.view
            //            MyDrawableCompat.setColorFilter(view.background, getColor(mContext, R.color.colorBlack))
            //            val text = view.findViewById<TextView>(R.id.message)
            //            text.setTextColor(getColor(mContext, R.color.colorWhite))
            toast.show()
        }

        /**
         * Show log of message
         */
        fun showLog(tag: String, message: String) {
            if (BuildConfig.DEBUG)
                Log.e(tag, message)
        }

        /**
         * Check Internet is connected or not
         */
        fun isNetworkConnected(context: Context?): Boolean {
            val connectivityManager: ConnectivityManager =
                context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            if (connectivityManager != null) {
                if (Build.VERSION.SDK_INT < 23) {
                    val ni = connectivityManager.activeNetworkInfo
                    if (ni != null) {
                        return ni.isConnected && (ni.type == ConnectivityManager.TYPE_WIFI
                                || ni.type == ConnectivityManager.TYPE_MOBILE)
                    }
                } else {
                    val network: Network? = connectivityManager.activeNetwork
                    if (network != null) {
                        val networkCapabilities: NetworkCapabilities? =
                            connectivityManager.getNetworkCapabilities(network)

                        return networkCapabilities!!.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                                || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                    }
                }
            }
            return false
        }

        /**
         * Share string Message To Other Apps
         */
        fun shareMessageToOtherApps(mContext: Context, message: String, extraMessage: String) {
            val sharingIntent = Intent(Intent.ACTION_SEND)
            sharingIntent.type = "text/plain"
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, extraMessage)
            sharingIntent.putExtra(Intent.EXTRA_TEXT, message)
            mContext.startActivity(Intent.createChooser(sharingIntent, "Share via"))
        }

        /**
         * open App In Play Store
         */
        fun openAppInPlayStore(mContext: Context) {
            val intent: Intent = Intent(android.content.Intent.ACTION_VIEW);
            intent.data = Uri.parse(APP_PLAY_STORE_URL);
            mContext.startActivity(intent);
        }

        /**
         * Hide keyboard
         */
        fun hideKeyboard(activity: Activity?) {
            val imm =
                activity!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            var view = activity.currentFocus
            if (view == null) view = View(activity)
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

        /**
         * Show Keyboard
         */
        fun showKeyboard(context: Context?, view: View?) {
            if (view!!.requestFocus()) {
                val imm =
                    context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
            }
        }

        /**
         * Get IMEI Number
         */
        fun getImeiNumber(): String? {
            return CrashPotApplication.instance.preferenceData?.getValueFromKey(PREF_IMEI)
        }

        /**
         * Get Fcm Token
         */
        fun getFcmToken(): String? {
            return CrashPotApplication.instance.preferenceData?.getValueFromKey(PREF_FCM_TOKEN)
        }

        /**
         * Get My Bet Amount
         */
        fun getMyBetAmount(): String? {
            return CrashPotApplication.instance.preferenceData?.getValueFromKey(PREF_MY_BET_VALUE)
        }

        /**
         * Get Interstitial Ad Count
         */
        fun getUserLevel(): Int {
            return CrashPotApplication.instance.preferenceData?.getValueIntFromKey(PREF_USER_LEVEL) ?: 1
        }

        /**
         * Get Interstitial Ad Count
         */
        fun getInterstitialAdCount(): Int {
            return CrashPotApplication.instance.preferenceData?.getValueIntFromKey(PREF_INTERSTITIAL_AD_COUNT) ?: 0
        }

        /**
         * Get Review Dialog Count
         */
        fun getReviewDialogCount(): Int {
            return CrashPotApplication.instance.preferenceData?.getValueIntFromKey(PREF_REVIEW_DIALOG_COUNT) ?: 0
        }

        /**
         * Get Spin Wheel Count
         */
        fun getSpinWheelCount(): Int {
            return CrashPotApplication.instance.preferenceData?.getValueIntFromKey(PREF_SPIN_WHEEL_COUNT) ?: 0
        }

        /**
         * Get User Details
         */
        fun getUserDetails(): ClsGuestRandomNumberResponse.Data? {
            val userDetails: String? =
                CrashPotApplication.instance.preferenceData?.getValueFromKey(PREF_USER_DETAIL)
            return Gson().fromJson(userDetails, ClsGuestRandomNumberResponse.Data::class.java)
        }

//        /**
//         * Get Access Token
//         */
//        fun getAccessToken(): String? {
//            return "Bearer " + getUserDetails()?.success?.token
//        }

        /**
         * Get User Details
         */
        fun getDashboardUserDetails(): ClsProfileDashboardResponse.Data? {
            val userDetails: String? = CrashPotApplication.instance.preferenceData?.getValueFromKey(PREF_DASHBOARD_USER_DETAIL)
            return Gson().fromJson(userDetails, ClsProfileDashboardResponse.Data::class.java)
        }

        /**
         * Change string coin format ex: 2,400,658,987
         */
        fun changeCoinsFormat(coins: String): String {
            if (!TextUtils.isEmpty(coins)) {
                val formatter: NumberFormat = DecimalFormat("#,###")
                val myNumber: Double? = coins.toDouble()
                return formatter.format(myNumber)
            }
            return "0"
        }

        /**
         * share message On Facebook As Post
         */
        fun shareOnFacebookAsPost(context: Activity?, quote: String) {
            callbackManager = CallbackManager.Factory.create();
            shareDialog = ShareDialog(context)
            val content: ShareLinkContent = ShareLinkContent.Builder()
                .setQuote("$quote\nDownload now from Play Store !")
                .setContentUrl(Uri.parse(APP_PLAY_STORE_URL))
                .build()
            shareDialog.show(content)
        }

        /**
         * change String Date Format for chat
         */
        fun changeStringDateFormat(dateString: String, newFormat: String): String? {
            val originalFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            originalFormat.timeZone = TimeZone.getTimeZone("UTC")
            val targetFormat: DateFormat = SimpleDateFormat(newFormat, Locale.getDefault())
            targetFormat.timeZone = TimeZone.getDefault()
            var date: Date? = null;
            try {
                date = originalFormat.parse(dateString);
            } catch (e: java.lang.Exception) {
                e.printStackTrace();
            }
            var formattedDate = targetFormat.format(date)
            return formattedDate
        }

        /**
         * Set Image in circle Image view
         */
        fun setCircularImage(context: Context, view: CircleImageView, url: String) {
            try {
                Glide.with(context).load(url).apply(
                    RequestOptions.circleCropTransform()
                        .placeholder(R.drawable.img_profile_pic_placeholder)
                        .error(R.drawable.img_profile_pic_placeholder)
                ).into(view)
            } catch (e: Exception) {
                showLog("Error:", e.printStackTrace().toString())
            }
        }

        /**
         * Set Image in AppCompat Image View
         */
        fun setSimpleImage(context: Context, view: AppCompatImageView, url: String) {
            try {
                Glide.with(context).load(url).apply(
                    RequestOptions.centerInsideTransform()
                        .placeholder(R.drawable.img_profile_pic_placeholder)
                        .error(R.drawable.img_profile_pic_placeholder)
                ).into(view)
            } catch (e: Exception) {
                showLog("Error:", e.printStackTrace().toString())
            }
        }


        fun setSpinWheelAlarm(context: Context, alarmTime: Long) {
            val myIntent = Intent(context, SpinWheelBroadcastReceiver::class.java)
            val pendingIntent: PendingIntent = PendingIntent.getBroadcast(context, 0, myIntent, 0)
            val alarmManager: AlarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager.setRepeating(AlarmManager.RTC, alarmTime, AlarmManager.INTERVAL_DAY, pendingIntent)
        }

        fun setRewardedVideoAlarm(context: Context, alarmTime: Long) {
            val myIntent = Intent(context, AdMobBroadcastReceiver::class.java)
            val pendingIntent: PendingIntent = PendingIntent.getBroadcast(context, 0, myIntent, 0)
            val alarmManager: AlarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager.setRepeating(AlarmManager.RTC, alarmTime, AlarmManager.INTERVAL_DAY, pendingIntent)
        }
    }
}