package com.crashpot.interfaces

/**
 * returns empty call back when view clicked
 */
interface OnCallBackChat {
    fun onCallBackReturn(name: String, userId: String)
}