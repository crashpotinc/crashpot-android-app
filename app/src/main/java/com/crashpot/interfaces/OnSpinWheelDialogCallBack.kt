package com.crashpot.interfaces

/**
 * returns empty call back when view clicked
 */
interface OnSpinWheelDialogCallBack {
    fun onEmptyCallBak()

    fun onCallBakeWithSpinWheelValue(amount: String, isXpOrCoin: String)
}