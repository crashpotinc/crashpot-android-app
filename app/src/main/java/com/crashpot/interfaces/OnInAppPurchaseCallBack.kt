package com.crashpot.interfaces

interface OnInAppPurchaseCallBack {

    fun onBeginnerPackClick(billingId: String)

    fun onValuePackClick(billingId: String)

    fun onDeluxPackClick(billingId: String)

    fun onCallBackReturn(stringValue: String)
}