package com.crashpot.interfaces

interface OnReviewDialogCallBack {

    fun onReviewNowClick(stringValue: String)

    fun onMayBeLaterClick(stringValue: String)

    fun onDontAskAgainClick(stringValue: String)

    fun onCallBackReturn(stringValue: String)
}