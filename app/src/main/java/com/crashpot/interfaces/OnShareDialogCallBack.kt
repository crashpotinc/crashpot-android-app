package com.crashpot.interfaces

/**
 * returns empty call back when view clicked
 */
interface OnShareDialogCallBack {
    fun onEmptyCallBak()

    fun onCallBakeWithString(amount: String, isXpOrCoin: String)
}