package com.crashpot.interfaces

/**
 * returns empty call back when view clicked
 */
interface OnShareAndRewardDialogCallBack {
    fun onEmptyCallBak()

    fun onCallBakeWithOneString(winAmount: String)

    fun onCallBakeWithString(amount: String, isXpOrCoin: String)
}