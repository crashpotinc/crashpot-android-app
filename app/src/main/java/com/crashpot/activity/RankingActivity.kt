package com.crashpot.activity

import android.widget.FrameLayout
import androidx.viewpager.widget.ViewPager
import com.crashpot.R
import com.crashpot.adapter.RankingPagerAdapter
import com.crashpot.base.BaseActivity
import com.crashpot.fragment.RankingByLevelFragment
import com.crashpot.fragment.RankingByProfitFragment
import com.crashpot.util.Utility.Companion.getDashboardUserDetails
import kotlinx.android.synthetic.main.activity_ranking.*

class RankingActivity : BaseActivity() {

    private var indicatorWidth = 0

    override fun getLayoutResId(): Int = R.layout.activity_ranking

    override fun initViews() {
        txtRankingValue.text = getDashboardUserDetails()?.RankingByLevelPostion.toString()

        setUpTab()
    }

    private fun setUpTab() {
        val rankingPagerAdapter = RankingPagerAdapter(supportFragmentManager, 0)
        rankingPagerAdapter.addFragment(RankingByLevelFragment(), "by Levels")
        rankingPagerAdapter.addFragment(RankingByProfitFragment(), "by Profits")
        pagerRanking.adapter = rankingPagerAdapter
        tabRanking.setupWithViewPager(pagerRanking)
    }

    override fun setListeners() {
        tabRanking.post {
            indicatorWidth = tabRanking.getWidth() / tabRanking.getTabCount()
            val indicatorParams: FrameLayout.LayoutParams = indicator.layoutParams as FrameLayout.LayoutParams
            indicatorParams.width = indicatorWidth
            indicator.layoutParams = indicatorParams
        }

        pagerRanking.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                val params = indicator.layoutParams as FrameLayout.LayoutParams
                val translationOffset: Float = (positionOffset + position) * indicatorWidth
                params.leftMargin = translationOffset.toInt()
                indicator.setLayoutParams(params)
            }

            override fun onPageSelected(position: Int) {
                if (position == 0) {
                    txtRankingValue.text = getDashboardUserDetails()?.RankingByLevelPostion.toString()
                } else {
                    txtRankingValue.text = getDashboardUserDetails()?.rankingByProfitPosition.toString()
                }
            }
        })

        imgBack.setOnClickListener { onBackPressed() }
    }
}
