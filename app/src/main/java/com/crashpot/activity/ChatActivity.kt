package com.crashpot.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import com.crashpot.CrashPotApplication
import com.crashpot.R
import com.crashpot.adapter.ChatMessageListAdapter
import com.crashpot.base.BaseActivity
import com.crashpot.dialog.AlreadyLoggedInDialog
import com.crashpot.dialog.LoadDataDialog
import com.crashpot.dialog.RainOnDialog
import com.crashpot.dialog.ReportUserDialog
import com.crashpot.interfaces.OnCallBack
import com.crashpot.interfaces.OnCallBackChat
import com.crashpot.model.*
import com.crashpot.network.ApiCallMethods
import com.crashpot.network.OnApiCallCompleted
import com.crashpot.util.*
import com.crashpot.util.Utility.Companion.changeCoinsFormat
import com.crashpot.util.Utility.Companion.getDashboardUserDetails
import com.crashpot.util.Utility.Companion.getMyBetAmount
import com.crashpot.util.Utility.Companion.getUserDetails
import com.crashpot.util.Utility.Companion.showLog
import com.crashpot.util.Utility.Companion.showToast
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.crashpot.interfaces.ItemClickCallback
import com.vanniktech.emoji.EmojiPopup
import io.socket.client.IO
import io.socket.client.Socket
import kotlinx.android.synthetic.main.activity_chat.*
import org.json.JSONException
import org.json.JSONObject
import java.net.URISyntaxException

class ChatActivity : BaseActivity(), ItemClickCallback<ClsGetChatHistoryResponse.Data>, OnCallBackChat {

    private var chatMessageList: ArrayList<ClsGetChatHistoryResponse.Data> = ArrayList()
    private lateinit var chatMessageListAdapter: ChatMessageListAdapter
    private var isUserJoin = false

    companion object {
        var isChatScreenOpen: Boolean = false
    }

    var emojiPopup: EmojiPopup? = null
    var pageNumber: Int = 0

    var reportUserDialog: ReportUserDialog? = null
    var rainOnDialog: RainOnDialog? = null
    var tagUserList: JsonArray = JsonArray()
    var pushId: String = ""

    private var userCoins: String = ""
    private var userXps: String = ""
    private var isUserWonGame: Boolean = false

    private lateinit var socket: Socket

    // Login
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var callbackManager: CallbackManager

    override fun getLayoutResId(): Int = R.layout.activity_chat

    override fun initViews() {
        emojiPopup = EmojiPopup.Builder.fromRootView(rootView).build(edtMessage)
        connectSocketIO()
        getChatHistory(getDashboardUserDetails()?.userID.toString(), "0", pageNumber)
        setAdapter(chatMessageList)

        manageChatRestrictionUi() //TODO un-comment this
    }

    override fun onResume() {
        super.onResume()
        isChatScreenOpen = true
    }

    override fun onDestroy() {
        super.onDestroy()
        isChatScreenOpen = false
    }

    private fun manageChatRestrictionUi() {
        if (CrashPotApplication.instance.preferenceData?.getValueBooleanFromKey(PREF_IS_USER_LOGGED_IN)!!) {

            if (getDashboardUserDetails()?.rankingByLevel?.toInt() ?: 0 < 2) {
                clRestrictionForChatLayer.visibility = View.VISIBLE
                txtLockMessageTwo.visibility = View.VISIBLE
                txtLockMessageOne.text = getString(R.string.chat_is_available_only_after_you_reach)
                groupLogIn.visibility = View.GONE
                clChat.visibility = View.GONE
            } else {
                if (getDashboardUserDetails()?.isBlock == 1) {
                    txtLockMessageOne.text = getString(R.string.you_have_been_banned_from_chat_for_violation_the_rules)
                    clRestrictionForChatLayer.visibility = View.VISIBLE
                    txtLockMessageTwo.visibility = View.GONE
                    groupLogIn.visibility = View.GONE
                } else {
                    clRestrictionForChatLayer.visibility = View.GONE
                    clChat.visibility = View.VISIBLE
                }
            }

        } else {
            txtLockMessageOne.text = getString(R.string.chat_is_available_only_for_logged_in_users)
            txtLockMessageTwo.visibility = View.GONE
            clRestrictionForChatLayer.visibility = View.VISIBLE
            groupLogIn.visibility = View.VISIBLE
            clChat.visibility = View.GONE
            loginWithFacebook()
            loginWithGoogleSettings()
        }
    }

    private fun setAdapter(chatMessageList: ArrayList<ClsGetChatHistoryResponse.Data>) {
        if (::chatMessageListAdapter.isInitialized) {
            chatMessageListAdapter.setList(chatMessageList)
            rvChat.smoothScrollToPosition(chatMessageList.size)
        } else {
            chatMessageListAdapter = ChatMessageListAdapter(chatMessageList, this, this)
            rvChat.adapter = chatMessageListAdapter
            rvChat.smoothScrollToPosition(chatMessageListAdapter.itemCount)
        }
    }

    private fun addMessageInAdapterList(message: ClsGetChatHistoryResponse.Data) {
        runOnUiThread {
            if (chatMessageList.isNotEmpty() && ::chatMessageListAdapter.isInitialized) {
                chatMessageListAdapter.updateData(message)
                rvChat.smoothScrollToPosition(chatMessageListAdapter.itemCount)
            }
        }
    }

    override fun setListeners() {
        imgBack.setOnClickListener { onBackPressed() }

        imgSend.setOnClickListener {
            if (!TextUtils.isEmpty(edtMessage.text.toString().trim())) {
                sendMessage(edtMessage.text.toString().trim(), MESSAGE_TYPE_NORMAL)
                edtMessage.setText("")
            }
        }

        txtChatLoginWithGoogle.setOnClickListener {
            val signInIntent = mGoogleSignInClient.signInIntent
            startActivityForResult(signInIntent, GOOGLE_SIGN_IN_CODE)
        }

        txtChatLoginWithFacebook.setOnClickListener {
            LoginManager.getInstance().logInWithReadPermissions(this, listOf("email"))
        }

        imgSmile.setOnCheckedChangeListener { p0, isSelected -> emojiPopup?.toggle() }

        pullChatHistory.setOnRefreshListener {
            getChatHistory(getDashboardUserDetails()?.userID.toString(), "0", pageNumber)
        }

        edtMessage.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(text: Editable?) {
                if (TextUtils.isEmpty(edtMessage.text.toString().trim())) {
                    tagUserList = JsonArray()
                    pushId = ""
                }
            }

            override fun beforeTextChanged(text: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
    }

    private fun sendMessage(message: String, messageType: String) {
        try {
            val msgJson = JsonObject()
            msgJson.addProperty("message", message)
            msgJson.addProperty("userId", getDashboardUserDetails()?.userID)
            msgJson.addProperty("name", getDashboardUserDetails()?.userName)
            msgJson.addProperty("email", getDashboardUserDetails()?.email)
            msgJson.addProperty("userImage", getDashboardUserDetails()?.userImage)
            msgJson.addProperty("messageType", messageType)
            msgJson.addProperty("rankingByLevel", getDashboardUserDetails()?.rankingByLevel)
            msgJson.addProperty("tagUserList", tagUserList.toString())
            msgJson.addProperty("pushId", pushId)

            socket.emit("newMessage", msgJson)
            showLog("@@@ messageSend", msgJson.toString())
            pushId = ""
        } catch (e: URISyntaxException) {
            showLog("@@@ send ", e.printStackTrace().toString())
            e.printStackTrace()
        }
    }

    override fun onItemClick(view: View, selectedItem: ClsGetChatHistoryResponse.Data, position: Int) {
        edtMessage.setText("")
        when (view.id) {
            R.id.imgRainOnUser -> {
                showRainOnDialog(selectedItem)
            }
            R.id.imgReportUser -> {
                showReportUserDialog(selectedItem)
            }
            R.id.txtTagUser -> {
                pushId = selectedItem.userId.toString()
                setTagUser(selectedItem.name.toString())
            }
            R.id.txtUserName, R.id.imgProfile -> {
                startActivity(
                    Intent(this@ChatActivity, ProfileActivity::class.java)
                        .putExtra(EXTRAS_USER_ID, selectedItem.userId)
                )
            }
        }
    }

    private fun setTagUser(username: String) {
        if (username != getDashboardUserDetails()?.userName) {
            tagUserList.add(username)

            val taagUser = "@${username} "
            edtMessage.setText(taagUser)
            edtMessage.requestFocus()
            edtMessage.setSelection(taagUser.length)
            showKeyboard(edtMessage)
        }
    }

    override fun onCallBackReturn(username: String, userId: String) {
        setTagUser(username)
        pushId = userId
    }

    /**
     * Show Report User Dialog
     */
    private fun showReportUserDialog(selectedItem: ClsGetChatHistoryResponse.Data) {
        reportUserDialog = ReportUserDialog(this@ChatActivity, selectedItem,
            object : OnCallBack {
                override fun onCallBackReturn(stringValue: String) {
                    if (stringValue == REPORT_TYPE_MUTE_AND_REPORT || stringValue == REPORT_TYPE_REPORT_ONLY) {
                        reportUser(getDashboardUserDetails()?.userID.toString(), selectedItem.userId.toString(), selectedItem.message.toString(), stringValue)
                    }
                }
            })
        reportUserDialog?.setCancelable(false)
        reportUserDialog?.show()
    }

    /**
     *  show Rain On Dialog
     */
    private fun showRainOnDialog(selectedItem: ClsGetChatHistoryResponse.Data) {
        rainOnDialog = RainOnDialog(this@ChatActivity, selectedItem,
            object : OnCallBack {
                override fun onCallBackReturn(stringValue: String) {
                    addUserCoins(
                        selectedItem.userId.toString(),
                        selectedItem.name.toString(),
                        stringValue,
                        GAME_TYPE_RAIN_ON,
                        COINS_STATUS_WIN_OR_ADD,
                        getDashboardUserDetails()?.userID.toString(),
                        ADD_COINS_COIN
                    )
                }
            })
        rainOnDialog?.setCancelable(true)
        rainOnDialog?.show()
    }

    //  SOCKET IO Connection related
    private fun connectSocketIO() {
        socket = IO.socket(SOCKET_IO_URL);

        socket.on(Socket.EVENT_CONNECT) {
            showLog("@@@ EVENT_CONNECT", "connected to server")
        }

        socket.on("crashStart") { args ->
            val data: JSONObject = args[0] as JSONObject
            receiveSocketGraphDetails(data.toString())
        }

        socket.on("newMessage") { args ->
            val data: JSONObject = args[0] as JSONObject
            val gson = Gson()
            val clsGetChatHistoryResponse: ClsGetChatHistoryResponse.Data = gson.fromJson(data.toString(), object : TypeToken<ClsGetChatHistoryResponse.Data?>() {}.type)
            showLog("@@@ newMessage rv", "message :: ${clsGetChatHistoryResponse.message}")
            showLog("@@@ newMessage rv", "userId :: ${clsGetChatHistoryResponse.userId}")
            showLog("@@@ newMessage rv", "name :: ${clsGetChatHistoryResponse.name}")
            showLog("@@@ newMessage rv", "email :: ${clsGetChatHistoryResponse.email}")
            showLog("@@@ newMessage rv", "userImage :: ${clsGetChatHistoryResponse.userImage}")
            showLog("@@@ newMessage rv", "messageType :: ${clsGetChatHistoryResponse.messageType}")
            showLog("@@@ newMessage rv", "tagUserList :: ${clsGetChatHistoryResponse.tagUserList}")
            showLog("@@@ newMessage rv", "messageId :: ${clsGetChatHistoryResponse.messageId}")
            showLog("@@@ newMessage rv", "time :: ${clsGetChatHistoryResponse.time}")
            showLog("@@@ newMessage rv", "pushId :: ${clsGetChatHistoryResponse.pushId}")
            showLog("@@@ newMessage rv", "=========================================================================")

            var isMuteUser: Boolean = false
            if (getDashboardUserDetails()?.mutedUser != null && getDashboardUserDetails()?.mutedUser?.size ?: 0 > 0) {
                for (i in 0 until getDashboardUserDetails()?.mutedUser?.size!!) {
                    if (clsGetChatHistoryResponse.userId == getDashboardUserDetails()?.mutedUser?.get(i)) {
                        isMuteUser = true
                        break
                    } else {
                        isMuteUser = false
                    }
                }
            } else {
                isMuteUser = false
            }

            if (!isMuteUser) {
                receiveSocketDetails(clsGetChatHistoryResponse)
            }
        }
        socket.connect()
    }

    private fun receiveSocketDetails(message: ClsGetChatHistoryResponse.Data) {
        runOnUiThread {
            addMessageInAdapterList(message)
        }
    }

    private fun receiveSocketGraphDetails(message: String) {
        runOnUiThread {
            manageGraphFlow(message)
        }
    }

    private fun manageGraphFlow(message: String) {
        if (!TextUtils.isEmpty(message)) {
            val jsonObj: JSONObject = JSONObject(message)
            val finalValue: String = jsonObj.optString(SOCKET_FINAL_VALUE)
            val xValue: String = jsonObj.optString(SOCKET_X_VALUE)
            val displayValue: String = jsonObj.optString(SOCKET_DISPLAY_VALUE)
            val type: Int = jsonObj.optInt(SOCKET_TYPE)

            when (type) {
                SOCKET_TYPE_GRAPH_RUNNING -> {
                    clCountDownTimer.visibility = View.GONE
                    txtProgress.visibility = View.VISIBLE
                    txtProgress.setBackgroundResource(R.drawable.ic_img_graph_started_bg)
                    txtProgress.text = String.format("%.2f", xValue.toFloat()) + "x"

                    val gson = Gson()
                    val clsCrashStartModel: ClsCrashStartModel = gson.fromJson(jsonObj.toString(), object : TypeToken<ClsCrashStartModel?>() {}.type)
                    // For alternate name changing flow
                    /*if (clsCrashStartModel.randomUser != null && clsCrashStartModel.randomUser!!.isNotEmpty()) {
                        clGameUserAndDetails.visibility = View.VISIBLE

                        var pos: Int = Random().nextInt((clsCrashStartModel.randomUser?.size?.minus(1))?.plus(1)!!) + 1
                        pos = pos.minus(1)

                        if (clsCrashStartModel.randomUser!![pos].status == 2) {
                            if (getUserDetails()?.userID == clsCrashStartModel.randomUser!![pos].id.toString()) {
                                txtUserNameWithDetails.text = "you bet"
                                txtTotalCoins.text = changeCoinsFormat(clsCrashStartModel.randomUser!![pos].betAmount)
                            } else {
                                if (isUserWonGame) {
                                    txtUserNameWithDetails.text = "you won"
                                    txtWinXpCount.text = userXps
                                    txtTotalCoins.text = userCoins
                                    txtWinXpCount.visibility = View.VISIBLE
                                } else {
                                    txtUserNameWithDetails.text = clsCrashStartModel.randomUser!![pos].name + " bets"
                                    txtWinXpCount.visibility = View.GONE
                                    txtTotalCoins.text = changeCoinsFormat(clsCrashStartModel.randomUser!![pos].betAmount)
                                }
                            }
                        } else {
                            if (getUserDetails()?.userID == clsCrashStartModel.randomUser!![pos].id.toString()) {
                                txtUserNameWithDetails.text = "you won"
                                isUserWonGame = true
                                userXps = "+" + changeCoinsFormat(clsCrashStartModel.randomUser!![pos].calculate_xp)
                                userCoins = changeCoinsFormat(clsCrashStartModel.randomUser!![pos].calculateAmount)

                                txtWinXpCount.text = userXps
                                txtTotalCoins.text = userCoins
                            } else {
                                if (!isUserWonGame) {
                                    txtUserNameWithDetails.text = clsCrashStartModel.randomUser!![pos].name + " won"
                                    txtWinXpCount.text = "+" + changeCoinsFormat(clsCrashStartModel.randomUser!![pos].calculate_xp)
                                    txtTotalCoins.text = changeCoinsFormat(clsCrashStartModel.randomUser!![pos].calculateAmount)
                                } else {
                                    txtUserNameWithDetails.text = "you won"
                                    txtWinXpCount.text = userXps
                                    txtTotalCoins.text = userCoins
                                }
                            }
                            txtWinXpCount.visibility = View.VISIBLE
                        }
                    }*/

                    if (clsCrashStartModel.lastUser != null && clsCrashStartModel.randomUser != null && clsCrashStartModel.randomUser!!.isNotEmpty()) {
                        clGameUserAndDetails.visibility = View.VISIBLE

                        if (clsCrashStartModel.lastUser?.status == 2) {
                            if (getUserDetails()?.userID == clsCrashStartModel.lastUser?.id.toString()) {
                                txtUserNameWithDetails.text = "you bet"
                                txtTotalCoins.text = changeCoinsFormat(clsCrashStartModel.lastUser?.betAmount.toString())
                            } else {
                                if (isUserWonGame) {
                                    txtUserNameWithDetails.text = "you won"
                                    txtWinXpCount.text = userXps
                                    txtTotalCoins.text = userCoins
                                    txtWinXpCount.visibility = View.VISIBLE
                                } else {
                                    txtUserNameWithDetails.text = clsCrashStartModel.lastUser?.name + " bets"
                                    txtWinXpCount.visibility = View.GONE
                                    txtTotalCoins.text = changeCoinsFormat(clsCrashStartModel.lastUser?.betAmount.toString())
                                }
                            }
                        } else {
                            if (getUserDetails()?.userID == clsCrashStartModel.lastUser?.id.toString()) {
                                txtUserNameWithDetails.text = "you won"
                                isUserWonGame = true
                                userXps = "+" + changeCoinsFormat(clsCrashStartModel.lastUser?.calculate_xp.toString())
                                userCoins = changeCoinsFormat(clsCrashStartModel.lastUser?.calculateAmount.toString())

                                txtWinXpCount.text = userXps
                                txtTotalCoins.text = userCoins
                            } else {
                                if (!isUserWonGame) {
                                    txtUserNameWithDetails.text = clsCrashStartModel.lastUser?.name + " won"
                                    txtWinXpCount.text = "+" + changeCoinsFormat(clsCrashStartModel.lastUser?.calculate_xp.toString())
                                    txtTotalCoins.text = changeCoinsFormat(clsCrashStartModel.lastUser?.calculateAmount.toString())
                                } else {
                                    txtUserNameWithDetails.text = "you won"
                                    txtWinXpCount.text = userXps
                                    txtTotalCoins.text = userCoins
                                }
                            }
                            txtWinXpCount.visibility = View.VISIBLE
                        }
                    }
                }

                SOCKET_TYPE_COUNT_DOWN -> {
                    clCountDownTimer.visibility = View.VISIBLE
                    txtProgress.visibility = View.GONE
                    txtWinXpCount.visibility = View.GONE

                    if (!TextUtils.isEmpty(displayValue))
                        txtCountDown.text = displayValue

                    val gson = Gson()
                    val clsCrashStartModel: ClsCrashStartModel = gson.fromJson(
                        jsonObj.toString(),
                        object : TypeToken<ClsCrashStartModel?>() {}.type
                    )
                    // For alternate name changing flow
                    /*if (clsCrashStartModel.randomUser != null && clsCrashStartModel.randomUser!!.isNotEmpty()) {
                        clGameUserAndDetails.visibility = View.VISIBLE

                        var pos: Int = Random().nextInt((clsCrashStartModel.randomUser?.size?.minus(1))?.plus(1)!!) + 1
                        pos = pos.minus(1)

                        if (clsCrashStartModel.randomUser!![pos].status == 2) {
                            if (getUserDetails()?.userID == clsCrashStartModel.randomUser!![pos].id.toString()) {
                                txtUserNameWithDetails.text = "you bet"
                            } else {
                                if (!isUserJoin) {
                                    txtUserNameWithDetails.text = clsCrashStartModel.randomUser!![pos].name + " bets"
                                    txtTotalCoins.text = changeCoinsFormat(clsCrashStartModel.randomUser!![pos].betAmount)
                                } else {
                                    txtUserNameWithDetails.text = "you bet"
                                    txtTotalCoins.text = getMyBetAmount()
                                }
                            }
                            txtWinXpCount.visibility = View.GONE
                        }
                    }*/

                    if (clsCrashStartModel.lastUser != null && clsCrashStartModel.randomUser != null && clsCrashStartModel.randomUser!!.isNotEmpty()) {
                        clGameUserAndDetails.visibility = View.VISIBLE

                        if (clsCrashStartModel.lastUser?.status == 2) {
                            if (getUserDetails()?.userID == clsCrashStartModel.lastUser?.id.toString()) {
                                txtUserNameWithDetails.text = "you bet"
                                txtTotalCoins.text = getMyBetAmount()
                            } else {
                                if (!isUserJoin) {
                                    txtUserNameWithDetails.text = clsCrashStartModel.lastUser?.name + " bets"
                                    txtTotalCoins.text = changeCoinsFormat(clsCrashStartModel.lastUser?.betAmount.toString())
                                } else {
                                    txtUserNameWithDetails.text = "you bet"
                                    txtTotalCoins.text = getMyBetAmount()
                                }
                            }
                            txtWinXpCount.visibility = View.GONE
                        }
                    }
                }

                SOCKET_TYPE_CRASH_OUT -> {
                    clCountDownTimer.visibility = View.GONE
                    clGameUserAndDetails.visibility = View.GONE
                    txtWinXpCount.visibility = View.GONE
                    txtProgress.setBackgroundResource(R.drawable.ic_img_graph_crashed_bg)

                    if (isUserJoin) {
                        txtUserNameWithDetails.text = "you loss"
                        txtTotalCoins.text = Utility.getMyBetAmount()
                    }
                    isUserJoin = false
                    isUserWonGame = false
                    userCoins = ""
                    userXps = ""
                    if (!TextUtils.isEmpty(displayValue))
                        txtProgress.text = displayValue
                }
            }
        } else {
            showLog("@@@", "No message data found")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GOOGLE_SIGN_IN_CODE) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    // GooGle login
    private fun loginWithGoogleSettings() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val acct: GoogleSignInAccount? = completedTask.getResult(ApiException::class.java)
            //            showToast(this, "Sign in successfully")
            getUserProfileGoogle(acct)
        } catch (e: ApiException) {
            showLog("Google Sign In Error", "signInResult:failed code=" + e.statusCode)
            //            Toast.makeText(this@ProfileActivity, "Failed", Toast.LENGTH_LONG).show()
        }
    }

    private fun getUserProfileGoogle(acct: GoogleSignInAccount?) {
        if (acct != null) {
            val personName: String? = acct.displayName
            val personGivenName: String? = acct.givenName
            val personFamilyName: String? = acct.familyName
            val personEmail: String? = acct.email
            val personId: String? = acct.id
            val personPhoto: Uri? = acct.photoUrl
            showLog("google", "Name: $personName")
            showLog("google", "Email: $personEmail")
            showLog("google", "ID: $personId")
            showLog("google", "URL : $personPhoto")
            showLog("google", "personGivenName : $personGivenName")
            showLog("google", "personFamilyName : $personFamilyName")

            isUserRegistered(personName.toString(), personPhoto.toString(), personEmail.toString(), LOGIN_WITH_GOOGLE, personId.toString(), getUserDetails()?.userID.toString())
        } else {
            groupLogIn.visibility = View.VISIBLE
        }
    }

    // Facebook login
    private fun loginWithFacebook() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                val loggedIn = AccessToken.getCurrentAccessToken() == null
                showLog("Facebook", "onSuccess $loggedIn")
                getUserProfileFacebook(AccessToken.getCurrentAccessToken())
            }

            override fun onCancel() {
                showLog("Facebook", "onCancel")
            }

            override fun onError(error: FacebookException?) {
                showLog("Facebook", "onError $error")
            }
        })
    }

    private fun getUserProfileFacebook(currentAccessToken: AccessToken) {
        val request = GraphRequest.newMeRequest(
            currentAccessToken
        ) { `object`, response ->
            showLog("TAG", `object`.toString())

            if (`object` != null) {
                try {
                    val first_name = `object`.getString("first_name")
                    val last_name = `object`.getString("last_name")
                    val email = `object`.getString("email")
                    val id = `object`.getString("id")
                    val image_url = "https://graph.facebook.com/$id/picture?type=normal"
                    showLog("Facebook", "First Name: $first_name")
                    showLog("Facebook", "Last Name: $last_name")
                    showLog("Facebook", "email: $email")
                    showLog("Facebook", "id : $id")
                    showLog("Facebook", "image_url : $image_url")

                    isUserRegistered("$first_name $last_name", image_url, email, LOGIN_WITH_FACEBOOK, id.toString(), getUserDetails()?.userID.toString())
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            } else {
                groupLogIn.visibility = View.VISIBLE
            }
        }
        val parameters = Bundle()
        parameters.putString("fields", "first_name,last_name,email,id")
        request.parameters = parameters
        request.executeAsync()
    }

    /**
     * show Logout Dialog
     */
    private fun loadDataDialog(
        name: String,
        avatar: String,
        email: String,
        socialMediaType: String,
        socialMediaId: String,
        userID: String,
        is_register: String?,
        oldUserId: String?
    ) {
        val loadDataDialog = LoadDataDialog(this@ChatActivity,
            object : OnCallBack {
                override fun onCallBackReturn(stringValue: String) {
                    login(name, avatar, email, socialMediaType, socialMediaId, userID, is_register, oldUserId)
                }
            })
        loadDataDialog.setCancelable(false)
        loadDataDialog.show()
    }

    /**
     * show Already Logged In Dialog
     */
    private fun showAlreadyLoggedInDialog(
        name: String,
        avatar: String,
        email: String,
        socialMediaType: String,
        socialMediaId: String,
        userID: String,
        is_register: String?,
        oldUserId: String?
    ) {
        val alreadyLoggedInDialog = AlreadyLoggedInDialog(this@ChatActivity,
            object : OnCallBack {
                override fun onCallBackReturn(stringValue: String) {
                    logoutAndNewUserLogin(name, avatar, email, socialMediaType, socialMediaId, userID, is_register, oldUserId)
                }
            })
        alreadyLoggedInDialog.setCancelable(false)
        alreadyLoggedInDialog.show()
    }

    // API Call
    private fun getChatHistory(userId: String, lastReadId: String, pgn: Int) {

        ApiCallMethods(this@ChatActivity).getChatHistory(
            userId, lastReadId, pgn,
            object : OnApiCallCompleted<ClsGetChatHistoryResponse> {
                override fun apiSuccess(obj: Any?) {
                    if (obj != null) {
                        val clsGetChatHistoryResponse: ClsGetChatHistoryResponse = obj as ClsGetChatHistoryResponse

                        if (clsGetChatHistoryResponse.data != null && clsGetChatHistoryResponse.data?.size ?: 0 > 0) {
                            chatMessageList = clsGetChatHistoryResponse.data as ArrayList<ClsGetChatHistoryResponse.Data>
                            setAdapter(chatMessageList)

                            pageNumber += 1
                        }

                        if (pullChatHistory != null) pullChatHistory.setRefreshing(false)
                    }
                }

                override fun apiFailure(errorMessage: String) {
                    showToast(this@ChatActivity, errorMessage)
                    if (pullChatHistory != null) pullChatHistory.setRefreshing(false)
                }

                override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                    val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                    showToast(this@ChatActivity, errorMessage)
                    if (pullChatHistory != null) pullChatHistory.setRefreshing(false)
                }
            })
    }

    private fun reportUser(
        userId: String,
        reportUserId: String,
        chatMessage: String,
        reportType: String
    ) {

        ApiCallMethods(this@ChatActivity)
            .reportUser(
                userId, reportUserId, chatMessage, reportType,
                object : OnApiCallCompleted<ClsReportUserResponse> {
                    override fun apiSuccess(obj: Any?) {
                        if (obj != null) {
                            val clsReportUserResponse: ClsReportUserResponse = obj as ClsReportUserResponse
                            if (reportType == "1") {
                                showToast(this@ChatActivity, "User Reported and Mute successfully")
                            } else {
                                showToast(this@ChatActivity, "User Reported successfully")
                            }
                            reportUserDialog?.dismiss()
                        }
                    }

                    override fun apiFailure(errorMessage: String) {
                        showToast(this@ChatActivity, errorMessage)
                        if (pullChatHistory != null) pullChatHistory.setRefreshing(false)
                    }

                    override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                        val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                        showToast(this@ChatActivity, errorMessage)
                        if (pullChatHistory != null) pullChatHistory.setRefreshing(false)
                    }
                })
    }

    private fun login(
        name: String,
        avatar: String,
        email: String,
        socialMediaType: String,
        socialMediaId: String,
        userID: String,
        is_register: String?,
        oldUserId: String?
    ) {
        ApiCallMethods(this@ChatActivity)
            .login(name, avatar, email, socialMediaType, socialMediaId, userID, is_register, oldUserId,
                object : OnApiCallCompleted<ClsLoginResponse> {
                    override fun apiSuccess(obj: Any?) {
                        if (obj != null) {
                            val clsLoginResponse: ClsLoginResponse = obj as ClsLoginResponse
                            if (clsLoginResponse.success) {
                                CrashPotApplication.instance.preferenceData?.setValueBoolean(PREF_IS_GUEST_USER, false)
                                CrashPotApplication.instance.preferenceData?.setValueBoolean(PREF_IS_USER_LOGGED_IN, true)

                                val clsGuestRandomNumberResponse: ClsGuestRandomNumberResponse.Data? = getUserDetails()
                                clsGuestRandomNumberResponse?.userName = name
                                clsGuestRandomNumberResponse?.socialMediaType = socialMediaType
                                CrashPotApplication.instance.preferenceData?.setValue(PREF_USER_DETAIL, Gson().toJson(clsGuestRandomNumberResponse))
                                CrashPotApplication.instance.preferenceData?.setValueInt(PREF_USER_LEVEL, clsLoginResponse.data?.rankingByLevel!!)
                                manageChatRestrictionUi()
                                getProfileDashboardData(clsLoginResponse?.data?.userID.toString(), true)
                            }
                        }
                    }

                    override fun apiFailure(errorMessage: String) {
                        showToast(this@ChatActivity, errorMessage)
                    }

                    override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                        if (code == RESPONSE_CODE_401) {
                            showAlreadyLoggedInDialog(name, avatar, email, socialMediaType, socialMediaId, userID, is_register, oldUserId)
                        } else {
                            val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                            showToast(this@ChatActivity, errorMessage)
                        }
                    }
                })
    }

    private fun logoutAndNewUserLogin(
        name: String,
        avatar: String,
        email: String,
        socialMediaType: String,
        socialMediaId: String,
        userID: String,
        is_register: String?,
        oldUserId: String?
    ) {
        ApiCallMethods(this@ChatActivity).logout(
            oldUserId.toString(), object : OnApiCallCompleted<ClsLogOutResponse> {
                override fun apiSuccess(obj: Any?) {
                    if (obj != null) {
                        val clsLogOutResponse: ClsLogOutResponse = obj as ClsLogOutResponse
                        if (clsLogOutResponse.success) {
                            login(name, avatar, email, socialMediaType, socialMediaId, userID, is_register, oldUserId)
                        }
                    }
                }

                override fun apiFailure(errorMessage: String) {
                    showToast(this@ChatActivity, errorMessage)
                }

                override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                    val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                    showToast(this@ChatActivity, errorMessage)
                }
            })
    }

    private fun getProfileDashboardData(userId: String, showProgress: Boolean) {
        ApiCallMethods(this@ChatActivity).getProfileDashboardData(
            userId, showProgress,
            object : OnApiCallCompleted<ClsProfileDashboardResponse> {
                override fun apiSuccess(obj: Any?) {
                    if (obj != null) {
                        val clsProfileDashboardResponse: ClsProfileDashboardResponse = obj as ClsProfileDashboardResponse
                        if (clsProfileDashboardResponse.success) {

                            val clsGuestRandomNumberResponse: ClsGuestRandomNumberResponse.Data? = getUserDetails()
                            clsGuestRandomNumberResponse?.guestNumber = clsProfileDashboardResponse.data?.guestNumber.toString()
                            clsGuestRandomNumberResponse?.userID = clsProfileDashboardResponse.data?.userID.toString()
                            clsGuestRandomNumberResponse?.userName = clsProfileDashboardResponse.data?.userName.toString()
                            clsGuestRandomNumberResponse?.userImage = clsProfileDashboardResponse.data?.userImage.toString()
                            clsGuestRandomNumberResponse?.email = clsProfileDashboardResponse.data?.email.toString()
                            clsGuestRandomNumberResponse?.isRegister = 1
                            clsGuestRandomNumberResponse?.isBlock = clsProfileDashboardResponse.data?.isBlock ?: 0
                            clsGuestRandomNumberResponse?.totalXP = clsProfileDashboardResponse.data?.totalXP?.toInt() ?: 0
                            clsGuestRandomNumberResponse?.totalCoins = clsProfileDashboardResponse.data?.totalCoins?.toInt() ?: 0
                            clsGuestRandomNumberResponse?.profit = clsProfileDashboardResponse.data?.profit?.toInt() ?: 0
                            clsGuestRandomNumberResponse?.wagered = clsProfileDashboardResponse.data?.wagered?.toInt() ?: 0
                            clsGuestRandomNumberResponse?.playedGames = clsProfileDashboardResponse.data?.playedGames?.toInt() ?: 0
                            clsGuestRandomNumberResponse?.rankingByLevel = clsProfileDashboardResponse.data?.rankingByLevel?.toInt() ?: 0
                            clsGuestRandomNumberResponse?.rankingByProfit = clsProfileDashboardResponse.data?.rankingByProfit?.toInt() ?: 0
                            clsGuestRandomNumberResponse?.lastReadId = clsProfileDashboardResponse.data?.lastReadId ?: 0
                            CrashPotApplication.instance.preferenceData?.setValue(PREF_USER_DETAIL, Gson().toJson(clsGuestRandomNumberResponse))

                            CrashPotApplication.instance.preferenceData?.setValue(PREF_DASHBOARD_USER_DETAIL, Gson().toJson(clsProfileDashboardResponse.data))
                        }
                    }
                }

                override fun apiFailure(errorMessage: String) {
                    showToast(this@ChatActivity, errorMessage)
                }

                override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                    val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                    showToast(this@ChatActivity, errorMessage)
                }
            })
    }

    private fun isUserRegistered(
        name: String,
        avatar: String,
        email: String,
        socialMediaType: String,
        socialMediaId: String,
        userID: String
    ) {
        ApiCallMethods(this@ChatActivity).isUserRegistered(
            socialMediaId, object : OnApiCallCompleted<ClsIsUserRegisteredResponse> {
                override fun apiSuccess(obj: Any?) {
                    if (obj != null) {
                        val clsIsUserRegisteredResponse: ClsIsUserRegisteredResponse = obj as ClsIsUserRegisteredResponse
                        if (clsIsUserRegisteredResponse.data?.isRegister == 1) {

                            loadDataDialog(
                                name, avatar, email, socialMediaType, socialMediaId, userID,
                                clsIsUserRegisteredResponse.data?.isRegister.toString(), clsIsUserRegisteredResponse.data?.oldUserId.toString()
                            )
                        } else {
                            login(
                                name, avatar, email, socialMediaType, socialMediaId, userID,
                                clsIsUserRegisteredResponse.data?.isRegister.toString(), clsIsUserRegisteredResponse.data?.oldUserId.toString()
                            )
                        }
                    }
                }

                override fun apiFailure(errorMessage: String) {
                    showToast(this@ChatActivity, errorMessage)
                }

                override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                    val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                    showToast(this@ChatActivity, errorMessage)
                }
            })
    }

    private fun addUserCoins(
        toUserId: String, toUserName: String, coins: String, gameType: String,
        status: String, fromUserId: String,
        isXpOrCoin: String
    ) {
        ApiCallMethods(this@ChatActivity)
            .addUserCoins(
                toUserId,
                coins,
                gameType,
                status,
                fromUserId,
                isXpOrCoin,
                object : OnApiCallCompleted<ClsAddCoinsResponse> {
                    override fun apiSuccess(obj: Any?) {
                        if (obj != null) {
                            val clsAddCoinsResponse: ClsAddCoinsResponse = obj as ClsAddCoinsResponse
                            if (clsAddCoinsResponse.success) {
                                tagUserList.add(getDashboardUserDetails()?.userName)
                                tagUserList.add(toUserName)
                                pushId = toUserId
                                var message: String = "@${getDashboardUserDetails()?.userName} rained ${changeCoinsFormat(coins)} coins on @${toUserName}"
                                sendMessage(message, MESSAGE_TYPE_BROADCAST)

                                rainOnDialog?.dismiss()
                            }
                        }
                    }

                    override fun apiFailure(errorMessage: String) {
                        showToast(this@ChatActivity, errorMessage)
                    }

                    override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                        val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                        showToast(this@ChatActivity, errorMessage)
                    }
                })
    }
}