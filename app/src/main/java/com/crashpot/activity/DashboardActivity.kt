package com.crashpot.activity

import android.annotation.SuppressLint
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.CountDownTimer
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import com.android.billingclient.api.*
import com.crashpot.CrashPotApplication
import com.crashpot.R
import com.crashpot.base.BaseActivity
import com.crashpot.dialog.*
import com.crashpot.interfaces.*
import com.crashpot.model.*
import com.crashpot.network.ApiCallMethods
import com.crashpot.network.OnApiCallCompleted
import com.crashpot.util.*
import com.crashpot.util.BubbleAnimationUtils.Companion.startWatchAndEarnBubbleAnimation
import com.crashpot.util.BubbleAnimationUtils.Companion.startWheelSpinBubbleAnimation
import com.crashpot.util.BubbleAnimationUtils.Companion.stopWatchAndEarnBubbleAnimation
import com.crashpot.util.BubbleAnimationUtils.Companion.stopWheelSpinBubbleAnimation
import com.crashpot.util.Utility.Companion.changeCoinsFormat
import com.crashpot.util.Utility.Companion.getDashboardUserDetails
import com.crashpot.util.Utility.Companion.getInterstitialAdCount
import com.crashpot.util.Utility.Companion.getMyBetAmount
import com.crashpot.util.Utility.Companion.getReviewDialogCount
import com.crashpot.util.Utility.Companion.getUserDetails
import com.crashpot.util.Utility.Companion.getUserLevel
import com.crashpot.util.Utility.Companion.setRewardedVideoAlarm
import com.crashpot.util.Utility.Companion.setSimpleImage
import com.crashpot.util.Utility.Companion.setSpinWheelAlarm
import com.crashpot.util.Utility.Companion.shareOnFacebookAsPost
import com.crashpot.util.Utility.Companion.showLog
import com.crashpot.util.Utility.Companion.showToast
import com.crashpot.util.mpchartlib.charts.CombinedChart
import com.crashpot.util.mpchartlib.charts.ScatterChart
import com.crashpot.util.mpchartlib.components.XAxis
import com.crashpot.util.mpchartlib.components.YAxis
import com.crashpot.util.mpchartlib.data.*
import com.crashpot.util.mpchartlib.formatter.ValueFormatter
import com.crashpot.util.mpchartlib.interfaces.datasets.ILineDataSet
import com.google.android.gms.ads.*
import com.google.android.gms.ads.RequestConfiguration.TAG_FOR_CHILD_DIRECTED_TREATMENT_TRUE
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import io.socket.client.IO
import io.socket.client.Socket
import kotlinx.android.synthetic.main.activity_dashboard.*
import org.json.JSONObject
import java.net.URISyntaxException
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.math.sqrt

class DashboardActivity : BaseActivity(), View.OnClickListener {

    var map: HashMap<Float, String> = HashMap<Float, String>()

    // Chart related
    private var xAxis = 0f
    private var yAxis = 0f
    private var maxValue = 0f

    // Socket
    private lateinit var socket: Socket
    private var status = 2
    private var isTimerOn = false
    private var isUserJoin = false
    private var isUserWonGame: Boolean = false
    var gameId: Int = 0
    var betId: Int = 0
    private var userCoins: String = ""
    private var userXps: String = ""

    // Ad mob
    private lateinit var mRewardedVideoAd: RewardedVideoAd
    private lateinit var mInterstitialAd: InterstitialAd
    private lateinit var spinWheelDialog: SpinWheelDialog

    private lateinit var billingClient: BillingClient
    private var purchaseID: String = ""

    override fun getLayoutResId(): Int = R.layout.activity_dashboard

    override fun setListeners() {
        clbtnBet.setOnClickListener(this)
        imgEditUserProfile.setOnClickListener(this)
        clNotificationCount.setOnClickListener(this)
        clChatCount.setOnClickListener(this)
        imgLeaderBoard.setOnClickListener(this)
        clStrategy.setOnClickListener(this)
        clMyBet.setOnClickListener(this)
        imgWatchAndLearn.setOnClickListener(this)
        imgSpinWheel.setOnClickListener(this)
        clByCoins.setOnClickListener(this)
    }

    override fun initViews() {
        (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).cancelAll()

        if (getUserDetails()?.isRegister == 0)
            showWelcomeBonusDialog()

        if (getUserDetails()?.socialMediaType == LOGIN_AS_GUEST) {
            CrashPotApplication.instance.preferenceData?.setValueBoolean(PREF_IS_GUEST_USER, true)
            CrashPotApplication.instance.preferenceData?.setValueBoolean(
                PREF_IS_USER_LOGGED_IN,
                false
            )
        } else {
            CrashPotApplication.instance.preferenceData?.setValueBoolean(PREF_IS_GUEST_USER, false)
            CrashPotApplication.instance.preferenceData?.setValueBoolean(
                PREF_IS_USER_LOGGED_IN,
                true
            )
        }

        txtUserName.text = getUserDetails()?.userName

        connectSocketIO() // TODO Socket IO

        setUpChart()

        setMyBetAndStrategyValue()

        MobileAds.initialize(this) {}
        loadBannerAdd()

        // Rewarded
        MobileAds.initialize(this, getString(R.string.ad_mob_app_id))
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this)

        // Interstitial Ad
        mInterstitialAd = InterstitialAd(this)
        mInterstitialAd.adUnitId = getString(R.string.ad_interstitial_unit_id)

        showAdMobTimer()
        showSpinWheelTimer()

        setUpBilling()
    }

    private fun loadInterstitialAd() {
        mInterstitialAd.loadAd(AdRequest.Builder().build())
        mInterstitialAd.adListener = object : AdListener() {
            override fun onAdLoaded() {
                showLog("Interstitial Ad", "onAdLoaded")
                showInterstitialAd()
            }

            override fun onAdFailedToLoad(errorCode: Int) {
                showLog("Interstitial Ad", "onAdFailedToLoad error : $errorCode")
            }

            override fun onAdOpened() {
                showLog("Interstitial Ad", "onAdOpened")
            }

            override fun onAdClicked() {
                showLog("Interstitial Ad", "onAdClicked")
            }

            override fun onAdLeftApplication() {
                showLog("Interstitial Ad", "onAdLeftApplication")
            }

            override fun onAdClosed() {
                showLog("Interstitial Ad", "onAdClosed")
            }
        }
    }

    private fun showInterstitialAd() {
        if (mInterstitialAd.isLoaded) {
            CrashPotApplication.instance.preferenceData?.setValueInt(PREF_INTERSTITIAL_AD_COUNT, 0)
            mInterstitialAd.show()
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.")
        }
    }

    private fun showAdMobTimer() {
        showLog(
            "3_hours",
            "System time : " + System.currentTimeMillis() + " new time : " + (System.currentTimeMillis() + MILLISECONDS_3_HOURS)
        )

        imgWatchAndLearn.setBackgroundResource(R.drawable.img_watch_earn_with_text_deselected)
        txtWatchAndLearnTimer.visibility = View.VISIBLE
        stopWatchAndEarnBubbleAnimation()

        var time: Long =
            CrashPotApplication.instance.preferenceData?.getValueLongFromKey(PREF_TIMER_AD_MOB)!!;

        if (mRewardedVideoAd.isLoaded && time < System.currentTimeMillis()) {

            imgWatchAndLearn.setBackgroundResource(R.drawable.img_watch_earn_with_text)
            txtWatchAndLearnTimer.visibility = View.GONE
            startWatchAndEarnBubbleAnimation(imgWatchAndLearn)
        } else if (time < System.currentTimeMillis()) {
            loadRewardedVideoAd()
        } else {
            var timer =
                CrashPotApplication.instance.preferenceData?.getValueLongFromKey(PREF_TIMER_AD_MOB)!! - System.currentTimeMillis()
            timer += 1000
            object : CountDownTimer(timer, 1000) {
                override fun onTick(ms: Long) {

                    val text = String.format(
                        "%02d:%02d:%02d",
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(ms)) / 60,
                        TimeUnit.MILLISECONDS.toMinutes(ms) - TimeUnit.HOURS.toMinutes(
                            TimeUnit.MILLISECONDS.toHours(
                                ms
                            )
                        ),
                        TimeUnit.MILLISECONDS.toSeconds(ms) - TimeUnit.MINUTES.toSeconds(
                            TimeUnit.MILLISECONDS.toMinutes(
                                ms
                            )
                        )
                    )
                    //                    showLog("hours", "" + text)
                    txtWatchAndLearnTimer.text = text
                }

                override fun onFinish() {
                    showLog("hours", "0")
                    loadRewardedVideoAd()
                }
            }.start()
        }
    }

    private fun showSpinWheelTimer() {
        showLog(
            "6_hours",
            "System time : " + System.currentTimeMillis() + " new time : " + (System.currentTimeMillis() + MILLISECONDS_6_HOURS)
        )

        imgSpinWheel.setBackgroundResource(R.drawable.img_spin_wheel_with_text_deselected)
        txtSpinWheel.visibility = View.VISIBLE
        stopWheelSpinBubbleAnimation()

        var time: Long =
            CrashPotApplication.instance.preferenceData?.getValueLongFromKey(PREF_TIMER_WHEEL_SPIN)!!;

        if (time < System.currentTimeMillis()) {
            imgSpinWheel.setBackgroundResource(R.drawable.img_spin_wheel_with_text)
            txtSpinWheel.visibility = View.GONE
            startWheelSpinBubbleAnimation(imgSpinWheel)

        } else {
            var timer = CrashPotApplication.instance.preferenceData?.getValueLongFromKey(
                PREF_TIMER_WHEEL_SPIN
            )!! - System.currentTimeMillis()
            timer += 1000
            object : CountDownTimer(timer, 1000) {
                override fun onTick(ms: Long) {

                    val text = String.format(
                        "%02d:%02d:%02d",
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(ms)) / 60,
                        TimeUnit.MILLISECONDS.toMinutes(ms) - TimeUnit.HOURS.toMinutes(
                            TimeUnit.MILLISECONDS.toHours(
                                ms
                            )
                        ),
                        TimeUnit.MILLISECONDS.toSeconds(ms) - TimeUnit.MINUTES.toSeconds(
                            TimeUnit.MILLISECONDS.toMinutes(
                                ms
                            )
                        )
                    )
                    //                    showLog("hours", "" + text)
                    txtSpinWheel.text = text
                }

                override fun onFinish() {
                    showLog("hours", "0")

                    imgSpinWheel.setBackgroundResource(R.drawable.img_spin_wheel_with_text)
                    txtSpinWheel.visibility = View.GONE

                }
            }.start()
        }
    }

    private fun loadBannerAdd() {
        val adRequest = AdRequest.Builder().build()
        adViewBanner.loadAd(adRequest)
    }

    private fun loadRewardedVideoAd() {

        val conf: RequestConfiguration = RequestConfiguration.Builder()
            .setTagForChildDirectedTreatment(TAG_FOR_CHILD_DIRECTED_TREATMENT_TRUE).build()
        MobileAds.setRequestConfiguration(conf)

        if (!mRewardedVideoAd.isLoaded) {
            val adRequest = AdRequest.Builder()
                .addTestDevice("FE31293CFC5B36E317886BA12AF676F7") // Asus max pro M2
                .build()
            mRewardedVideoAd.loadAd(getString(R.string.ad_rewarded_unit_id), adRequest)
        }

        mRewardedVideoAd.rewardedVideoAdListener = object : RewardedVideoAdListener {
            override fun onRewardedVideoAdClosed() {
                showLog("c AD", "onRewardedVideoAdClosed")
                showAdMobTimer()
            }

            override fun onRewardedVideoAdLeftApplication() {
                showLog("Rewarded AD", "onRewardedVideoAdLeftApplication")
            }

            override fun onRewardedVideoAdLoaded() {
                showLog("Rewarded AD", "onRewardedVideoAdLoaded")
                imgWatchAndLearn.setBackgroundResource(R.drawable.img_watch_earn_with_text)
                txtWatchAndLearnTimer.visibility = View.GONE
                startWatchAndEarnBubbleAnimation(imgWatchAndLearn)
            }

            override fun onRewardedVideoAdOpened() {
                showLog("Rewarded AD", "onRewardedVideoAdOpened")
            }

            override fun onRewardedVideoCompleted() {
                showLog("Rewarded AD", "onRewardedVideoCompleted")
            }

            override fun onRewarded(reward: RewardItem?) {
                showYourRewardsDialog(FROM_GOOGLE_ADS, GAME_TYPE_GOOGLE_ADS, "10000", ADD_COINS_COIN)
                showLog(
                    "Rewarded AD",
                    "onRewarded! currency: ${reward?.type} amount: ${reward?.amount}"
                )
                CrashPotApplication.instance.preferenceData?.setValueLong(
                    PREF_TIMER_AD_MOB,
                    (System.currentTimeMillis() + MILLISECONDS_3_HOURS)
                )
                setRewardedVideoAlarm(
                    this@DashboardActivity,
                    (System.currentTimeMillis() + MILLISECONDS_3_HOURS)
                )
            }

            override fun onRewardedVideoStarted() {
                showLog("Rewarded AD", "onRewardedVideoStarted")
            }

            override fun onRewardedVideoAdFailedToLoad(errorCode: Int) {
                showLog("Rewarded AD", "onRewardedVideoAdFailedToLoad: $errorCode")
            }
        }
    }

    override fun onPause() {
        super.onPause()
        mRewardedVideoAd.pause(this)
    }

    override fun onResume() {
        super.onResume()
        getProfileDashboardData(getUserDetails()?.userID.toString(), true)
        mRewardedVideoAd.resume(this)
        ChatActivity.isChatScreenOpen = false
        showAdMobTimer()
    }

    override fun onDestroy() {
        super.onDestroy()
        mRewardedVideoAd.destroy(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.clbtnBet -> {
                manageBetbutton()
            }
            R.id.imgEditUserProfile -> {
                startActivity(
                    Intent(this@DashboardActivity, ProfileActivity::class.java)
                        .putExtra(EXTRAS_USER_ID, getDashboardUserDetails()?.userID)
                )
            }
            R.id.clNotificationCount -> {
                startActivity(Intent(this@DashboardActivity, NotificationActivity::class.java))
            }
            R.id.clChatCount -> {
                startActivity(Intent(this@DashboardActivity, ChatActivity::class.java))
            }
            R.id.imgLeaderBoard -> {
                startActivity(Intent(this@DashboardActivity, RankingActivity::class.java))
            }
            R.id.clMyBet -> {
                if (!isUserJoin)
                    showStrategyAndAmountDialog(getString(R.string.my_bet))
            }
            R.id.clStrategy -> {
                if (!isUserJoin)
                    showStrategyAndAmountDialog(getString(R.string.strategy))
            }
            R.id.clByCoins -> {
                showInAppPurchaseDialog()
            }
            R.id.imgSpinWheel -> {
                if (txtSpinWheel.visibility == View.VISIBLE) {
                    showInAppPurchaseDialog()
                } else {
                    showSpinWheelDialog()
                }
            }
            R.id.imgWatchAndLearn -> {
                if (txtWatchAndLearnTimer.visibility == View.VISIBLE) {
                    showInAppPurchaseDialog()
                } else {
                    imgWatchAndLearn.setBackgroundResource(R.drawable.img_watch_earn_with_text_deselected)
                    txtWatchAndLearnTimer.visibility = View.VISIBLE
                    stopWatchAndEarnBubbleAnimation()

                    if (mRewardedVideoAd.isLoaded) {
                        mRewardedVideoAd.show()
                    }
                }
            }
        }
    }

    private fun manageBetbutton() {
        val myBet: Double = getMyBetAmount()?.toDouble() ?: 0.0
        val coins: Double = Utility.getDashboardUserDetails()?.totalCoins?.toDouble() ?: 0.0

        if (isTimerOn && isUserJoin) { // cancel bet
            if (myBet > coins) {
                showToast(
                    this,
                    getString(R.string.not_enough_coins_please_set_bet_amount_below_equal_your_coins)
                )
            } else {
                try {
                    val betJson = JsonObject()
                    betJson.addProperty("userId", getUserDetails()?.userID)
                    betJson.addProperty("userName", getUserDetails()?.userName)
                    betJson.addProperty("xAxis", xAxis)
                    betJson.addProperty("yAxis", yAxis)
                    betJson.addProperty("betAmount", getMyBetAmount())
                    betJson.addProperty("finalValue", maxValue)
                    betJson.addProperty("status", 3)
                    betJson.addProperty("betId", betId)
                    betJson.addProperty("gameId", gameId)
                    socket.emit("betPoint", betJson.toString())

                    isUserJoin = false
                    txtBet.text = "Bet"
                    txtBetLine2.visibility = View.GONE
                    txtBetLine2.text = "Cancel"
                    imgBtnBet.setBackgroundResource(R.drawable.img_start_bet_button)

                    showLog("@@@ betJson", betJson.toString())
                } catch (e: URISyntaxException) {
                    showLog("@@@ betJson error ", e.printStackTrace().toString())
                    e.printStackTrace()
                }
            }
        } else if (isTimerOn && !isUserJoin) { // bet button
            if (myBet > coins) {
                showToast(
                    this,
                    getString(R.string.not_enough_coins_please_set_bet_amount_below_equal_your_coins)
                )
            } else {
                try {
                    val betJson = JsonObject()
                    betJson.addProperty("userId", getUserDetails()?.userID)
                    betJson.addProperty("userName", getUserDetails()?.userName)
                    betJson.addProperty("xAxis", xAxis)
                    betJson.addProperty("yAxis", yAxis)
                    betJson.addProperty("betAmount", getMyBetAmount())
                    betJson.addProperty("finalValue", maxValue)
                    betJson.addProperty("status", 2)
                    betJson.addProperty("betId", betId)
                    betJson.addProperty("gameId", gameId)
                    socket.emit("betPoint", betJson.toString())
                    showLog("@@@ betJson", betJson.toString())

                    isUserJoin = true
                    txtBet.text = "Betting"
                    txtBetLine2.visibility = View.VISIBLE
                    txtBetLine2.text = "Cancel"
                    imgBtnBet.setBackgroundResource(R.drawable.img_start_betting_button)

                } catch (e: URISyntaxException) {
                    showLog("@@@ betJson error ", e.printStackTrace().toString())
                    e.printStackTrace()
                }
            }
        } else if (!isTimerOn && isUserJoin) { // Cashout button click
            try {
                val betJson = JsonObject()
                betJson.addProperty("userId", getUserDetails()?.userID)
                betJson.addProperty("userName", getUserDetails()?.userName)
                betJson.addProperty("xAxis", xAxis)
                betJson.addProperty("yAxis", yAxis)
                betJson.addProperty("betAmount", getMyBetAmount())
                betJson.addProperty("finalValue", maxValue)
                betJson.addProperty("status", -1)
                betJson.addProperty("betId", betId)
                betJson.addProperty("gameId", gameId)
                betJson.addProperty("rankingByLevel", getDashboardUserDetails()?.rankingByLevel)
                socket.emit("betPoint", betJson.toString())
                showLog("@@@ betJson", betJson.toString())

                var count: Int = getInterstitialAdCount()
                if (count < INTERSTITIAL_AD_MAX_VALUE) {
                    count += 1
                } else {
                    count = 0
                }
                CrashPotApplication.instance.preferenceData?.setValueInt(
                    PREF_INTERSTITIAL_AD_COUNT,
                    count
                )

                manageReviewCount()

                if (getReviewDialogCount() == REVIEW_DIALOG_MAX_VALUE &&
                    !CrashPotApplication.instance.preferenceData?.getValueBooleanFromKey(
                        PREF_DONT_SHOW_REVIEW_DIALOG
                    )!!
                ) {
                    showReviewDialog()
                }

                isUserJoin = false
                txtBet.text = "Bet"
                txtBetLine2.visibility = View.VISIBLE
                txtBetLine2.text = "wait for next round"
                imgBtnBet.setBackgroundResource(R.drawable.img_wait_for_bet_button)

                getProfileDashboardData(getUserDetails()?.userID.toString(), false)

            } catch (e: URISyntaxException) {
                showLog("@@@ betJson error ", e.printStackTrace().toString())
                e.printStackTrace()
            }
        } else {

        }
    }

    private fun manageReviewCount() {
        var reviewCount: Int = getReviewDialogCount()

        if (reviewCount < REVIEW_DIALOG_MAX_VALUE) {
            reviewCount += 1
        } else {
            reviewCount = 0
        }
        CrashPotApplication.instance.preferenceData?.setValueInt(
            PREF_REVIEW_DIALOG_COUNT,
            reviewCount
        )
    }

    private fun setMyBetAndStrategyValue() {
        if (!TextUtils.isEmpty(
                CrashPotApplication.instance.preferenceData?.getValueFromKey(
                    PREF_MY_BET_VALUE
                )
            )
        ) {
            txtMyBetValue.text =
                CrashPotApplication.instance.preferenceData?.getValueFromKey(PREF_MY_BET_VALUE)
        } else {
            txtMyBetValue.text = "1000"
            CrashPotApplication.instance.preferenceData?.setValue(PREF_MY_BET_VALUE, "1000")
        }

        if (!TextUtils.isEmpty(
                CrashPotApplication.instance.preferenceData?.getValueFromKey(
                    PREF_STRATEGY_VALUE
                )
            )
        ) {
            txtStrategyValue.text =
                CrashPotApplication.instance.preferenceData?.getValueFromKey(PREF_STRATEGY_VALUE) + "x"
        } else {
            txtStrategyValue.text = "2500x"
            CrashPotApplication.instance.preferenceData?.setValue(PREF_STRATEGY_VALUE, "2500")
        }
    }

    /**
     * show Welcome Bonus Dialog
     */
    private fun showWelcomeBonusDialog() {
        val clsGuestRandomNumberResponse: ClsGuestRandomNumberResponse.Data? = getUserDetails()
        clsGuestRandomNumberResponse?.isRegister = 1
        CrashPotApplication.instance.preferenceData?.setValue(
            PREF_USER_DETAIL,
            Gson().toJson(clsGuestRandomNumberResponse)
        )

        //        addUserCoins(getUserDetails()?.userID.toString(),
        //            "100",
        //            GAME_TYPE_REWARD_POINTS,
        //            COINS_STATUS_WIN_OR_ADD,
        //            getUserDetails()?.userID.toString(),
        //            ADD_COINS_COIN)

        val welcomeBonusDialog = WelcomeBonusDialog(this@DashboardActivity,
            object : OnCallBack {
                override fun onCallBackReturn(stringValue: String) {
                    shareOnFacebookAsPost(
                        this@DashboardActivity,
                        getUserDetails()?.userName + " Wins : 100,000 coins reward..!"
                    )
                }
            })
        welcomeBonusDialog.setCancelable(false)
        welcomeBonusDialog.show()
    }

    /**
     * show Review Dialog
     */
    private fun showReviewDialog() {
        val reviewDialog = ReviewDialog(this@DashboardActivity,
            object : OnReviewDialogCallBack {
                override fun onReviewNowClick(stringValue: String) {
                    Utility.openAppInPlayStore(this@DashboardActivity)
                }

                override fun onMayBeLaterClick(stringValue: String) {
                }

                override fun onDontAskAgainClick(stringValue: String) {
                    CrashPotApplication.instance.preferenceData?.setValueBoolean(
                        PREF_DONT_SHOW_REVIEW_DIALOG,
                        true
                    )
                }

                override fun onCallBackReturn(stringValue: String) {
                }
            })
        reviewDialog.setCancelable(false)
        reviewDialog.show()
    }

    /**
     * show Level Up Dialog
     */

    private fun showLevelUpDialog(rankingByLevel: Int?) {
        try {
            val levelUpDialog = LevelUpDialog(rankingByLevel, this@DashboardActivity,
                object : OnShareDialogCallBack {
                    override fun onEmptyCallBak() {
                        addUserCoins(
                            getUserDetails()?.userID.toString(),
                            (rankingByLevel!! * 1000).toString(),
                            GAME_TYPE_REWARD_POINTS,
                            COINS_STATUS_WIN_OR_ADD,
                            getUserDetails()?.userID.toString(),
                            ADD_COINS_COIN
                        )
                    }

                    override fun onCallBakeWithString(amount: String, isXpOrCoin: String) {
                        shareOnFacebookAsPost(
                            this@DashboardActivity,
                            getUserDetails()?.userName + " received level up bonus $amount coins reward..!"
                        )
                    }

                })
            levelUpDialog.setCancelable(false)
            levelUpDialog.show()
        } catch (e: Exception) {
            
        }
    }

    /**
     * show Level Up Dialog
     */
    private fun showInAppPurchaseDialog() {
        val inAppPurchaseDialog = InAppPurchaseDialog(this@DashboardActivity,
            object : OnInAppPurchaseCallBack {
                override fun onBeginnerPackClick(billingId: String) {
                    manageInApp(billingId, 0)
                }

                override fun onValuePackClick(billingId: String) {
                    manageInApp(billingId, 1)
                }

                override fun onDeluxPackClick(billingId: String) {
                    manageInApp(billingId, 2)
                }

                override fun onCallBackReturn(stringValue: String) {
                }

            })
        inAppPurchaseDialog.setCancelable(false)
        inAppPurchaseDialog.show()
    }

    private fun manageInApp(billingId: String, position: Int) {
//        showToast(this@DashboardActivity, billingId)

        if (billingClient.isReady) {

            val skuList = ArrayList<String>()
            skuList.add(PURCHASE_ID_BEGINNER_PACK)
            skuList.add(PURCHASE_ID_VALUE_PACK)
            skuList.add(PURCHASE_ID_DELUX_PACK)

            val params = SkuDetailsParams
                .newBuilder()
                .setSkusList(skuList)
                .setType(BillingClient.SkuType.INAPP)
                .build()



            billingClient.querySkuDetailsAsync(params, object : SkuDetailsResponseListener {
                override fun onSkuDetailsResponse(
                    billingResult: BillingResult?,
                    skuDetailsList: MutableList<SkuDetails>?
                ) {
                    if (billingResult?.responseCode == BillingClient.BillingResponseCode.OK) {

                        if (skuDetailsList?.size ?: 0 > 0) {
                            val billingFlowParams = BillingFlowParams
                                .newBuilder()
                                .setSkuDetails(skuDetailsList?.get(position))
                                .build()
                            billingClient.launchBillingFlow(
                                this@DashboardActivity,
                                billingFlowParams
                            )

                            purchaseID = skuList.get(position)
                        }
                    } else {
                        showLog(
                            "In App",
                            "Can't querySkuDetailsAsync, responseCode: ${billingResult?.responseCode}"
                        )
                    }
                }
            })

        } else {
            showLog("In App", "Billing client not ready")
        }
    }

    private fun setUpBilling() {
        billingClient = BillingClient
            .newBuilder(this)
            .enablePendingPurchases()
            .setListener(
                object : PurchasesUpdatedListener {
                    override fun onPurchasesUpdated(
                        billingResult: BillingResult?,
                        purchaseList: MutableList<Purchase>?
                    ) {
                        showLog("In App", "purchaseList size" + purchaseList?.size)
                        if (billingResult!!.responseCode == BillingClient.BillingResponseCode.OK && purchaseList != null) {
                            showLog("In App", "BillingResponseCode.OK")
                            for (purchase in purchaseList) {
                                //handleSuccess();
                                Log.e("In App", "onSkuDetailsResponse: $purchase")
                                Log.e("Purchase JSON", "==>" + purchase.originalJson)

                                var amount: String = "0"

                                when (purchaseID) {
                                    PURCHASE_ID_BEGINNER_PACK -> {
                                        amount = 100000.toString()
                                    }
                                    PURCHASE_ID_VALUE_PACK -> {
                                        amount = 1000000.toString()
                                    }
                                    PURCHASE_ID_DELUX_PACK -> {
                                        amount = 10000000.toString()
                                    }
                                }

                                showLog("amount", amount)
                                showLog(
                                    "billingResult?.responseCode",
                                    billingResult.responseCode.toString()
                                )
                                showLog(
                                    "BillingClient.BillingResponseCode.OK",
                                    BillingClient.BillingResponseCode.OK.toString()
                                )

                                if (amount != "0") {
                                    /*  addUserCoins(
                                          getUserDetails()?.userID.toString(),
                                          amount,
                                          GAME_TYPE_IN_APP,
                                          COINS_STATUS_WIN_OR_ADD,
                                          getUserDetails()?.userID.toString(),
                                          "0"
                                      )*/

                                    showYourRewardsDialog(FROM_IN_APP, GAME_TYPE_IN_APP, amount, "0")
                                }
                                consumeProduct(purchase, billingResult.responseCode)
                            }
                        } else if (billingResult.responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
                            // Handle an error caused by a user cancelling the purchase flow.
                            Log.e("In App", "USER_CANCELED: ")
                        } else if (BillingClient.BillingResponseCode.ITEM_NOT_OWNED == billingResult.responseCode) { //this condition was missing
                            // update records if required or ask to buy
                            showLog("In App", "BillingResponseCode.ITEM_NOT_OWNED")
                        } else if (BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED == billingResult.responseCode) {
                            showLog("In App", "BillingResponseCode.ITEM_ALREADY_OWNED")
                        } else {
                            // Handle any other error codes.
                            Log.e("In App", "purchase error: ")
                        }
                    }
                }).build()



        setupBillingClient()

        clearHistory()
    }

    private fun clearHistory() {

        if (billingClient.queryPurchases(BillingClient.SkuType.INAPP).purchasesList != null &&
            billingClient.queryPurchases(BillingClient.SkuType.INAPP).purchasesList.size > 0
        ) {

            billingClient.queryPurchases(BillingClient.SkuType.INAPP).purchasesList
                .forEach {

                    it.let {
                        val consumeParams = ConsumeParams
                            .newBuilder()
                            .setPurchaseToken(it.purchaseToken)
                            .build()

                        billingClient.consumeAsync(consumeParams) { billingResult, purchaseToken ->
                            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && purchaseToken != null) {
                                println("onPurchases Updated consumeAsync, purchases token removed: $purchaseToken")
                            } else {
                                println("onPurchases some troubles happened: $billingResult")
                            }
                        }
                    }
                }
        }
    }

    private fun consumeProduct(purchase: Purchase?, responseCode_: Int) {
        purchase?.let {
            val consumeParams = ConsumeParams
                .newBuilder()
                .setPurchaseToken(purchase.purchaseToken)
                .build()

            val responseCode =
                billingClient.consumeAsync(consumeParams, object : ConsumeResponseListener {
                    override fun onConsumeResponse(
                        billingResult: BillingResult?,
                        purchaseToken: String?
                    ) {
                        if (billingResult?.responseCode == BillingClient.BillingResponseCode.OK &&
                            responseCode_ == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED
                        ) {
                            //   handlePurchase(purchase)
                        }
                    }
                })
            return
        }
    }

    private fun setupBillingClient() {
        billingClient.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(billingResult: BillingResult) {
                if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                    // The BillingClient is ready. You can query purchases here.
                    showLog("In App", "Success to connect Billing")
                } else {
                    showLog(
                        "In App",
                        "Failure to connect Billing code : ${billingResult.responseCode}"
                    )
                }
            }

            override fun onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
                showLog("In App", "Billing disconnected")
            }
        })
    }

    /**
     * show Spin Wheel Dialog
     */
    private fun showSpinWheelDialog() {
        spinWheelDialog = SpinWheelDialog(this@DashboardActivity,
            object : OnSpinWheelDialogCallBack {
                override fun onEmptyCallBak() {
                }

                override fun onCallBakeWithSpinWheelValue(amount: String, isXpOrCoin: String) {
                    Handler().postDelayed({

                        showYourRewardsDialog(
                            FROM_SPINNER_WHEEL,
                            GAME_TYPE_SPINNER_WHEEL,
                            amount,
                            isXpOrCoin
                        )
                        CrashPotApplication.instance.preferenceData?.setValueLong(
                            PREF_TIMER_WHEEL_SPIN,
                            (System.currentTimeMillis() + MILLISECONDS_6_HOURS)
                        )
                        showSpinWheelTimer()
                        setSpinWheelAlarm(
                            this@DashboardActivity,
                            (System.currentTimeMillis() + MILLISECONDS_6_HOURS)
                        )
                        spinWheelDialog.dismiss()

                    }, 1000)
                }
            })
        spinWheelDialog.setCancelable(false)
        spinWheelDialog.show()
    }

    /**
     * Show Your Rewards Dialog
     */
    private fun showYourRewardsDialog(
        isFrom: String,
        gameType: String,
        amount: String,
        xpOrCoin: String
    ) {

        val yourRewardsDialog = YourRewardsDialog(isFrom, amount, xpOrCoin, this@DashboardActivity,
            object : OnShareAndRewardDialogCallBack {
                override fun onEmptyCallBak() {
                    addUserCoins(
                        getUserDetails()?.userID.toString(),
                        amount,
                        gameType,
                        COINS_STATUS_WIN_OR_ADD,
                        getUserDetails()?.userID.toString(),
                        xpOrCoin
                    )
                }

                override fun onCallBakeWithOneString(winAmount: String) {
                    addUserCoins(
                        getUserDetails()?.userID.toString(),
                        winAmount,
                        gameType,
                        COINS_STATUS_WIN_OR_ADD,
                        getUserDetails()?.userID.toString(),
                        xpOrCoin
                    )
                }

                override fun onCallBakeWithString(amount: String, isXpOrCoin: String) {
                    shareOnFacebookAsPost(
                        this@DashboardActivity,
                        getUserDetails()?.userName + " Wins : $amount $isXpOrCoin reward..!"
                    )
                }
            })
        yourRewardsDialog.setCancelable(false)
        yourRewardsDialog.show()
    }

    /**
     * show Strategy And Amount Dialog
     */
    private fun showStrategyAndAmountDialog(dialogType: String) {
        val strategyAndAmountDialog =
            StrategyAndAmountDialog(this@DashboardActivity,
                dialogType,
                object : OnCallBack {
                    override fun onCallBackReturn(stringValue: String) {
                        if (dialogType == getString(R.string.my_bet)) {
                            CrashPotApplication.instance.preferenceData?.setValue(
                                PREF_MY_BET_VALUE,
                                stringValue
                            )
                            txtMyBetValue.text = stringValue
                        } else {
                            CrashPotApplication.instance.preferenceData?.setValue(
                                PREF_STRATEGY_VALUE,
                                stringValue
                            )
                            txtStrategyValue.text = stringValue + "x"
                        }
                    }
                })
        strategyAndAmountDialog.setCancelable(true)
        strategyAndAmountDialog.show()
    }

    // Line Chart  Methods start
    private fun setUpChart() {

        chart.setTouchEnabled(false)
        chart.isDragEnabled = false
        chart.setScaleEnabled(false)
        chart.setPinchZoom(false)

        chart.description.isEnabled = false
        chart.setBackgroundColor(Color.TRANSPARENT)
        chart.setDrawGridBackground(false)
        chart.setDrawBarShadow(false)
        chart.isHighlightFullBarEnabled = false

        // draw bars behind lines
        chart.drawOrder = arrayOf(CombinedChart.DrawOrder.LINE, CombinedChart.DrawOrder.SCATTER)

        chart.legend.isEnabled = false // hide legend

        val xl: XAxis = chart.xAxis
        xl.position = XAxis.XAxisPosition.BOTTOM
        xl.textColor = Color.WHITE
        xl.setDrawGridLines(false)
        xl.axisMinimum = 1f
        xl.setDrawLabels(false)
        xl.isEnabled = true

        val yAxisLeft: YAxis = chart.getAxisLeft()
        yAxisLeft.textColor = Color.WHITE
        yAxisLeft.axisMinimum = 1f
        yAxisLeft.setDrawGridLines(false)

        chart.axisRight.axisMinimum = 1f
        chart.axisRight.isEnabled = false // hide axisRight

        val data = CombinedData()
        data.setData(generateScatterData())
        data.setData(LineData())
        chart.data = data
        chart.invalidate()

        addLineEntry(1f, maxValue) // TODo default point
    }

    //    Scatter Chart Start
    private fun generateScatterData(): ScatterData? {
        val d = ScatterData()
        val entries = ArrayList<Entry>()
        entries.add(Entry(0f, 0f))
        val set = ScatterDataSet(entries, "Scatter DataSet")
        set.setColors(Color.parseColor("#D550F8"))
        set.scatterShapeSize = 25f
        set.setScatterShape(ScatterChart.ScatterShape.CIRCLE)
        set.setDrawValues(true)
        set.valueFormatter = object : ValueFormatter() {
            override fun getPointLabel(entry: Entry): String {
                return "${map.get(entry.y)} @ ${entry.y} x"
            }
        }
        set.scatterShapeHoleRadius = 0f
        set.valueTextSize = 10f
        set.valueTextColor = Color.WHITE
        d.addDataSet(set)
        return d
    }

    private fun createSetForScattered(): ScatterDataSet {
        val set = ScatterDataSet(null, "")
        set.setColors(Color.parseColor("#D550F8"))
        set.scatterShapeSize = 25f
        set.setScatterShape(ScatterChart.ScatterShape.CIRCLE)
        set.setDrawValues(false)
        set.scatterShapeHoleRadius = 0f
        set.valueTextSize = 10f
        set.valueFormatter = object : ValueFormatter() {
            override fun getPointLabel(entry: Entry?): String {
                return "${entry?.y} XP"
            }
        }
        set.valueTextColor = Color.WHITE
        return set
    }

    private fun addScatterData(xVal: String, yVal: String) {
        val data = chart.data.scatterData
        if (data != null) {
            var set = data.getDataSetByIndex(0)
            if (set == null) {
                set = createSetForScattered()
                data.addDataSet(set)
            }
        }
        data!!.addEntry(Entry(xVal.toFloat(), yVal.toFloat()), 0)
        data.notifyDataChanged()
        chart.notifyDataSetChanged()
        chart.invalidate()
    }
    //    Scatter Chart ends

    @SuppressLint("SetTextI18n")
    private fun addLineEntry(xValue: Float, finalValue: Float) {
        val data = chart.data.lineData
        if (data != null) {
            var set = data.getDataSetByIndex(0)
            if (set == null) {
                set = createSet1()
                data.addDataSet(set)
            }

            maxValue = finalValue
            //            xAxis = xValue
            //            yAxis = 2 * (xAxis * xAxis)
            xAxis = sqrt(xValue.toDouble()).toFloat()
            yAxis = xValue

            data.addEntry(Entry(xAxis, yAxis), 0)
            data.notifyDataChanged()
            chart.notifyDataSetChanged() // let the chart know it's data has changed
            chart.moveViewTo(
                data.entryCount.toFloat(),
                data.entryCount.toFloat(),
                YAxis.AxisDependency.LEFT
            )

//            chart.xAxis.axisMaximum = sqrt(maxValue.toDouble()).toFloat()
            if (xAxis < 1.8f)
                chart.xAxis.axisMaximum = 1.8f
            else
                chart.xAxis.axisMaximum = xAxis

            if (yAxis < 3.5f)
                chart.axisLeft.axisMaximum = 3.5f // maxValue
            else
                chart.axisLeft.axisMaximum = yAxis // maxValue

            txtProgress.text = String.format("%.2f", yAxis) + "x"
            //            showLog("check : ", "xAxis : $xAxis | yAxis : $yAxis")
            chart.invalidate()
        }
    }

    private fun resetChart() {
        chart.clear()
        chart.invalidate()
        setUpChart()
    }

    private fun createSet1(): ILineDataSet? {
        val set = LineDataSet(null, "")
        set.axisDependency = YAxis.AxisDependency.LEFT
        set.color = Color.parseColor("#D550F8")
        set.setCircleColor(Color.parseColor("#D550F8"))
        set.lineWidth = 4f
        set.circleRadius = 4f
        set.fillAlpha = 10
        set.valueTextColor = Color.WHITE
        set.valueTextSize = 9f
        set.setDrawCircles(false)
        set.setDrawValues(false)
        set.setDrawFilled(true)
        set.setDrawCircleHole(false)
        set.mode = LineDataSet.Mode.CUBIC_BEZIER
        set.setDrawFilled(true)
        val drawable = ContextCompat.getDrawable(this, R.drawable.fade_purple)
        set.fillDrawable = drawable
        return set
    }
    // Line Chart  Methods end

    //  SOCKET IO Connection related
    private fun connectSocketIO() {
        socket = IO.socket(SOCKET_IO_URL);

        socket.on(Socket.EVENT_CONNECT) {
            showLog("@@@ EVENT_CONNECT", "connected to server")
        }

        socket.on("crashStart") { args ->
            val data: JSONObject = args[0] as JSONObject
            receiveSocketDetails(data.toString())
        }

        socket.on("betPointResult") { args ->
            val data: JSONObject = args[0] as JSONObject
            showLog("@@@ betPointResult", data.toString())
            receiveSocketDetails(data.toString())
        }

        socket.connect()
    }

    private fun receiveSocketDetails(message: String) {
        runOnUiThread {
            manageGraphFlow(message)
        }
    }

    private fun manageGraphFlow(message: String) {
        if (!TextUtils.isEmpty(message)) {
            showLog("@@@", message)

            val jsonObj: JSONObject = JSONObject(message)
            val finalValue: String = jsonObj.optString(SOCKET_FINAL_VALUE)
            val xValue: String = jsonObj.optString(SOCKET_X_VALUE)
            val displayValue: String = jsonObj.optString(SOCKET_DISPLAY_VALUE)
            val type: Int = jsonObj.optInt(SOCKET_TYPE)

            if (jsonObj.has("betId") && getUserDetails()?.userID == jsonObj.get("userId"))
                betId = jsonObj.optInt("betId")

            if (jsonObj.has("gameId"))
                gameId = jsonObj.optInt("gameId")

            when (type) {
                SOCKET_TYPE_GRAPH_RUNNING -> {
                    isTimerOn = false
                    clCountDownTimer.visibility = View.GONE
                    txtCrashed.visibility = View.GONE
                    clProgressCounter.setBackgroundResource(R.drawable.ic_game_xp_count_display_bg)
                    clProgressCounter.visibility = View.VISIBLE
                    if (isUserJoin) {
                        txtBet.text = "Cashout"
                        txtBetLine2.visibility = View.VISIBLE
                        txtBetLine2.text =
                            String.format("%.2f", (getMyBetAmount()?.toFloat()?.times(yAxis)))
                        imgBtnBet.setBackgroundResource(R.drawable.img_crashout_button)

                    } else {
                        txtBet.text = "Bet"
                        txtBetLine2.visibility = View.VISIBLE
                        txtBetLine2.text = "wait for next round"
                        imgBtnBet.setBackgroundResource(R.drawable.img_wait_for_bet_button)
                    }
                    if (!TextUtils.isEmpty(xValue) && !TextUtils.isEmpty(finalValue))
                        addLineEntry(xValue.toFloat(), finalValue.toFloat())

                    if (xValue.toFloat() == CrashPotApplication.instance.preferenceData?.getValueFromKey(
                            PREF_STRATEGY_VALUE
                        )?.toFloat()
                    ) {
                        manageBetbutton()
                    }


                    val gson = Gson()
                    val clsCrashStartModel: ClsCrashStartModel = gson.fromJson(
                        jsonObj.toString(),
                        object : TypeToken<ClsCrashStartModel?>() {}.type
                    )
                    // For alternate name changing flow
                    /*  if (clsCrashStartModel.randomUser != null && clsCrashStartModel.randomUser!!.isNotEmpty()) {
                          clGameUserAndDetails.visibility = View.VISIBLE

                          var pos: Int = Random().nextInt((clsCrashStartModel.randomUser?.size?.minus(1))?.plus(1)!!) + 1
                          pos = pos.minus(1)

                          if (clsCrashStartModel.randomUser!![pos].status == 2) {
                              if (getUserDetails()?.userID == clsCrashStartModel.randomUser!![pos].id.toString()) {
                                  txtUserNameWithDetails.text = "you bet"
                                  txtTotalCoins.text = changeCoinsFormat(clsCrashStartModel.randomUser!![pos].betAmount)
                              } else {
                                  if (isUserWonGame) {
                                      txtUserNameWithDetails.text = "you won"
                                      txtWinXpCount.text = userXps
                                      txtTotalCoins.text = userCoins
                                      txtWinXpCount.visibility = View.VISIBLE
                                  } else {
                                      txtUserNameWithDetails.text = clsCrashStartModel.randomUser!![pos].name + " bets"
                                      txtWinXpCount.visibility = View.GONE
                                      txtTotalCoins.text = changeCoinsFormat(clsCrashStartModel.randomUser!![pos].betAmount)
                                  }
                              }
                          } else {
                              imgUserWonBg.visibility = View.VISIBLE
                              addScatterData(clsCrashStartModel.randomUser!![pos].betAt, clsCrashStartModel.randomUser!![pos].yAxis)

                              if (getUserDetails()?.userID == clsCrashStartModel.randomUser!![pos].id.toString()) {
                                  txtUserNameWithDetails.text = "you won"
                                  isUserWonGame = true
                                  userXps = "+" + changeCoinsFormat(clsCrashStartModel.randomUser!![pos].calculate_xp)
                                  userCoins = changeCoinsFormat(clsCrashStartModel.randomUser!![pos].calculateAmount)

                                  txtWinXpCount.text = userXps
                                  txtTotalCoins.text = userCoins

                                  if (getInterstitialAdCount() == INTERSTITIAL_AD_MAX_VALUE) {
                                      loadInterstitialAd()
                                  }
                              } else {
                                  if (!isUserWonGame) {
                                      txtUserNameWithDetails.text = clsCrashStartModel.randomUser!![pos].name + " won"
                                      txtWinXpCount.text = "+" + changeCoinsFormat(clsCrashStartModel.randomUser!![pos].calculate_xp)
                                      txtTotalCoins.text = changeCoinsFormat(clsCrashStartModel.randomUser!![pos].calculateAmount)
                                  } else {
                                      txtUserNameWithDetails.text = "you won"
                                      txtWinXpCount.text = userXps
                                      txtTotalCoins.text = userCoins
                                  }
                              }

                              map.put(clsCrashStartModel.randomUser!![pos].yAxis.toFloat(), clsCrashStartModel.randomUser!![pos].name)
                              txtWinXpCount.visibility = View.VISIBLE
                          }
                      }*/

                    if (clsCrashStartModel.lastUser != null && clsCrashStartModel.randomUser != null && clsCrashStartModel.randomUser!!.isNotEmpty()) {
                        clGameUserAndDetails.visibility = View.VISIBLE

                        if (clsCrashStartModel.lastUser?.status == 2) {
                            if (getUserDetails()?.userID == clsCrashStartModel.lastUser?.id.toString()) {
                                txtUserNameWithDetails.text = "you bet"
                                txtTotalCoins.text =
                                    changeCoinsFormat(clsCrashStartModel.lastUser?.betAmount.toString())
                            } else {
                                if (isUserWonGame) {
                                    txtUserNameWithDetails.text = "you won"
                                    txtWinXpCount.text = userXps
                                    txtTotalCoins.text = userCoins
                                    txtWinXpCount.visibility = View.VISIBLE
                                } else {
                                    txtUserNameWithDetails.text =
                                        clsCrashStartModel.lastUser?.name + " bets"
                                    txtWinXpCount.visibility = View.GONE
                                    txtTotalCoins.text =
                                        changeCoinsFormat(clsCrashStartModel.lastUser?.betAmount.toString())
                                }
                            }
                        } else {
                            imgUserWonBg.visibility = View.VISIBLE
                            addScatterData(
                                clsCrashStartModel.lastUser?.betAt.toString(),
                                clsCrashStartModel.lastUser?.yAxis.toString()
                            )

                            if (getUserDetails()?.userID == clsCrashStartModel.lastUser?.id.toString()) {
                                txtUserNameWithDetails.text = "you won"
                                isUserWonGame = true
                                userXps =
                                    "+" + changeCoinsFormat(clsCrashStartModel.lastUser?.calculate_xp.toString())
                                userCoins =
                                    changeCoinsFormat(clsCrashStartModel.lastUser?.calculateAmount.toString())

                                txtWinXpCount.text = userXps
                                txtTotalCoins.text = userCoins

                                if (getInterstitialAdCount() == INTERSTITIAL_AD_MAX_VALUE) {
                                    loadInterstitialAd()
                                }
                            } else {
                                if (!isUserWonGame) {
                                    txtUserNameWithDetails.text =
                                        clsCrashStartModel.lastUser?.name + " won"
                                    txtWinXpCount.text =
                                        "+" + changeCoinsFormat(clsCrashStartModel.lastUser?.calculate_xp.toString())
                                    txtTotalCoins.text =
                                        changeCoinsFormat(clsCrashStartModel.lastUser?.calculateAmount.toString())
                                } else {
                                    txtUserNameWithDetails.text = "you won"
                                    txtWinXpCount.text = userXps
                                    txtTotalCoins.text = userCoins
                                }
                            }

                            map.put(
                                clsCrashStartModel.lastUser?.yAxis!!.toFloat(),
                                clsCrashStartModel.lastUser?.name.toString()
                            )
                            txtWinXpCount.visibility = View.VISIBLE
                        }
                    }
                }

                SOCKET_TYPE_COUNT_DOWN -> {
                    map.clear()
                    resetChart()
                    imgUserWonBg.visibility = View.INVISIBLE
                    clGameUserAndDetails.visibility = View.INVISIBLE
                    txtWinXpCount.visibility = View.GONE
                    clProgressCounter.setBackgroundResource(R.drawable.ic_game_xp_count_display_bg)
                    clProgressCounter.visibility = View.GONE
                    isTimerOn = true
                    if (isUserJoin) {
                        status = 3
                        txtBet.text = "Betting"
                        txtBetLine2.visibility = View.VISIBLE
                        txtBetLine2.text = "Cancel"
                        imgBtnBet.setBackgroundResource(R.drawable.img_start_betting_button)
                    } else {
                        status = 2
                        txtBet.text = "Bet"
                        txtBetLine2.visibility = View.GONE
                        txtBetLine2.text = "Cancel"
                        imgBtnBet.setBackgroundResource(R.drawable.img_start_bet_button)
                    }

                    txtCrashed.visibility = View.GONE

                    imgBtnBet.setBackgroundResource(R.drawable.img_start_bet_button)
                    clCountDownTimer.visibility = View.VISIBLE
                    if (!TextUtils.isEmpty(displayValue))
                        txtCountDown.text = displayValue


                    val gson = Gson()
                    val clsCrashStartModel: ClsCrashStartModel = gson.fromJson(
                        jsonObj.toString(),
                        object : TypeToken<ClsCrashStartModel?>() {}.type
                    )
                    // For alternate name changing flow
                    /* if (clsCrashStartModel.randomUser != null && clsCrashStartModel.randomUser!!.isNotEmpty()) {
                         clGameUserAndDetails.visibility = View.VISIBLE

                         var pos: Int = Random().nextInt((clsCrashStartModel.randomUser?.size?.minus(1))?.plus(1)!!) + 1
                         pos = pos.minus(1)

                         if (clsCrashStartModel.randomUser!![pos].status == 2) {
                             if (getUserDetails()?.userID == clsCrashStartModel.randomUser!![pos].id.toString()) {
                                 txtUserNameWithDetails.text = "you bet"
                                 txtTotalCoins.text = getMyBetAmount()
                             } else {
                                 if (!isUserJoin) {
                                     txtUserNameWithDetails.text = clsCrashStartModel.randomUser!![pos].name + " bets"
                                     txtTotalCoins.text = changeCoinsFormat(clsCrashStartModel.randomUser!![pos].betAmount)
                                 } else {
                                     txtUserNameWithDetails.text = "you bet"
                                     txtTotalCoins.text = getMyBetAmount()
                                 }
                             }
                             txtWinXpCount.visibility = View.GONE
                         }
                     }*/

                    if (clsCrashStartModel.lastUser != null && clsCrashStartModel.randomUser != null && clsCrashStartModel.randomUser!!.isNotEmpty()) {
                        clGameUserAndDetails.visibility = View.VISIBLE

                        if (clsCrashStartModel.lastUser?.status == 2) {
                            if (getUserDetails()?.userID == clsCrashStartModel.lastUser?.id.toString()) {
                                txtUserNameWithDetails.text = "you bet"
                                txtTotalCoins.text = getMyBetAmount()
                            } else {
                                if (!isUserJoin) {
                                    txtUserNameWithDetails.text =
                                        clsCrashStartModel.lastUser?.name + " bets"
                                    txtTotalCoins.text =
                                        changeCoinsFormat(clsCrashStartModel.lastUser?.betAmount.toString())
                                } else {
                                    txtUserNameWithDetails.text = "you bet"
                                    txtTotalCoins.text = getMyBetAmount()
                                }
                            }
                            txtWinXpCount.visibility = View.GONE
                        }
                    }
                }

                SOCKET_TYPE_CRASH_OUT -> {
                    map.clear()
                    txtWinXpCount.visibility = View.GONE
                    if (isUserJoin) {
                        txtUserNameWithDetails.text = "you loss"
                        txtTotalCoins.text = getMyBetAmount()
                    }

                    txtBet.text = "Crashed"
                    txtBetLine2.visibility = View.GONE
                    imgBtnBet.setBackgroundResource(R.drawable.img_crashed_button)
                    clProgressCounter.setBackgroundResource(R.drawable.ic_game_crashed_count_bg)
                    clProgressCounter.visibility = View.VISIBLE
                    txtCrashed.visibility = View.VISIBLE
                    txtProgress.text = "@ ${finalValue}x"
                    clCountDownTimer.visibility = View.GONE

                    isTimerOn = false
                    isUserJoin = false
                    isUserWonGame = false
                    userCoins = ""
                    userXps = ""
                    betId = 0
                    getProfileDashboardData(getUserDetails()?.userID.toString(), false)
                    if (!TextUtils.isEmpty(displayValue))
                        txtProgress.text = displayValue
                }
            }
        } else {
            showLog("@@@", "No message data found")
        }
    }

    // API
    private fun addUserCoins(
        userId: String, coins: String, gameType: String,
        status: String, fromUserId: String,
        isXpOrCoin: String
    ) {
        ApiCallMethods(this@DashboardActivity)
            .addUserCoins(
                userId,
                coins,
                gameType,
                status,
                fromUserId,
                isXpOrCoin,
                object : OnApiCallCompleted<ClsAddCoinsResponse> {
                    override fun apiSuccess(obj: Any?) {
                        if (obj != null) {
                            val clsAddCoinsResponse: ClsAddCoinsResponse =
                                obj as ClsAddCoinsResponse
                            if (clsAddCoinsResponse.success) {
                                getProfileDashboardData(getUserDetails()?.userID.toString(), false)
                            }
                        }
                    }

                    override fun apiFailure(errorMessage: String) {
                        showToast(this@DashboardActivity, errorMessage)
                    }

                    override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                        val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                        showToast(this@DashboardActivity, errorMessage)
                    }
                })
    }

    private fun getProfileDashboardData(userId: String, showProgress: Boolean) {
        ApiCallMethods(this@DashboardActivity)
            .getProfileDashboardData(
                userId,
                showProgress,
                object : OnApiCallCompleted<ClsProfileDashboardResponse> {
                    override fun apiSuccess(obj: Any?) {
                        if (obj != null) {
                            val clsProfileDashboardResponse: ClsProfileDashboardResponse =
                                obj as ClsProfileDashboardResponse

                            if (clsProfileDashboardResponse.success) {
                                txtUserName.text = clsProfileDashboardResponse.data?.userName
                                txtStarCount.text = clsProfileDashboardResponse.data?.rankingByLevel
                                txtUserCoins.text =
                                    changeCoinsFormat(clsProfileDashboardResponse.data?.totalCoins.toString())

                                val totalXpPoints: Int =
                                    clsProfileDashboardResponse.data?.remainXP ?: 0
                                progressBar1.progress = totalXpPoints

                                if (clsProfileDashboardResponse.data?.chatCNT ?: 0 > 99)
                                    txtChatCount.text = "99+"
                                else
                                    txtChatCount.text =
                                        clsProfileDashboardResponse.data?.chatCNT.toString()

                                if (clsProfileDashboardResponse.data?.notificationCNT ?: 0 > 99)
                                    txtNotificationCount.text = "99+"
                                else
                                    txtNotificationCount.text =
                                        clsProfileDashboardResponse.data?.notificationCNT.toString()

                                if (this@DashboardActivity != null && imgProfilePic != null)
                                    setSimpleImage(
                                        this@DashboardActivity,
                                        imgProfilePic,
                                        clsProfileDashboardResponse.data?.userImage.toString()
                                    )


                                val rankingStored: Int = getUserLevel()
                                val rankingNew: Int =
                                    clsProfileDashboardResponse.data?.rankingByLevel?.toInt() ?: 1

                                if (rankingNew > rankingStored) {
                                    showLevelUpDialog(rankingNew)
                                }
                                CrashPotApplication.instance.preferenceData?.setValueInt(
                                    PREF_USER_LEVEL,
                                    rankingNew
                                )
                                CrashPotApplication.instance.preferenceData?.setValue(
                                    PREF_DASHBOARD_USER_DETAIL,
                                    Gson().toJson(clsProfileDashboardResponse.data)
                                )

                                getUserByLevel(RANKING_BY_LEVEL)
                            }
                        }
                    }

                    override fun apiFailure(errorMessage: String) {
                        showToast(this@DashboardActivity, errorMessage)
                    }

                    override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                        val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                        showToast(this@DashboardActivity, errorMessage)
                    }
                })
    }


    //    API CALL
    private fun getUserByLevel(levelType: String) {
        ApiCallMethods(this@DashboardActivity).getUserByLevel(
            levelType, getDashboardUserDetails()?.userID.toString(),
            object : OnApiCallCompleted<ClsUserByLevelResponse> {
                override fun apiSuccess(obj: Any?) {
                    if (obj != null) {
                        val clsUserByLevelResponse: ClsUserByLevelResponse =
                            obj as ClsUserByLevelResponse
                    }
                }

                override fun apiFailure(errorMessage: String) {
                    showToast(this@DashboardActivity, errorMessage)
                }

                override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                    val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                    showToast(this@DashboardActivity, errorMessage)
                }
            })
    }
}