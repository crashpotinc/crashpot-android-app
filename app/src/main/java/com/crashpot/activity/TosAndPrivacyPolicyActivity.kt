package com.crashpot.activity

import android.os.Bundle
import android.text.TextUtils
import android.text.method.ScrollingMovementMethod
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.crashpot.R
import com.crashpot.model.ClsPrivacyPolicyResponse
import com.crashpot.model.ClsTermsAndConditionResponse
import com.crashpot.network.ApiCallMethods
import com.crashpot.network.OnApiCallCompleted
import com.crashpot.util.EXTRAS_TOS_NAME
import com.crashpot.util.MESSAGE_SMALL
import com.crashpot.util.Utility.Companion.showToast
import kotlinx.android.synthetic.main.activity_tos_and_privacy_policy.*
import org.json.JSONObject


class TosAndPrivacyPolicyActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tos_and_privacy_policy)

        var tosNAme: String = getString(R.string.terms_of_service)

        if (intent != null)
            tosNAme = intent.getStringExtra(EXTRAS_TOS_NAME)

        txtHeaderTitle.text = tosNAme

        if (tosNAme == getString(R.string.terms_of_service))
            getTermsAndCondition()
        else
            getPrivacyPolicy()

        txtDescription.movementMethod = ScrollingMovementMethod()
        imgBack.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.imgBack -> onBackPressed()
        }
    }

    // API call
    private fun getTermsAndCondition() {
        ApiCallMethods(this@TosAndPrivacyPolicyActivity)
            .getTermsAndCondition(object : OnApiCallCompleted<ClsTermsAndConditionResponse> {
                override fun apiSuccess(obj: Any?) {
                    if (obj != null) {
                        val clsTermsAndConditionResponse: ClsTermsAndConditionResponse =
                            obj as ClsTermsAndConditionResponse

                        if (clsTermsAndConditionResponse.data != null &&
                            !TextUtils.isEmpty(clsTermsAndConditionResponse.data!!.termsAndConditions)) {

                            txtDescription.text = clsTermsAndConditionResponse.data?.termsAndConditions
                        }
                    }
                }

                override fun apiFailure(errorMessage: String) {
                    showToast(this@TosAndPrivacyPolicyActivity, errorMessage)
                }

                override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                    val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                    showToast(this@TosAndPrivacyPolicyActivity, errorMessage)
                }
            })
    }

    private fun getPrivacyPolicy() {
        ApiCallMethods(this@TosAndPrivacyPolicyActivity)
            .getPrivacyPolicy(object : OnApiCallCompleted<ClsTermsAndConditionResponse> {
                override fun apiSuccess(obj: Any?) {
                    if (obj != null) {
                        val clsPrivacyPolicyResponse: ClsPrivacyPolicyResponse =
                            obj as ClsPrivacyPolicyResponse

                        if (clsPrivacyPolicyResponse.data != null &&
                            !TextUtils.isEmpty(clsPrivacyPolicyResponse.data!!.privacyPolicy)) {

                            txtDescription.text = clsPrivacyPolicyResponse.data?.privacyPolicy
                        }
                    }
                }

                override fun apiFailure(errorMessage: String) {
                    showToast(this@TosAndPrivacyPolicyActivity, errorMessage)
                }

                override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                    val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                    showToast(this@TosAndPrivacyPolicyActivity, errorMessage)
                }
            })
    }

}
