package com.crashpot.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import com.crashpot.CrashPotApplication
import com.crashpot.R
import com.crashpot.base.BaseActivity
import com.crashpot.dialog.*
import com.crashpot.interfaces.OnCallBack
import com.crashpot.interfaces.OnShareDialogCallBack
import com.crashpot.model.*
import com.crashpot.network.ApiCallMethods
import com.crashpot.network.OnApiCallCompleted
import com.crashpot.util.*
import com.crashpot.util.Utility.Companion.changeCoinsFormat
import com.crashpot.util.Utility.Companion.getDashboardUserDetails
import com.crashpot.util.Utility.Companion.getUserDetails
import com.crashpot.util.Utility.Companion.shareOnFacebookAsPost
import com.crashpot.util.Utility.Companion.showLog
import com.crashpot.util.Utility.Companion.showToast
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_profile.*
import org.json.JSONException
import org.json.JSONObject

class ProfileActivity : BaseActivity() {

    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var callbackManager: CallbackManager
    private var userID: String = getDashboardUserDetails()?.userID.toString()
    private var isOtherUser: Boolean = false

    override fun getLayoutResId(): Int = R.layout.activity_profile

    override fun initViews() {
        txtUserName.text = getUserDetails()?.userName

        if (intent != null)
            userID = intent.extras?.get(EXTRAS_USER_ID).toString()

        isOtherUser = userID != getDashboardUserDetails()?.userID.toString()

        if (isOtherUser) {
            getUserProfile(userID)
        } else {
            getProfileDashboardData(userID, true)
        }

        if (userID == getDashboardUserDetails()?.userID.toString()) {
            if (CrashPotApplication.instance.preferenceData?.getValueBooleanFromKey(PREF_IS_USER_LOGGED_IN)!!) {
                updateUI()
            } else {
                loginWithFacebook()
                loginWithGoogleSettings()
            }
            imgEditUserProfile.visibility = View.VISIBLE
        } else {
            updateGuestUserUI()
            imgEditUserProfile.visibility = View.GONE
        }
    }

    private fun updateUI() {
        txtLogOut.visibility = View.VISIBLE
        groupLogIn.visibility = View.GONE
        txtUserName.text = getUserDetails()?.userName
    }

    private fun updateGuestUserUI() {
        txtLogOut.visibility = View.GONE
        groupLogIn.visibility = View.GONE
        txtUserName.text = getUserDetails()?.userName
    }

    override fun setListeners() {
        clGmailButtonProfile.setOnClickListener {
            loginWithGmailButtonClick()
        }

        clFacebookButtonProfile.setOnClickListener {
            loginwithFacebookButtonClick()
        }

        imgEditUserProfile.setOnClickListener {
            showUpdateUserName(txtUserName.text.toString())
        }

        imgBack.setOnClickListener { onBackPressed() }

        txtLogOut.setOnClickListener {
            logOutDialog()
        }
    }

    private fun loginwithFacebookButtonClick() {
        LoginManager.getInstance().logInWithReadPermissions(this, listOf("email"))
    }

    private fun loginWithGmailButtonClick() {
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, GOOGLE_SIGN_IN_CODE)
    }

    /**
     * show Update User Name
     */
    private fun showUpdateUserName(dialogType: String) {
        val updateUserNameDialog =
            UpdateUserNameDialog(this@ProfileActivity,
                dialogType,
                object : OnCallBack {
                    override fun onCallBackReturn(stringValue: String) {
                        updateUserName(getUserDetails()?.userID.toString(), stringValue)
                    }
                })
        updateUserNameDialog.setCancelable(true)
        updateUserNameDialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GOOGLE_SIGN_IN_CODE) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    private fun loginWithFacebook() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                val loggedIn = AccessToken.getCurrentAccessToken() == null
                showLog("Facebook", "onSuccess $loggedIn")
                getUserProfileFacebook(AccessToken.getCurrentAccessToken())
            }

            override fun onCancel() {
                showLog("Facebook", "onCancel")
            }

            override fun onError(error: FacebookException?) {
                showLog("Facebook", "onError $error")
            }
        })
    }

    private fun loginWithGoogleSettings() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val acct: GoogleSignInAccount? = completedTask.getResult(ApiException::class.java)
            //            showToast(this, "Sign in successfully")
            getUserProfileGoogle(acct)
        } catch (e: ApiException) {
            showLog("Google Sign In Error", "signInResult:failed code=" + e.statusCode)
            //            Toast.makeText(this@ProfileActivity, "Failed", Toast.LENGTH_LONG).show()
        }
    }

    private fun getUserProfileFacebook(currentAccessToken: AccessToken) {
        val request = GraphRequest.newMeRequest(
            currentAccessToken
        ) { `object`, response ->
            Log.d("TAG", `object`.toString())

            if (`object` != null) {
                try {
                    val first_name = `object`.getString("first_name")
                    val last_name = `object`.getString("last_name")
                    val email = `object`.getString("email")
                    val id = `object`.getString("id")
                    val image_url = "https://graph.facebook.com/$id/picture?type=normal"
                    showLog("Facebook", "First Name: $first_name")
                    showLog("Facebook", "Last Name: $last_name")
                    showLog("Facebook", "email: $email")
                    showLog("Facebook", "id : $id")
                    showLog("Facebook", "image_url : $image_url")

                    isUserRegistered("$first_name $last_name", image_url, email, LOGIN_WITH_FACEBOOK, id.toString(), getUserDetails()?.userID.toString())
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            } else {
                txtLogOut.visibility = View.GONE
                groupLogIn.visibility = View.VISIBLE
            }
        }
        val parameters = Bundle()
        parameters.putString("fields", "first_name,last_name,email,id")
        request.parameters = parameters
        request.executeAsync()
    }

    private fun getUserProfileGoogle(acct: GoogleSignInAccount?) {
        if (acct != null) {
            val personName: String? = acct.displayName
            val personGivenName: String? = acct.givenName
            val personFamilyName: String? = acct.familyName
            val personEmail: String? = acct.email
            val personId: String? = acct.id
            val personPhoto: Uri? = acct.photoUrl
            showLog("google", "Name: $personName")
            showLog("google", "Email: $personEmail")
            showLog("google", "ID: $personId")
            showLog("google", "URL : $personPhoto")
            showLog("google", "personGivenName : $personGivenName")
            showLog("google", "personFamilyName : $personFamilyName")

            isUserRegistered(personName.toString(), personPhoto.toString(), personEmail.toString(), LOGIN_WITH_GOOGLE, personId.toString(), getUserDetails()?.userID.toString())
        } else {
            txtLogOut.visibility = View.GONE
            groupLogIn.visibility = View.VISIBLE
        }
    }

    /**
     * show Level Up Dialog
     */
    private fun showLevelUpDialog(rankingByLevel: Int?) {
        val levelUpDialog = LevelUpDialog(rankingByLevel, this@ProfileActivity,
            object : OnShareDialogCallBack {
                override fun onEmptyCallBak() {
                }

                override fun onCallBakeWithString(amount: String, isXpOrCoin: String) {
                    shareOnFacebookAsPost(this@ProfileActivity, getUserDetails()?.userName + " received level up bonus $amount coins reward..!")
                }

            })
        levelUpDialog.setCancelable(false)
        levelUpDialog.show()
    }

    /**
     * show Logout Dialog
     */
    private fun logOutDialog() {
        val logOutDialog = LogOutAndLoadDataDialog(this@ProfileActivity,
            object : OnCallBack {
                override fun onCallBackReturn(stringValue: String) {
                    logout(getDashboardUserDetails()?.userID.toString())
                }
            })
        logOutDialog.setCancelable(false)
        logOutDialog.show()
    }

    /**
     * show Logout Dialog
     */
    private fun loadDataDialog(
        name: String,
        avatar: String,
        email: String,
        socialMediaType: String,
        socialMediaId: String,
        userID: String,
        is_register: String?,
        oldUserId: String?
    ) {
        val loadDataDialog = LoadDataDialog(this@ProfileActivity,
            object : OnCallBack {
                override fun onCallBackReturn(stringValue: String) {
                    login(name, avatar, email, socialMediaType, socialMediaId, userID, is_register, oldUserId)
                }
            })
        loadDataDialog.setCancelable(false)
        loadDataDialog.show()
    }

    // API
    private fun login(
        name: String,
        avatar: String,
        email: String,
        socialMediaType: String,
        socialMediaId: String,
        userID: String,
        is_register: String?,
        oldUserId: String?
    ) {
        ApiCallMethods(this@ProfileActivity)
            .login(name, avatar, email, socialMediaType, socialMediaId, userID, is_register, oldUserId,
                object : OnApiCallCompleted<ClsLoginResponse> {
                    override fun apiSuccess(obj: Any?) {
                        if (obj != null) {
                            val clsLoginResponse: ClsLoginResponse = obj as ClsLoginResponse
                            if (clsLoginResponse.success) {
                                CrashPotApplication.instance.preferenceData?.setValueBoolean(PREF_IS_GUEST_USER, false)
                                CrashPotApplication.instance.preferenceData?.setValueBoolean(PREF_IS_USER_LOGGED_IN, true)

                                val clsGuestRandomNumberResponse: ClsGuestRandomNumberResponse.Data? = getUserDetails()
                                clsGuestRandomNumberResponse?.userName = name
                                clsGuestRandomNumberResponse?.socialMediaType = socialMediaType
                                CrashPotApplication.instance.preferenceData?.setValue(PREF_USER_DETAIL, Gson().toJson(clsGuestRandomNumberResponse))
                                CrashPotApplication.instance.preferenceData?.setValueInt(PREF_USER_LEVEL, clsLoginResponse.data?.rankingByLevel!!)
                                updateUI()
                                getProfileDashboardData(clsLoginResponse?.data?.userID.toString(), true)
                            }
                        }
                    }

                    override fun apiFailure(errorMessage: String) {
                        showToast(this@ProfileActivity, errorMessage)
                    }

                    override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                        if (code == RESPONSE_CODE_401) {
                            showAlreadyLoggedInDialog(name, avatar, email, socialMediaType, socialMediaId, userID, is_register, oldUserId)
                        } else {
                            val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                            showToast(this@ProfileActivity, errorMessage)
                        }
                    }
                })
    }

    /**
     * show Already Logged In Dialog
     */
    private fun showAlreadyLoggedInDialog(
        name: String,
        avatar: String,
        email: String,
        socialMediaType: String,
        socialMediaId: String,
        userID: String,
        is_register: String?,
        oldUserId: String?
    ) {
        val alreadyLoggedInDialog = AlreadyLoggedInDialog(this@ProfileActivity,
            object : OnCallBack {
                override fun onCallBackReturn(stringValue: String) {
                    logoutAndNewUserLogin(name, avatar, email, socialMediaType, socialMediaId, userID, is_register, oldUserId)
                }
            })
        alreadyLoggedInDialog.setCancelable(false)
        alreadyLoggedInDialog.show()
    }

    private fun updateUserName(
        userId: String,
        userName: String
    ) {
        ApiCallMethods(this@ProfileActivity)
            .updateUserName(userId, userName,
                object : OnApiCallCompleted<ClsUpdateUserNameResponse> {
                    override fun apiSuccess(obj: Any?) {
                        if (obj != null) {
                            val clsUpdateUserNameResponse: ClsUpdateUserNameResponse = obj as ClsUpdateUserNameResponse
                            if (clsUpdateUserNameResponse.success) {
                                val clsGuestRandomNumberResponse: ClsGuestRandomNumberResponse.Data? = getUserDetails()
                                clsGuestRandomNumberResponse?.userName = clsUpdateUserNameResponse.data?.userName.toString()
                                CrashPotApplication.instance.preferenceData?.setValue(PREF_USER_DETAIL, Gson().toJson(clsGuestRandomNumberResponse))

                                getProfileDashboardData(clsGuestRandomNumberResponse?.userID.toString(), true)
                            }
                        }
                    }

                    override fun apiFailure(errorMessage: String) {
                        showToast(this@ProfileActivity, errorMessage)
                    }

                    override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                        val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                        showToast(this@ProfileActivity, errorMessage)
                    }
                })
    }

    private fun getProfileDashboardData(userId: String, showProgress: Boolean) {
        ApiCallMethods(this@ProfileActivity).getProfileDashboardData(
            userId, showProgress,
            object : OnApiCallCompleted<ClsProfileDashboardResponse> {
                override fun apiSuccess(obj: Any?) {
                    if (obj != null) {
                        val clsProfileDashboardResponse: ClsProfileDashboardResponse = obj as ClsProfileDashboardResponse

                        if (clsProfileDashboardResponse.success) {
                            txtUserName.text = clsProfileDashboardResponse.data?.userName
                            txtStarCount.text = clsProfileDashboardResponse.data?.rankingByLevel

                            val totalXpPoints: Int = clsProfileDashboardResponse.data?.remainXP ?: 0
                            progressBar1.progress = totalXpPoints

                            if (this@ProfileActivity != null && imgProfilePic != null)
                                Utility.setSimpleImage(this@ProfileActivity, imgProfilePic, clsProfileDashboardResponse.data?.userImage.toString())

                            txtCoins.text = changeCoinsFormat(clsProfileDashboardResponse.data?.totalCoins.toString())

                            if (clsProfileDashboardResponse.data?.profit?.toInt() ?: 0 > 0)
                                txtCoinsProfit.text = "+${clsProfileDashboardResponse.data?.profit}"
                            else
                                txtCoinsProfit.text = "0"

                            txtCoinsWagered.text = clsProfileDashboardResponse.data?.wagered
                            txtCoinsPlayedGames.text = clsProfileDashboardResponse.data?.playedGames
                            txtCoinsRankingByLevelValue.text = clsProfileDashboardResponse.data?.RankingByLevelPostion.toString()
                            txtCoinsRankingByProfitValue.text = clsProfileDashboardResponse.data?.rankingByProfitPosition.toString()


                            if (!isOtherUser) {
                                val clsGuestRandomNumberResponse: ClsGuestRandomNumberResponse.Data? = getUserDetails()
                                clsGuestRandomNumberResponse?.guestNumber = clsProfileDashboardResponse.data?.guestNumber.toString()
                                clsGuestRandomNumberResponse?.userID = clsProfileDashboardResponse.data?.userID.toString()
                                clsGuestRandomNumberResponse?.userName = clsProfileDashboardResponse.data?.userName.toString()
                                clsGuestRandomNumberResponse?.userImage = clsProfileDashboardResponse.data?.userImage.toString()
                                clsGuestRandomNumberResponse?.email = clsProfileDashboardResponse.data?.email.toString()
                                clsGuestRandomNumberResponse?.isRegister = 1
                                clsGuestRandomNumberResponse?.isBlock = clsProfileDashboardResponse.data?.isBlock ?: 0
                                clsGuestRandomNumberResponse?.totalXP = clsProfileDashboardResponse.data?.totalXP?.toInt() ?: 0
                                clsGuestRandomNumberResponse?.totalCoins = clsProfileDashboardResponse.data?.totalCoins?.toInt() ?: 0
                                clsGuestRandomNumberResponse?.profit = clsProfileDashboardResponse.data?.profit?.toInt() ?: 0
                                clsGuestRandomNumberResponse?.wagered = clsProfileDashboardResponse.data?.wagered?.toInt() ?: 0
                                clsGuestRandomNumberResponse?.playedGames = clsProfileDashboardResponse.data?.playedGames?.toInt() ?: 0
                                clsGuestRandomNumberResponse?.rankingByLevel = clsProfileDashboardResponse.data?.rankingByLevel?.toInt() ?: 0
                                clsGuestRandomNumberResponse?.rankingByProfit = clsProfileDashboardResponse.data?.rankingByProfit?.toInt() ?: 0
                                clsGuestRandomNumberResponse?.lastReadId = clsProfileDashboardResponse.data?.lastReadId ?: 0
                                CrashPotApplication.instance.preferenceData?.setValue(PREF_USER_DETAIL, Gson().toJson(clsGuestRandomNumberResponse))

                                CrashPotApplication.instance.preferenceData?.setValue(PREF_DASHBOARD_USER_DETAIL, Gson().toJson(clsProfileDashboardResponse.data))
                            }
                        }
                    }
                }

                override fun apiFailure(errorMessage: String) {
                    showToast(this@ProfileActivity, errorMessage)
                }

                override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                    val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                    showToast(this@ProfileActivity, errorMessage)
                }
            })
    }

    private fun getUserProfile(userId: String) {
        ApiCallMethods(this@ProfileActivity).getUserProfile(
            userId,
            object : OnApiCallCompleted<ClsGetUserProfileResponse> {
                override fun apiSuccess(obj: Any?) {
                    if (obj != null) {
                        val clsGetUserProfileResponse: ClsGetUserProfileResponse = obj as ClsGetUserProfileResponse

                        if (clsGetUserProfileResponse.success) {
                            txtUserName.text = clsGetUserProfileResponse.data?.userName
                            txtStarCount.text = clsGetUserProfileResponse.data?.rankingByLevel

                            val totalXpPoints: Int = clsGetUserProfileResponse.data?.remainXP ?: 0
                            progressBar1.progress = totalXpPoints

                            if (this@ProfileActivity != null && imgProfilePic != null)
                                Utility.setSimpleImage(this@ProfileActivity, imgProfilePic, clsGetUserProfileResponse.data?.userImage.toString())

                            txtCoins.text = changeCoinsFormat(clsGetUserProfileResponse.data?.totalCoins.toString())

                            txtCoinsProfit.text = "+${clsGetUserProfileResponse.data?.profit}"
                            txtCoinsWagered.text = clsGetUserProfileResponse.data?.wagered
                            txtCoinsPlayedGames.text = clsGetUserProfileResponse.data?.playedGames
                            txtCoinsRankingByLevelValue.text = clsGetUserProfileResponse.data?.RankingByLevelPostion.toString()
                            txtCoinsRankingByProfitValue.text = clsGetUserProfileResponse.data?.rankingByProfitPosition.toString()
                        }
                    }
                }

                override fun apiFailure(errorMessage: String) {
                    showToast(this@ProfileActivity, errorMessage)
                }

                override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                    val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                    showToast(this@ProfileActivity, errorMessage)
                }
            })
    }

    private fun logout(userId: String) {
        ApiCallMethods(this@ProfileActivity).logout(
            userId, object : OnApiCallCompleted<ClsLogOutResponse> {
                override fun apiSuccess(obj: Any?) {
                    if (obj != null) {
                        val clsLogOutResponse: ClsLogOutResponse = obj as ClsLogOutResponse
                        if (clsLogOutResponse.success) {
                            generateGuestRandomNumber()
                        }
                    }
                }

                override fun apiFailure(errorMessage: String) {
                    showToast(this@ProfileActivity, errorMessage)
                }

                override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                    val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                    showToast(this@ProfileActivity, errorMessage)
                }
            })
    }

    private fun logoutAndNewUserLogin(
        name: String,
        avatar: String,
        email: String,
        socialMediaType: String,
        socialMediaId: String,
        userID: String,
        is_register: String?,
        oldUserId: String?
    ) {
        ApiCallMethods(this@ProfileActivity).logout(
            oldUserId.toString(), object : OnApiCallCompleted<ClsLogOutResponse> {
                override fun apiSuccess(obj: Any?) {
                    if (obj != null) {
                        val clsLogOutResponse: ClsLogOutResponse = obj as ClsLogOutResponse
                        if (clsLogOutResponse.success) {
                            login(name, avatar, email, socialMediaType, socialMediaId, userID, is_register, oldUserId)
                        }
                    }
                }

                override fun apiFailure(errorMessage: String) {
                    showToast(this@ProfileActivity, errorMessage)
                }

                override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                    val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                    showToast(this@ProfileActivity, errorMessage)
                }
            })
    }

    private fun isUserRegistered(
        name: String,
        avatar: String,
        email: String,
        socialMediaType: String,
        socialMediaId: String,
        userID: String
    ) {
        ApiCallMethods(this@ProfileActivity).isUserRegistered(
            socialMediaId, object : OnApiCallCompleted<ClsIsUserRegisteredResponse> {
                override fun apiSuccess(obj: Any?) {
                    if (obj != null) {
                        val clsIsUserRegisteredResponse: ClsIsUserRegisteredResponse = obj as ClsIsUserRegisteredResponse
                        if (clsIsUserRegisteredResponse.data?.isRegister == 1) {

                            loadDataDialog(
                                name, avatar, email, socialMediaType, socialMediaId, userID,
                                clsIsUserRegisteredResponse.data?.isRegister.toString(), clsIsUserRegisteredResponse.data?.oldUserId.toString()
                            )
                        } else {
                            login(
                                name, avatar, email, socialMediaType, socialMediaId, userID,
                                clsIsUserRegisteredResponse.data?.isRegister.toString(), clsIsUserRegisteredResponse.data?.oldUserId.toString()
                            )
                        }
                    }
                }

                override fun apiFailure(errorMessage: String) {
                    showToast(this@ProfileActivity, errorMessage)
                }

                override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                    val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                    showToast(this@ProfileActivity, errorMessage)
                }
            })
    }

    private fun generateGuestRandomNumber() {
        ApiCallMethods(this@ProfileActivity)
            .generateGuestRandomNumber(object : OnApiCallCompleted<ClsGuestRandomNumberResponse> {
                override fun apiSuccess(obj: Any?) {
                    if (obj != null) {
                        val clsGuestRandomNumberResponse: ClsGuestRandomNumberResponse =
                            obj as ClsGuestRandomNumberResponse

                        if (clsGuestRandomNumberResponse.data != null &&
                            !TextUtils.isEmpty(clsGuestRandomNumberResponse.data!!.guestNumber)
                        ) {
                            CrashPotApplication.instance.preferenceData?.setValueBoolean(PREF_IS_ACCEPT_TOS, true)
                            CrashPotApplication.instance.preferenceData?.setValueBoolean(PREF_IS_GUEST_USER, true)
                            CrashPotApplication.instance.preferenceData?.setValueBoolean(PREF_IS_USER_LOGGED_IN, false)
                            CrashPotApplication.instance.preferenceData?.setValue(PREF_USER_DETAIL, Gson().toJson(clsGuestRandomNumberResponse.data))
                            CrashPotApplication.instance.preferenceData?.setValueInt(PREF_USER_LEVEL, clsGuestRandomNumberResponse.data?.rankingByLevel!!)

                            val intent = Intent(this@ProfileActivity, DashboardActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                            overridePendingTransition(0, 0);
                        }
                    }
                }

                override fun apiFailure(errorMessage: String) {
                    showToast(this@ProfileActivity, errorMessage)
                }

                override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                    val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                    showToast(this@ProfileActivity, errorMessage)
                }
            })
    }
}
