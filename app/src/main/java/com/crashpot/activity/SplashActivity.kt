package com.crashpot.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import com.crashpot.CrashPotApplication
import com.crashpot.dialog.TermsAndConditionDialog
import com.crashpot.interfaces.OnCallBack
import com.crashpot.model.ClsGuestRandomNumberResponse
import com.crashpot.network.ApiCallMethods
import com.crashpot.network.OnApiCallCompleted
import com.crashpot.util.*
import com.crashpot.worker.OneDayNotificationWorker
import com.google.gson.Gson
import org.json.JSONObject
import java.util.concurrent.TimeUnit


class SplashActivity : AppCompatActivity() {
    var androidId: String = ""
    private lateinit var termsAndConditionDialog: TermsAndConditionDialog

    @SuppressLint("HardwareIds")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        androidId = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
        CrashPotApplication.instance.preferenceData?.setValue(PREF_IMEI, androidId)

        CrashPotApplication.instance.preferenceData?.setValueLong(PREF_TIMER_APP_OPEN, (System.currentTimeMillis() + MILLISECONDS_24_HOURS))
        setOneDayWorker()

        Handler().postDelayed({
            if (CrashPotApplication.instance.preferenceData?.getValueBooleanFromKey(PREF_IS_ACCEPT_TOS)!!) {
                startActivity(
                    Intent(
                        this@SplashActivity,
                        DashboardActivity::class.java
                    )
                )
                finish()
            } else
                manageTosDialog()
        }, SPLASH_TIME_OUT)
    }

    fun setOneDayWorker() {
        val oneDayNotificationWork = PeriodicWorkRequest.Builder(OneDayNotificationWorker::class.java, 15, TimeUnit.MINUTES)
            .build()

        val workManager = WorkManager.getInstance(this)
        workManager.enqueue(oneDayNotificationWork)
    }

    /**
     * Manage ToS Dialog at splash screen
     */
    private fun manageTosDialog() {
        termsAndConditionDialog = TermsAndConditionDialog(this@SplashActivity,
            object : OnCallBack {
                override fun onCallBackReturn(stringValue: String) {
                    generateGuestRandomNumber()
                }
            })
        termsAndConditionDialog.setCancelable(false)
        termsAndConditionDialog.show()
    }

    private fun generateGuestRandomNumber() {
        ApiCallMethods(this@SplashActivity)
            .generateGuestRandomNumber(object : OnApiCallCompleted<ClsGuestRandomNumberResponse> {
                override fun apiSuccess(obj: Any?) {
                    if (obj != null) {
                        val clsGuestRandomNumberResponse: ClsGuestRandomNumberResponse =
                            obj as ClsGuestRandomNumberResponse

                        if (clsGuestRandomNumberResponse.data != null &&
                            !TextUtils.isEmpty(clsGuestRandomNumberResponse.data!!.guestNumber)
                        ) {

                            CrashPotApplication.instance.preferenceData?.setValueBoolean(PREF_IS_ACCEPT_TOS, true)
                            CrashPotApplication.instance.preferenceData?.setValueBoolean(PREF_IS_GUEST_USER, true)
                            CrashPotApplication.instance.preferenceData?.setValue(PREF_USER_DETAIL, Gson().toJson(clsGuestRandomNumberResponse.data))
                            CrashPotApplication.instance.preferenceData?.setValueInt(PREF_USER_LEVEL, clsGuestRandomNumberResponse.data?.rankingByLevel!!)

                            if (::termsAndConditionDialog.isInitialized)
                                termsAndConditionDialog.dismiss()

                            startActivity(Intent(this@SplashActivity, DashboardActivity::class.java))
                            finish()
                        }
                    }
                }

                override fun apiFailure(errorMessage: String) {
                    Utility.showToast(this@SplashActivity, errorMessage)
                }

                override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                    val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                    Utility.showToast(this@SplashActivity, errorMessage)
                }
            })
    }
}
