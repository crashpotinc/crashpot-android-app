package com.crashpot.activity

import android.view.View
import com.crashpot.R
import com.crashpot.adapter.NotificationListAdapter
import com.crashpot.base.BaseActivity
import com.crashpot.model.ClsNotificationListResponse
import com.crashpot.network.ApiCallMethods
import com.crashpot.network.OnApiCallCompleted
import com.crashpot.util.MESSAGE_SMALL
import com.crashpot.util.Utility
import com.crashpot.interfaces.ItemClickCallback
import kotlinx.android.synthetic.main.activity_notification.*
import org.json.JSONObject

class NotificationActivity : BaseActivity(), ItemClickCallback<ClsNotificationListResponse.Data> {

    private var clsNotificationListResponse: ArrayList<ClsNotificationListResponse.Data> = ArrayList()

    override fun getLayoutResId(): Int = R.layout.activity_notification

    override fun initViews() {
        getNotificationList()
    }

    override fun setListeners() {
        imgBack.setOnClickListener { onBackPressed() }
    }

    private fun setAdapter(clsNotificationListResponse: ClsNotificationListResponse) {
        this.clsNotificationListResponse = clsNotificationListResponse.data as ArrayList<ClsNotificationListResponse.Data>
        rvNotificationList.adapter = NotificationListAdapter(this.clsNotificationListResponse, this)
    }

    override fun onItemClick(view: View, selectedItem: ClsNotificationListResponse.Data, position: Int) {

    }

    // API Call
    private fun getNotificationList() {
        ApiCallMethods(this@NotificationActivity).getNotificationList(Utility.getDashboardUserDetails()?.userID.toString(),
            object : OnApiCallCompleted<ClsNotificationListResponse> {
                override fun apiSuccess(obj: Any?) {
                    if (obj != null) {
                        val clsNotificationListResponse: ClsNotificationListResponse = obj as ClsNotificationListResponse
                        if (clsNotificationListResponse.data != null && clsNotificationListResponse.data?.size ?: 0 > 0
                        ) {
                            setAdapter(clsNotificationListResponse)
                        }
                    }
                }

                override fun apiFailure(errorMessage: String) {
                    Utility.showToast(this@NotificationActivity, errorMessage)
                }

                override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                    val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                    Utility.showToast(this@NotificationActivity, errorMessage)
                }
            })
    }
}
