package com.crashpot.network

import com.crashpot.model.*
import retrofit2.Call
import retrofit2.http.*


interface ApiMethods {

    @GET("get/terms-conditions")
    fun getTermsAndCondition(): Call<ClsTermsAndConditionResponse?>

    @GET("get/privacy-policy")
    fun getPrivacyPolicy(): Call<ClsPrivacyPolicyResponse?>

    @FormUrlEncoded
    @POST("guest-random-number")
    fun generateGuestRandomNumber(
        @Field("deviceType") deviceType: Int,
        @Field("deviceToken") deviceToken: String?,
        @Field("IMEI") IMEI: String?
    ): Call<ClsGuestRandomNumberResponse?>

    @FormUrlEncoded
    @POST("login")
    fun login(
        @Field("name") name: String?,
        @Field("avatar") avatar: String?,
        @Field("email") email: String?,
        @Field("socialMediaType") socialMediaType: String?,
        @Field("socialMediaId") socialMediaId: String?,
        @Field("deviceType") deviceType: Int?,
        @Field("deviceToken") deviceToken: String?,
        @Field("IMEI") IMEI: String?,
        @Field("userID") userID: String?,
        @Field("is_register") is_register: String?,
        @Field("oldUserId") oldUserId: String?
    ): Call<ClsLoginResponse?>

    @FormUrlEncoded
    @POST("add/user-coins")
    fun addUserCoins(
        @Field("userId") userId: String?,  // To user id
        @Field("coins") coins: String?,
        @Field("gameType") gameType: String?,
        @Field("status") status: String?,
        @Field("fromUserId") fromUserId: String?, // From user id
        @Field("is_xp_or_coin") isXpOrCoin: String?,
        @Field("IMEI") IMEI: String?
    ): Call<ClsAddCoinsResponse?>

    @PUT("update/user-name")
    fun updateUserName(
        @Query("userId") userId: String?,
        @Query("userName") userName: String?,
        @Query("IMEI") IMEI: String?
    ): Call<ClsUpdateUserNameResponse?>


    @GET("dashboard")
    fun getProfileDashboardData(
        @Query("userId") userId: String?,
        @Query("IMEI") IMEI: String?
    ): Call<ClsProfileDashboardResponse?>

    @GET("get/user-profile")
    fun getUserProfile(
        @Query("userId") userId: String?,
        @Query("IMEI") IMEI: String?
    ): Call<ClsGetUserProfileResponse?>

    @GET("chat-history")
    fun getChatHistory(
        @Query("userId") userId: String?,
        @Query("last_read_id") last_read_id: String?,
        @Query("pgn") pgn: Int?,
        @Query("IMEI") IMEI: String?
    ): Call<ClsGetChatHistoryResponse?>

    @FormUrlEncoded
    @POST("report-user")
    fun reportUser(
        @Field("userId") userId: String,
        @Field("reportUserId") reportUserId: String?,
        @Field("chatMessage") chatMessage: String?,
        @Field("reportType") reportType: String?, // reportType 1 = mute and report and reportType 2 = mute only
        @Field("IMEI") IMEI: String?
    ): Call<ClsReportUserResponse?>

    @GET("user-by-level")
    fun getUserByLevel(
        @Query("levelType") levelType: String?,// 1 for ranking By Profit & 2 for ranking By Level
        @Query("userId") userId: String?,
        @Query("IMEI") IMEI: String?
    ): Call<ClsUserByLevelResponse?>

    @GET("logout")
    fun logout(
        @Query("userId") userId: String?,
        @Query("IMEI") IMEI: String?
    ): Call<ClsLogOutResponse?>

    @GET("notification")
    fun getNotificationList(
        @Query("userId") userId: String?,
        @Query("IMEI") IMEI: String?
    ): Call<ClsNotificationListResponse?>

    @GET("duplicatelogin")
    fun isUserRegistered(
        @Query("socialMediaId") socialMediaId: String?,
        @Query("IMEI") IMEI: String?
    ): Call<ClsIsUserRegisteredResponse?>
}