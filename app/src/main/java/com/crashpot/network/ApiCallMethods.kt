package com.crashpot.network

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import com.crashpot.CrashPotApplication
import com.crashpot.R
import com.crashpot.activity.DashboardActivity
import com.crashpot.model.*
import com.crashpot.util.*
import com.crashpot.util.ProgressBarUtils.Companion.cancelProgress
import com.crashpot.util.ProgressBarUtils.Companion.showProgress
import com.crashpot.util.Utility.Companion.getFcmToken
import com.crashpot.util.Utility.Companion.getImeiNumber
import com.google.gson.Gson
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class ApiCallMethods(private var mContext: Context) {

    private fun checkResponseCodes(
        response: Response<*>,
        onApiCallCompleted: OnApiCallCompleted<*>
    ): Boolean {
        try {
            return if (response.code() == RESPONSE_CODE_SUCCESS_200 ||
                response.code() == RESPONSE_CODE_SUCCESS_201
            ) {
                if (response.body() != null) {
                    true
                } else {
                    onApiCallCompleted.apiFailure(mContext.getString(R.string.msg_server_error))
                    false
                }
            } else {
                val errorBody = response.errorBody()
                if (errorBody != null) {
                    val jsonObject = JSONObject(errorBody.string())
                    if (response.code() == RESPONSE_CODE_402) {
//                        onApiCallCompleted.apiFailure(jsonObject.getString(MESSAGE_SMALL))
                        generateGuestRandomNumber()
                    } else {
                        onApiCallCompleted.apiFailureWithCode(jsonObject, response.code())
                    }
                } else {
                    onApiCallCompleted.apiFailure(mContext.getString(R.string.msg_server_error))
                }
                false
            }
        } catch (e: JSONException) {
            e.printStackTrace()
            onApiCallCompleted.apiFailure(mContext.getString(R.string.msg_server_error))
        } catch (e: IOException) {
            e.printStackTrace()
            onApiCallCompleted.apiFailure(mContext.getString(R.string.msg_server_error))
        }
        return false
    }

    //API CALL
    fun getTermsAndCondition(onApiCallCompleted: OnApiCallCompleted<ClsTermsAndConditionResponse>) {
        try {
            if (!Utility.isNetworkConnected(mContext)) {
                onApiCallCompleted.apiFailure(mContext.getString(R.string.msg_network_error))
                return
            }
            showProgress(mContext)
            val call: Call<ClsTermsAndConditionResponse?> =
                ApiClient.getRestApiMethods(BASE_URL).getTermsAndCondition()

            call.enqueue(object : Callback<ClsTermsAndConditionResponse?> {
                override fun onResponse(
                    call: Call<ClsTermsAndConditionResponse?>,
                    response: Response<ClsTermsAndConditionResponse?>
                ) {
                    cancelProgress()
                    if (checkResponseCodes(response, onApiCallCompleted)) {
                        val body: ClsTermsAndConditionResponse? = response.body()
                        onApiCallCompleted.apiSuccess(body)
                    }
                }

                override fun onFailure(call: Call<ClsTermsAndConditionResponse?>, t: Throwable) {
                    cancelProgress()
                    onApiCallCompleted.apiFailure(t.message.toString())
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getPrivacyPolicy(onApiCallCompleted: OnApiCallCompleted<ClsTermsAndConditionResponse>) {
        try {
            if (!Utility.isNetworkConnected(mContext)) {
                onApiCallCompleted.apiFailure(mContext.getString(R.string.msg_network_error))
                return
            }
            showProgress(mContext)
            val call: Call<ClsPrivacyPolicyResponse?> =
                ApiClient.getRestApiMethods(BASE_URL).getPrivacyPolicy()

            call.enqueue(object : Callback<ClsPrivacyPolicyResponse?> {
                override fun onResponse(
                    call: Call<ClsPrivacyPolicyResponse?>,
                    response: Response<ClsPrivacyPolicyResponse?>
                ) {
                    cancelProgress()
                    if (checkResponseCodes(response, onApiCallCompleted)) {
                        val body: ClsPrivacyPolicyResponse? = response.body()
                        onApiCallCompleted.apiSuccess(body)
                    }
                }

                override fun onFailure(call: Call<ClsPrivacyPolicyResponse?>, t: Throwable) {
                    cancelProgress()
                    onApiCallCompleted.apiFailure(t.message.toString())
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun generateGuestRandomNumber(onApiCallCompleted: OnApiCallCompleted<ClsGuestRandomNumberResponse>) {
        try {
            if (!Utility.isNetworkConnected(mContext)) {
                onApiCallCompleted.apiFailure(mContext.getString(R.string.msg_network_error))
                return
            }
            showProgress(mContext)
            val call: Call<ClsGuestRandomNumberResponse?> =
                ApiClient.getRestApiMethods(BASE_URL)
                    .generateGuestRandomNumber(
                        DEVICE_TYPE,
                        getFcmToken(),
                        getImeiNumber()
                    )

            call.enqueue(object : Callback<ClsGuestRandomNumberResponse?> {
                override fun onResponse(
                    call: Call<ClsGuestRandomNumberResponse?>,
                    response: Response<ClsGuestRandomNumberResponse?>
                ) {
                    cancelProgress()
                    if (checkResponseCodes(response, onApiCallCompleted)) {
                        val body: ClsGuestRandomNumberResponse? = response.body()
                        onApiCallCompleted.apiSuccess(body)
                    }
                }

                override fun onFailure(call: Call<ClsGuestRandomNumberResponse?>, t: Throwable) {
                    cancelProgress()
                    onApiCallCompleted.apiFailure(t.message.toString())
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun login(
        name: String,
        avatar: String,
        email: String,
        socialMediaType: String,
        socialMediaId: String,
        userID: String,
        is_register: String?,
        oldUserId: String?,
        onApiCallCompleted: OnApiCallCompleted<ClsLoginResponse>
    ) {
        try {
            if (!Utility.isNetworkConnected(mContext)) {
                onApiCallCompleted.apiFailure(mContext.getString(R.string.msg_network_error))
                return
            }
            showProgress(mContext)
            val call: Call<ClsLoginResponse?> =
                ApiClient.getRestApiMethods(BASE_URL)
                    .login(
//                        getAccessToken(),
//                        HEADER_ACCEPT_VALUE,
                        name,
                        avatar,
                        email,
                        socialMediaType,
                        socialMediaId,
                        DEVICE_TYPE,
                        getFcmToken(),
                        getImeiNumber(),
                        userID,
                        is_register,
                        oldUserId
                    )

            call.enqueue(object : Callback<ClsLoginResponse?> {
                override fun onResponse(
                    call: Call<ClsLoginResponse?>,
                    response: Response<ClsLoginResponse?>
                ) {
                    cancelProgress()
                    if (checkResponseCodes(response, onApiCallCompleted)) {
                        val body: ClsLoginResponse? = response.body()
                        onApiCallCompleted.apiSuccess(body)
                    }
                }

                override fun onFailure(call: Call<ClsLoginResponse?>, t: Throwable) {
                    cancelProgress()
                    onApiCallCompleted.apiFailure(t.message.toString())
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun addUserCoins(
        userId: String,
        coins: String,
        gameType: String,
        status: String,
        fromUserId: String,
        isXpOrCoin: String,
        onApiCallCompleted: OnApiCallCompleted<ClsAddCoinsResponse>
    ) {
        try {
            if (!Utility.isNetworkConnected(mContext)) {
                onApiCallCompleted.apiFailure(mContext.getString(R.string.msg_network_error))
                return
            }
            //            showProgress(mContext)
            val call: Call<ClsAddCoinsResponse?> =
                ApiClient.getRestApiMethods(BASE_URL)
                    .addUserCoins(
//                        getAccessToken(),
//                        HEADER_ACCEPT_VALUE,
                        userId,
                        coins,
                        gameType,
                        status,
                        fromUserId,
                        isXpOrCoin,
                        getImeiNumber()
                    )

            call.enqueue(object : Callback<ClsAddCoinsResponse?> {
                override fun onResponse(
                    call: Call<ClsAddCoinsResponse?>,
                    response: Response<ClsAddCoinsResponse?>
                ) {
                    cancelProgress()
                    if (checkResponseCodes(response, onApiCallCompleted)) {
                        val body: ClsAddCoinsResponse? = response.body()
                        onApiCallCompleted.apiSuccess(body)
                    }
                }

                override fun onFailure(call: Call<ClsAddCoinsResponse?>, t: Throwable) {
                    cancelProgress()
                    onApiCallCompleted.apiFailure(t.message.toString())
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun updateUserName(
        userId: String,
        userName: String,
        onApiCallCompleted: OnApiCallCompleted<ClsUpdateUserNameResponse>
    ) {
        try {
            if (!Utility.isNetworkConnected(mContext)) {
                onApiCallCompleted.apiFailure(mContext.getString(R.string.msg_network_error))
                return
            }
            showProgress(mContext)
            val call: Call<ClsUpdateUserNameResponse?> =
                ApiClient.getRestApiMethods(BASE_URL)
                    .updateUserName(
//                        getAccessToken(),
//                        HEADER_ACCEPT_VALUE,
                        userId,
                        userName,
                        getImeiNumber()
                    )

            call.enqueue(object : Callback<ClsUpdateUserNameResponse?> {
                override fun onResponse(
                    call: Call<ClsUpdateUserNameResponse?>,
                    response: Response<ClsUpdateUserNameResponse?>
                ) {
                    cancelProgress()
                    if (checkResponseCodes(response, onApiCallCompleted)) {
                        val body: ClsUpdateUserNameResponse? = response.body()
                        onApiCallCompleted.apiSuccess(body)
                    }
                }

                override fun onFailure(call: Call<ClsUpdateUserNameResponse?>, t: Throwable) {
                    cancelProgress()
                    onApiCallCompleted.apiFailure(t.message.toString())
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getProfileDashboardData(
        userId: String, showProgress: Boolean,
        onApiCallCompleted: OnApiCallCompleted<ClsProfileDashboardResponse>
    ) {
        try {
            if (!Utility.isNetworkConnected(mContext)) {
                onApiCallCompleted.apiFailure(mContext.getString(R.string.msg_network_error))
                return
            }
            if (showProgress)
                showProgress(mContext)
            val call: Call<ClsProfileDashboardResponse?> = ApiClient.getRestApiMethods(BASE_URL)
                .getProfileDashboardData(
//                    getAccessToken(),
//                    HEADER_ACCEPT_VALUE,
                    userId,
                    getImeiNumber()
                )
            call.enqueue(object : Callback<ClsProfileDashboardResponse?> {
                override fun onResponse(
                    call: Call<ClsProfileDashboardResponse?>,
                    response: Response<ClsProfileDashboardResponse?>
                ) {
                    cancelProgress()
                    if (checkResponseCodes(response, onApiCallCompleted)) {
                        val body: ClsProfileDashboardResponse? = response.body()
                        onApiCallCompleted.apiSuccess(body)
                    }
                }

                override fun onFailure(call: Call<ClsProfileDashboardResponse?>, t: Throwable) {
                    cancelProgress()
                    onApiCallCompleted.apiFailure(t.message.toString())
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getUserProfile(
        userId: String,
        onApiCallCompleted: OnApiCallCompleted<ClsGetUserProfileResponse>
    ) {
        try {
            if (!Utility.isNetworkConnected(mContext)) {
                onApiCallCompleted.apiFailure(mContext.getString(R.string.msg_network_error))
                return
            }
            showProgress(mContext)
            val call: Call<ClsGetUserProfileResponse?> =
                ApiClient.getRestApiMethods(BASE_URL).getUserProfile(
//                    getAccessToken(),
//                    HEADER_ACCEPT_VALUE,
                    userId,
                    getImeiNumber()
                )
            call.enqueue(object : Callback<ClsGetUserProfileResponse?> {
                override fun onResponse(
                    call: Call<ClsGetUserProfileResponse?>,
                    response: Response<ClsGetUserProfileResponse?>
                ) {
                    cancelProgress()
                    if (checkResponseCodes(response, onApiCallCompleted)) {
                        val body: ClsGetUserProfileResponse? = response.body()
                        onApiCallCompleted.apiSuccess(body)
                    }
                }

                override fun onFailure(call: Call<ClsGetUserProfileResponse?>, t: Throwable) {
                    cancelProgress()
                    onApiCallCompleted.apiFailure(t.message.toString())
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getChatHistory(
        userId: String,
        lastReadId: String,
        pgn: Int,
        onApiCallCompleted: OnApiCallCompleted<ClsGetChatHistoryResponse>
    ) {
        try {
            if (!Utility.isNetworkConnected(mContext)) {
                onApiCallCompleted.apiFailure(mContext.getString(R.string.msg_network_error))
                return
            }
//            showProgress(mContext)
            val call: Call<ClsGetChatHistoryResponse?> =
                ApiClient.getRestApiMethods(BASE_URL).getChatHistory(
//                    getAccessToken(),
//                    HEADER_ACCEPT_VALUE,
                    userId,
                    lastReadId,
                    pgn,
                    getImeiNumber()
                )
            call.enqueue(object : Callback<ClsGetChatHistoryResponse?> {
                override fun onResponse(
                    call: Call<ClsGetChatHistoryResponse?>,
                    response: Response<ClsGetChatHistoryResponse?>
                ) {
                    cancelProgress()
                    if (checkResponseCodes(response, onApiCallCompleted)) {
                        val body: ClsGetChatHistoryResponse? = response.body()
                        onApiCallCompleted.apiSuccess(body)
                    }
                }

                override fun onFailure(call: Call<ClsGetChatHistoryResponse?>, t: Throwable) {
                    cancelProgress()
                    onApiCallCompleted.apiFailure(t.message.toString())
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun reportUser(
        userId: String,
        reportUserId: String,
        chatMessage: String,
        reportType: String,
        onApiCallCompleted: OnApiCallCompleted<ClsReportUserResponse>
    ) {
        try {
            if (!Utility.isNetworkConnected(mContext)) {
                onApiCallCompleted.apiFailure(mContext.getString(R.string.msg_network_error))
                return
            }
            showProgress(mContext)
            val call: Call<ClsReportUserResponse?> = ApiClient.getRestApiMethods(BASE_URL).reportUser(
//                getAccessToken(),
//                HEADER_ACCEPT_VALUE,
                userId,
                reportUserId,
                chatMessage,
                reportType,
                getImeiNumber()
            )
            call.enqueue(object : Callback<ClsReportUserResponse?> {
                override fun onResponse(
                    call: Call<ClsReportUserResponse?>,
                    response: Response<ClsReportUserResponse?>
                ) {
                    cancelProgress()
                    if (checkResponseCodes(response, onApiCallCompleted)) {
                        val body: ClsReportUserResponse? = response.body()
                        onApiCallCompleted.apiSuccess(body)
                    }
                }

                override fun onFailure(call: Call<ClsReportUserResponse?>, t: Throwable) {
                    cancelProgress()
                    onApiCallCompleted.apiFailure(t.message.toString())
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getUserByLevel(
        levelType: String,
        userId: String,
        onApiCallCompleted: OnApiCallCompleted<ClsUserByLevelResponse>
    ) {
        try {
            if (!Utility.isNetworkConnected(mContext)) {
                onApiCallCompleted.apiFailure(mContext.getString(R.string.msg_network_error))
                return
            }
            showProgress(mContext)
            val call: Call<ClsUserByLevelResponse?> = ApiClient.getRestApiMethods(BASE_URL).getUserByLevel(
//                getAccessToken(),
//                HEADER_ACCEPT_VALUE,
                levelType, userId,
                getImeiNumber()
            )
            call.enqueue(object : Callback<ClsUserByLevelResponse?> {
                override fun onResponse(
                    call: Call<ClsUserByLevelResponse?>,
                    response: Response<ClsUserByLevelResponse?>
                ) {
                    cancelProgress()
                    if (checkResponseCodes(response, onApiCallCompleted)) {
                        val body: ClsUserByLevelResponse? = response.body()
                        onApiCallCompleted.apiSuccess(body)
                    }
                }

                override fun onFailure(call: Call<ClsUserByLevelResponse?>, t: Throwable) {
                    cancelProgress()
                    onApiCallCompleted.apiFailure(t.message.toString())
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun logout(
        userId: String,
        onApiCallCompleted: OnApiCallCompleted<ClsLogOutResponse>
    ) {
        try {
            if (!Utility.isNetworkConnected(mContext)) {
                onApiCallCompleted.apiFailure(mContext.getString(R.string.msg_network_error))
                return
            }
            showProgress(mContext)
            val call: Call<ClsLogOutResponse?> = ApiClient.getRestApiMethods(BASE_URL).logout(
//                getAccessToken(),
//                HEADER_ACCEPT_VALUE,
                userId,
                getImeiNumber()
            )
            call.enqueue(object : Callback<ClsLogOutResponse?> {
                override fun onResponse(
                    call: Call<ClsLogOutResponse?>,
                    response: Response<ClsLogOutResponse?>
                ) {
                    cancelProgress()
                    if (checkResponseCodes(response, onApiCallCompleted)) {
                        val body: ClsLogOutResponse? = response.body()
                        onApiCallCompleted.apiSuccess(body)
                    }
                }

                override fun onFailure(call: Call<ClsLogOutResponse?>, t: Throwable) {
                    cancelProgress()
                    onApiCallCompleted.apiFailure(t.message.toString())
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getNotificationList(
        userId: String,
        onApiCallCompleted: OnApiCallCompleted<ClsNotificationListResponse>
    ) {
        try {
            if (!Utility.isNetworkConnected(mContext)) {
                onApiCallCompleted.apiFailure(mContext.getString(R.string.msg_network_error))
                return
            }
            showProgress(mContext)
            val call: Call<ClsNotificationListResponse?> = ApiClient.getRestApiMethods(BASE_URL).getNotificationList(
//                getAccessToken(),
//                HEADER_ACCEPT_VALUE,
                userId,
                getImeiNumber()
            )
            call.enqueue(object : Callback<ClsNotificationListResponse?> {
                override fun onResponse(
                    call: Call<ClsNotificationListResponse?>,
                    response: Response<ClsNotificationListResponse?>
                ) {
                    cancelProgress()
                    if (checkResponseCodes(response, onApiCallCompleted)) {
                        val body: ClsNotificationListResponse? = response.body()
                        onApiCallCompleted.apiSuccess(body)
                    }
                }

                override fun onFailure(call: Call<ClsNotificationListResponse?>, t: Throwable) {
                    cancelProgress()
                    onApiCallCompleted.apiFailure(t.message.toString())
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun isUserRegistered(
        socialMediaId: String,
        onApiCallCompleted: OnApiCallCompleted<ClsIsUserRegisteredResponse>
    ) {
        try {
            if (!Utility.isNetworkConnected(mContext)) {
                onApiCallCompleted.apiFailure(mContext.getString(R.string.msg_network_error))
                return
            }
            showProgress(mContext)
            val call: Call<ClsIsUserRegisteredResponse?> = ApiClient.getRestApiMethods(BASE_URL).isUserRegistered(
//                getAccessToken(),
//                HEADER_ACCEPT_VALUE,
                socialMediaId,
                getImeiNumber()
            )
            call.enqueue(object : Callback<ClsIsUserRegisteredResponse?> {
                override fun onResponse(
                    call: Call<ClsIsUserRegisteredResponse?>,
                    response: Response<ClsIsUserRegisteredResponse?>
                ) {
                    cancelProgress()
                    if (checkResponseCodes(response, onApiCallCompleted)) {
                        val body: ClsIsUserRegisteredResponse? = response.body()
                        onApiCallCompleted.apiSuccess(body)
                    }
                }

                override fun onFailure(call: Call<ClsIsUserRegisteredResponse?>, t: Throwable) {
                    cancelProgress()
                    onApiCallCompleted.apiFailure(t.message.toString())
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    // GUest user data
    private fun generateGuestRandomNumber() {
        ApiCallMethods(mContext)
            .generateGuestRandomNumber(object : OnApiCallCompleted<ClsGuestRandomNumberResponse> {
                override fun apiSuccess(obj: Any?) {
                    if (obj != null) {
                        val clsGuestRandomNumberResponse: ClsGuestRandomNumberResponse =
                            obj as ClsGuestRandomNumberResponse

                        if (clsGuestRandomNumberResponse.data != null &&
                            !TextUtils.isEmpty(clsGuestRandomNumberResponse.data!!.guestNumber)
                        ) {

                            CrashPotApplication.instance.preferenceData?.setValueBoolean(PREF_IS_ACCEPT_TOS, true)
                            CrashPotApplication.instance.preferenceData?.setValueBoolean(PREF_IS_GUEST_USER, true)
                            CrashPotApplication.instance.preferenceData?.setValue(PREF_USER_DETAIL, Gson().toJson(clsGuestRandomNumberResponse.data))
                            CrashPotApplication.instance.preferenceData?.setValueInt(PREF_USER_LEVEL, clsGuestRandomNumberResponse.data?.rankingByLevel!!)

                            mContext.startActivity(Intent(mContext, DashboardActivity::class.java))
                        }
                    }
                }

                override fun apiFailure(errorMessage: String) {
                    Utility.showToast(mContext, errorMessage)
                }

                override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                    val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                    Utility.showToast(mContext, errorMessage)
                }
            })
    }
}