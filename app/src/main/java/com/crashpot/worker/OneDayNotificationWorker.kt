package com.crashpot.worker

import android.content.Context
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.crashpot.CrashPotApplication
import com.crashpot.R
import com.crashpot.util.MILLISECONDS_24_HOURS
import com.crashpot.util.NOTIFICATION_TYPE_ONE_DAY
import com.crashpot.util.NotificationUtils
import com.crashpot.util.PREF_TIMER_APP_OPEN
import java.util.concurrent.TimeUnit

class OneDayNotificationWorker(val context: Context, params: WorkerParameters) : Worker(context, params) {

    override fun doWork(): Result {

        val time: Long = CrashPotApplication.instance.preferenceData?.getValueLongFromKey(PREF_TIMER_APP_OPEN) ?: 0

        if (time < System.currentTimeMillis()) {

            NotificationUtils.instance.showLocalNotification(
                context,
                NOTIFICATION_TYPE_ONE_DAY,
                true,
                "",
                context.getString(R.string.app_name),
                "We Miss You! Please come back and play our game again soon."
            )

            CrashPotApplication.instance.preferenceData?.setValueLong(PREF_TIMER_APP_OPEN, (System.currentTimeMillis() + MILLISECONDS_24_HOURS))
            setOneDayWorker()
        }
        return Result.success()
    }

    fun setOneDayWorker() {
        val oneDayNotificationWork = PeriodicWorkRequest.Builder(OneDayNotificationWorker::class.java, 24, TimeUnit.HOURS)
            .build()
        val workManager = WorkManager.getInstance(context)
        workManager.enqueue(oneDayNotificationWork)
    }
}