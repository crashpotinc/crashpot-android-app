package com.crashpot.fragment


import android.content.Intent
import android.os.Bundle
import android.view.View
import com.crashpot.R
import com.crashpot.activity.ProfileActivity
import com.crashpot.adapter.RankingListByLevelAdapter
import com.crashpot.base.BaseFragment
import com.crashpot.model.ClsUserByLevelResponse
import com.crashpot.network.ApiCallMethods
import com.crashpot.network.OnApiCallCompleted
import com.crashpot.util.EXTRAS_USER_ID
import com.crashpot.util.MESSAGE_SMALL
import com.crashpot.util.RANKING_BY_LEVEL
import com.crashpot.util.Utility.Companion.getDashboardUserDetails
import com.crashpot.util.Utility.Companion.showToast
import com.crashpot.interfaces.ItemClickCallback
import kotlinx.android.synthetic.main.fragment_ranking_by_level.*
import org.json.JSONObject

class RankingByLevelFragment : BaseFragment(), ItemClickCallback<ClsUserByLevelResponse.Data> {

    private var clsUserByLevelResponseList: ArrayList<ClsUserByLevelResponse.Data> = ArrayList()

    override fun setContentView(): Int = R.layout.fragment_ranking_by_level

    override fun initView(rootView: View?, savedInstanceState: Bundle?) {
        getUserByLevel(RANKING_BY_LEVEL)
    }

    override fun setListeners() {
    }

    //    API CALL
    private fun getUserByLevel(levelType: String) {
        ApiCallMethods(requireActivity()).getUserByLevel(levelType, getDashboardUserDetails()?.userID.toString(),
            object : OnApiCallCompleted<ClsUserByLevelResponse> {
                override fun apiSuccess(obj: Any?) {
                    if (obj != null) {
                        val clsUserByLevelResponse: ClsUserByLevelResponse = obj as ClsUserByLevelResponse
                        if (clsUserByLevelResponse.data != null && clsUserByLevelResponse.data?.size ?: 0 > 0
                        ) {
                            setAdapter(clsUserByLevelResponse)
                        }
                    }
                }

                override fun apiFailure(errorMessage: String) {
                    showToast(requireActivity(), errorMessage)
                }

                override fun apiFailureWithCode(errorObject: JSONObject, code: Int) {
                    val errorMessage: String = errorObject.getString(MESSAGE_SMALL)
                    showToast(requireActivity(), errorMessage)
                }
            })
    }

    private fun setAdapter(clsUserByLevelResponse: ClsUserByLevelResponse) {
        clsUserByLevelResponseList = clsUserByLevelResponse.data as ArrayList<ClsUserByLevelResponse.Data>
        rvRankingByLevel.adapter = RankingListByLevelAdapter(RANKING_BY_LEVEL,clsUserByLevelResponseList, this)
    }

    override fun onItemClick(view: View, selectedItem: ClsUserByLevelResponse.Data, position: Int) {
        startActivity(
            Intent(requireActivity(), ProfileActivity::class.java)
                .putExtra(EXTRAS_USER_ID, selectedItem.userID)
        )
    }
}
