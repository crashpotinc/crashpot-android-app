package com.crashpot.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ClsUserByLevelResponse(
    @SerializedName("success") @Expose var success: Boolean = false,
    @SerializedName("status_code") @Expose var statusCode: Int = 0,
    @SerializedName("message") @Expose var message: String = "",
    @SerializedName("data") @Expose var data: List<Data>? = null
) {
    class Data(
        @SerializedName("guestNumber") @Expose var guestNumber: String = "",
        @SerializedName("userName") @Expose var userName: String = "",
        @SerializedName("userImage") @Expose var userImage: String = "",
        @SerializedName("profit") @Expose var profit: Int = 0,
        @SerializedName("wagered") @Expose var wagered: Int = 0,
        @SerializedName("playedGames") @Expose var playedGames: Int = 0,
        @SerializedName("rankingByLevel") @Expose var rankingByLevel: Int = 0,
        @SerializedName("rankingByProfit") @Expose var rankingByProfit: Int = 0,
        @SerializedName("RankingByLevelPostion") @Expose var RankingByLevelPostion: Int = 0,
        @SerializedName("rankingByProfitPosition") @Expose var rankingByProfitPosition: Int = 0,
        @SerializedName("userID") @Expose var userID: Int = 0,
        @SerializedName("remainXP") @Expose var remainXP: Int = 0

    )
}