package com.crashpot.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ClsLogOutResponse(
    @SerializedName("success") @Expose var success: Boolean = false,
    @SerializedName("status_code") @Expose var statusCode: Int? = 0,
    @SerializedName("message") @Expose var message: String? = ""
)