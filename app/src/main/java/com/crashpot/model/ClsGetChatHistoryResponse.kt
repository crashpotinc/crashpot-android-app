package com.crashpot.model

import com.crashpot.util.MESSAGE_TYPE_NORMAL
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ClsGetChatHistoryResponse(
    @SerializedName("success") @Expose var success: Boolean? = null,
    @SerializedName("status_code") @Expose var statusCode: Int? = null,
    @SerializedName("message") @Expose var message: String? = null,
    @SerializedName("data") @Expose var data: List<Data>? = null
) {
    class Data(
        @SerializedName("message") @Expose var message: String? = null,
        @SerializedName("userId") @Expose var userId: Int? = null,
        @SerializedName("name") @Expose var name: String? = null,
        @SerializedName("email") @Expose var email: String? = null,
        @SerializedName("userImage") @Expose var userImage: String? = null,
        @SerializedName("messageType") @Expose var messageType: String = MESSAGE_TYPE_NORMAL,
        @SerializedName("tagUserList") @Expose var tagUserList: String = "[]",
        @SerializedName("messageId") @Expose var messageId: Int? = null,
        @SerializedName("rankingByLevel") @Expose var rankingByLevel: String = "0",
        @SerializedName("time") @Expose var time: String? = null,
        @SerializedName("pushId") @Expose var pushId: String? = ""
    )
}