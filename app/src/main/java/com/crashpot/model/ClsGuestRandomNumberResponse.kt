package com.crashpot.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ClsGuestRandomNumberResponse(
    @SerializedName("success") @Expose var success: Boolean = false,
    @SerializedName("status_code") @Expose var statusCode: Int = 0,
    @SerializedName("message") @Expose var message: String = "",
    @SerializedName("data") @Expose var data: Data? = null
) {
    class Data(
        @SerializedName("guestNumber") @Expose var guestNumber: String = "",
        @SerializedName("userID") @Expose var userID: String = "",
        @SerializedName("userName") @Expose var userName: String = "",
        @SerializedName("userImage") @Expose var userImage: String = "",
        @SerializedName("email") @Expose var email: String = "",
        @SerializedName("is_block") @Expose var isBlock: Int = 0,
        @SerializedName("isRegister") @Expose var isRegister: Int = 0,
        @SerializedName("totalXP") @Expose var totalXP: Int = 0,
        @SerializedName("totalCoins") @Expose var totalCoins: Int = 0,
        @SerializedName("profit") @Expose var profit: Int = 0,
        @SerializedName("wagered") @Expose var wagered: Int = 0,
        @SerializedName("playedGames") @Expose var playedGames: Int = 0,
        @SerializedName("socialMediaType") @Expose var socialMediaType: String = "",
        @SerializedName("rankingByLevel") @Expose var rankingByLevel: Int = 1,
        @SerializedName("rankingByProfit") @Expose var rankingByProfit: Int = 0,
        @SerializedName("last_read_id") @Expose var lastReadId: Int = 0,
        @SerializedName("is_level_up") @Expose var isLevelUp: Int = 0,
        @SerializedName("success") @Expose var success: Success? = null
    ) {
        class Success(@SerializedName("token") @Expose var token: String = "")
    }


}