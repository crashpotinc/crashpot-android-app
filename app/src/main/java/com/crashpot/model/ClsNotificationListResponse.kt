package com.crashpot.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ClsNotificationListResponse(
    @SerializedName("success") @Expose var success: Boolean = false,
    @SerializedName("status_code") @Expose var statusCode: Int? = 0,
    @SerializedName("message") @Expose var message: String? = "",
    @SerializedName("data") @Expose var data: List<Data>? = null
) {
    class Data(
        @SerializedName("msgTitle") @Expose var msgTitle: String = "",
        @SerializedName("notificationMsg") @Expose var notificationMsg: String = "",
        @SerializedName("time") @Expose var time: String = ""
    )
}