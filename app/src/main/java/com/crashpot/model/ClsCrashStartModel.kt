package com.crashpot.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ClsCrashStartModel(
    @SerializedName("finalValue") @Expose var finalValue: String = "",
    @SerializedName("xValue") @Expose var xValue: String = "",
    @SerializedName("displayValue") @Expose var displayValue: String = "",
    @SerializedName("type") @Expose var type: Int = 0,
    @SerializedName("gameId") @Expose var gameId: Int = 0,
    @SerializedName("randomUser") @Expose var randomUser: List<RandomUser>? = null,
    @SerializedName("lastUser") @Expose var lastUser: LastUser? = null
) {

    class RandomUser(
        @SerializedName("id") @Expose var id: Int = 0,
        @SerializedName("name") @Expose var name: String = "",
        @SerializedName("email") @Expose var email: String = "",
        @SerializedName("status") @Expose var status: Int = 0,
        @SerializedName("bet_at") @Expose var betAt: String = "",
        @SerializedName("y_axis") @Expose var yAxis: String = "",
        @SerializedName("bet_amount") @Expose var betAmount: String = "",
        @SerializedName("calculate_amount") @Expose var calculateAmount: String = "",
        @SerializedName("calculate_xp") @Expose var calculate_xp: String = "",
        @SerializedName("created_at") @Expose var createdAt: String = ""
    )

    class LastUser(
        @SerializedName("id") @Expose var id: Int = 0,
        @SerializedName("name") @Expose var name: String = "",
        @SerializedName("email") @Expose var email: String = "",
        @SerializedName("status") @Expose var status: Int = 0,
        @SerializedName("bet_at") @Expose var betAt: String = "",
        @SerializedName("y_axis") @Expose var yAxis: String = "",
        @SerializedName("bet_amount") @Expose var betAmount: String = "",
        @SerializedName("calculate_amount") @Expose var calculateAmount: String = "",
        @SerializedName("calculate_xp") @Expose var calculate_xp: String = "",
        @SerializedName("created_at") @Expose var createdAt: String = ""
    )
}