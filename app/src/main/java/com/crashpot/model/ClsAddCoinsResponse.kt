package com.crashpot.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ClsAddCoinsResponse(
    @SerializedName("success") @Expose var success: Boolean = false,
    @SerializedName("status_code") @Expose var statusCode: Int? = 0,
    @SerializedName("message") @Expose var message: String? = "",
    @SerializedName("data") @Expose var data: Data? = null
) {
    class Data(
        @SerializedName("guestNumber") @Expose var guestNumber: String? = null,
        @SerializedName("userID") @Expose var userID: Int? = null,
        @SerializedName("userName") @Expose var userName: String? = null,
        @SerializedName("userImage") @Expose var userImage: String = "",
        @SerializedName("email") @Expose var email: String = "",
        @SerializedName("is_block") @Expose var isBlock: Int? = null,
        @SerializedName("totalXP") @Expose var totalXP: Int? = null,
        @SerializedName("totalCoins") @Expose var totalCoins: Int? = null,
        @SerializedName("profit") @Expose var profit: Int? = null,
        @SerializedName("wagered") @Expose var wagered: Int? = null,
        @SerializedName("playedGames") @Expose var playedGames: Int? = null,
        @SerializedName("rankingByLevel") @Expose var rankingByLevel: Int? = null,
        @SerializedName("rankingByProfit") @Expose var rankingByProfit: Int? = null,
        @SerializedName("last_read_id") @Expose var lastReadId: Int? = null,
        @SerializedName("remainXP") @Expose var remainXP: Int = 0,
        @SerializedName("is_level_up") @Expose var is_level_up: Int = 0
    )
}