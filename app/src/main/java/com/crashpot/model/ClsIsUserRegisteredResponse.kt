package com.crashpot.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ClsIsUserRegisteredResponse(
    @SerializedName("success") @Expose var success: Boolean = false,
    @SerializedName("status_code") @Expose var statusCode: Int? = 0,
    @SerializedName("message") @Expose var message: String? = "",
    @SerializedName("data") @Expose var data: Data? = null
) {

    class Data(
        @SerializedName("social_media_id")
        @Expose
        var socialMediaId: String? = null,

        @SerializedName("is_register")
        @Expose
        var isRegister: Int = 0,

        @SerializedName("oldUserId")
        @Expose
        var oldUserId: String? = ""
    )
}