package com.crashpot.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ClsProfileDashboardResponse(
    @SerializedName("success") @Expose var success: Boolean = false,
    @SerializedName("status_code") @Expose var statusCode: Int? = 0,
    @SerializedName("message") @Expose var message: String? = "",
    @SerializedName("data") @Expose var data: Data? = null
) {
    class Data(
        @SerializedName("guestNumber") @Expose var guestNumber: String = "",
        @SerializedName("userID") @Expose var userID: Int = 0,
        @SerializedName("userName") @Expose var userName: String = "",
        @SerializedName("userImage") @Expose var userImage: String = "",
        @SerializedName("email") @Expose var email: String = "",
        @SerializedName("is_block") @Expose var isBlock: Int = 0,
        @SerializedName("totalXP") @Expose var totalXP: String = "0",
        @SerializedName("totalCoins") @Expose var totalCoins: String = "0",
        @SerializedName("profit") @Expose var profit: String = "0",
        @SerializedName("wagered") @Expose var wagered: String = "0",
        @SerializedName("playedGames") @Expose var playedGames: String = "0",
        @SerializedName("rankingByLevel") @Expose var rankingByLevel: String = "0",
        @SerializedName("rankingByProfit") @Expose var rankingByProfit: String = "0",
        @SerializedName("RankingByLevelPostion") @Expose var RankingByLevelPostion: Int = 0,
        @SerializedName("rankingByProfitPosition") @Expose var rankingByProfitPosition: Int = 0,
        @SerializedName("last_read_id") @Expose var lastReadId: Int? = null,
        @SerializedName("remainXP") @Expose var remainXP: Int = 0,
        @SerializedName("notificationCNT") @Expose var notificationCNT: Int = 0,
        @SerializedName("chatCNT") @Expose var chatCNT: Int = 0,
        @SerializedName("is_level_up") @Expose var is_level_up: Int = 0,
        @SerializedName("mutedUser") @Expose var mutedUser: List<Int>? = null
    )
}