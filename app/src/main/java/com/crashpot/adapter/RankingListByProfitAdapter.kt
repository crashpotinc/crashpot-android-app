package com.crashpot.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.crashpot.R

class RankingListByProfitAdapter() : RecyclerView.Adapter<RankingListByProfitAdapter.ObjectHolder>() {

    private var context: Context? = null
    var rankingList: ArrayList<String> = ArrayList()

    override fun getItemCount(): Int = 15/*notificationList.size*/

    class ObjectHolder(view: View) : RecyclerView.ViewHolder(view) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ObjectHolder {
        context = parent.context
        return ObjectHolder(LayoutInflater.from(context).inflate(R.layout.list_item_ranking_by_profit, parent, false))
    }

    override fun onBindViewHolder(holder: ObjectHolder, position: Int) {
    }
}