package com.crashpot.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.crashpot.R
import com.crashpot.model.ClsNotificationListResponse
import com.crashpot.util.DATE_FORMAT_HH_MM
import com.crashpot.util.Utility.Companion.changeStringDateFormat
import com.crashpot.interfaces.ItemClickCallback

class NotificationListAdapter(
    private val clsNotificationListResponse: ArrayList<ClsNotificationListResponse.Data>,
    private val itemClickCallback: ItemClickCallback<ClsNotificationListResponse.Data>
) : RecyclerView.Adapter<NotificationListAdapter.ObjectHolder>() {

    private lateinit var context: Context

    override fun getItemCount(): Int = if (clsNotificationListResponse.isNullOrEmpty()) 0 else clsNotificationListResponse.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ObjectHolder {
        context = parent.context
        return ObjectHolder(LayoutInflater.from(context).inflate(R.layout.list_item_notification, parent, false))
    }

    override fun onBindViewHolder(holder: ObjectHolder, position: Int) {
        if (::context.isInitialized)
            holder.bindItems(context, clsNotificationListResponse[position], itemClickCallback)
    }

    class ObjectHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindItems(context: Context, messageItem: ClsNotificationListResponse.Data, itemClickCallback: ItemClickCallback<ClsNotificationListResponse.Data>) {

            val txtNotificationTitle: TextView = itemView.findViewById(R.id.txtNotificationTitle) as TextView
            val txtNotificationValue: TextView = itemView.findViewById(R.id.txtNotificationValue) as TextView
            val txtNotificationTime: TextView = itemView.findViewById(R.id.txtNotificationTime) as TextView

            txtNotificationTitle.text = messageItem.msgTitle
            txtNotificationValue.text = messageItem.notificationMsg
            txtNotificationTime.text = changeStringDateFormat(messageItem.time, DATE_FORMAT_HH_MM)
//            txtNotificationTime.text = messageItem.time
        }
    }
}