package com.crashpot.adapter

import android.content.Context
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.crashpot.R
import com.crashpot.interfaces.OnCallBackChat
import com.crashpot.model.ClsGetChatHistoryResponse
import com.crashpot.util.DATE_FORMAT_HH_MM
import com.crashpot.util.MESSAGE_TYPE_BROADCAST
import com.crashpot.util.MESSAGE_TYPE_NORMAL
import com.crashpot.util.Utility.Companion.changeStringDateFormat
import com.crashpot.util.Utility.Companion.getColor
import com.crashpot.util.Utility.Companion.getDashboardUserDetails
import com.crashpot.util.Utility.Companion.setCircularImage
import com.crashpot.util.Utility.Companion.showLog
import com.crashpot.interfaces.ItemClickCallback
import de.hdodenhof.circleimageview.CircleImageView
import org.json.JSONArray


class ChatMessageListAdapter(
    private val chatMessageList: ArrayList<ClsGetChatHistoryResponse.Data>,
    private val itemClickCallback: ItemClickCallback<ClsGetChatHistoryResponse.Data>,
    private val onCallBackChat: OnCallBackChat
) : RecyclerView.Adapter<ChatMessageListAdapter.ObjectHolder>() {

    private lateinit var context: Context

    override fun getItemCount(): Int = if (chatMessageList.isNullOrEmpty()) 0 else chatMessageList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ObjectHolder {
        context = parent.context
        return ObjectHolder(LayoutInflater.from(context).inflate(R.layout.list_item_chat, parent, false))
    }

    override fun onBindViewHolder(holder: ObjectHolder, position: Int) {
        if (::context.isInitialized)
            holder.bindItems(context, chatMessageList[position], itemClickCallback, onCallBackChat)
    }

    fun updateData(chatMessageList: ClsGetChatHistoryResponse.Data) {
        this.chatMessageList.add(chatMessageList)
        notifyDataSetChanged()
    }

    fun setList(messageList: ArrayList<ClsGetChatHistoryResponse.Data>) {
        this.chatMessageList.addAll(0, messageList)
        notifyDataSetChanged()
    }

    class ObjectHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindItems(context: Context, messageItem: ClsGetChatHistoryResponse.Data, itemClickCallback: ItemClickCallback<ClsGetChatHistoryResponse.Data>, onCallBackChat: OnCallBackChat) {
            val txtMessage: TextView = itemView.findViewById(R.id.txtMessage) as TextView
            val txtUserName: TextView = itemView.findViewById(R.id.txtUserName) as TextView
            val txtLevel: TextView = itemView.findViewById(R.id.txtLevel) as TextView

            val txtTagUser: TextView = itemView.findViewById(R.id.txtTagUser) as TextView
            val txtTime: TextView = itemView.findViewById(R.id.txtTime) as TextView
            val imgRainOnUser: AppCompatImageView = itemView.findViewById(R.id.imgRainOnUser) as AppCompatImageView
            val imgReportUser: AppCompatImageView = itemView.findViewById(R.id.imgReportUser) as AppCompatImageView
            val imgProfile: CircleImageView = itemView.findViewById(R.id.imgProfile) as CircleImageView
            val clOtherUSerFeature: ConstraintLayout = itemView.findViewById(R.id.clOtherUSerFeature) as ConstraintLayout
            val clUserName: ConstraintLayout = itemView.findViewById(R.id.clUserName) as ConstraintLayout

            txtMessage.text = messageItem.message

            val spannable: Spannable = SpannableString(messageItem.message)
            var isUserTagged: Boolean = false
            if (!TextUtils.isEmpty(messageItem.tagUserList) && messageItem.tagUserList != "") {
//                showLog("adapter", "String :: " + messageItem.message)

                var tagUserList: JSONArray = JSONArray(messageItem.tagUserList)
                if (tagUserList.length() > 0) {

                    for (i in 0 until tagUserList.length()) {
                        showLog("adapter", "$i word :: " + tagUserList[i])

                        if (getDashboardUserDetails()?.userName == tagUserList[i])
                            isUserTagged = true

                        var word: String = tagUserList[i].toString()
                        val str = spannable.toString()

                        if (str.contains(word)) {
                            var startPos: Int = 0
                            var endPos: Int = 0
                            startPos = str.indexOf(word) - 1
                            endPos = startPos + word.length + 1/*10 characters = in-network. */
                            spannable.setSpan(ForegroundColorSpan(getColor(context, R.color.colorGold_fcda04)), startPos, endPos, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

                            spannable.setSpan(
                                object : ClickableSpan() {
                                    override fun onClick(widget: View) {
//                                        showToast(context, "" + tagUserList[i])
                                        if (getDashboardUserDetails()?.userID.toString() != messageItem.pushId.toString())
                                            onCallBackChat.onCallBackReturn(tagUserList[i].toString(), messageItem.pushId.toString())
                                    }

                                    override fun updateDrawState(ds: TextPaint) {
                                        ds.color = getColor(context, R.color.colorGold_fcda04)
                                    }
                                }, startPos, endPos, Spanned.SPAN_INCLUSIVE_INCLUSIVE
                            )
//                            showLog("adapter", "startPos $startPos")
//                            showLog("adapter", "endPos $endPos")
                        }
                    }
                }

            }

            try {
                txtMessage.text = spannable
                txtMessage.setMovementMethod(LinkMovementMethod.getInstance());
            } catch (e: Exception) {
                showLog("Exception", "Exception :: ${e.printStackTrace()}")
            }
//            showLog("adapter", "========================================")


            txtTime.text = changeStringDateFormat(messageItem.time.toString(), DATE_FORMAT_HH_MM)
            txtUserName.text = messageItem.name
            txtLevel.text = "level ${messageItem.rankingByLevel}"

            if (getDashboardUserDetails()?.userID == messageItem.userId) {
                clOtherUSerFeature.visibility = View.GONE
            } else {
                clOtherUSerFeature.visibility = View.VISIBLE
            }

            if (isUserTagged) {
                txtMessage.setBackgroundResource(R.drawable.shape_chat_message_text_user_bg)
            } else {
                txtMessage.setBackgroundResource(R.drawable.shape_chat_message_text_bg)
            }

            if (messageItem.messageType == MESSAGE_TYPE_BROADCAST) {
                clUserName.visibility = View.GONE
                txtLevel.visibility = View.GONE
                imgProfile.setImageResource(R.drawable.ic_launcher_round_logo)
            } else {
                clUserName.visibility = View.VISIBLE
                txtLevel.visibility = View.VISIBLE
                setCircularImage(context, imgProfile, messageItem.userImage.toString())
            }

            imgRainOnUser.setOnClickListener {
                itemClickCallback.onItemClick(it, messageItem, adapterPosition)
            }
            imgReportUser.setOnClickListener {
                itemClickCallback.onItemClick(it, messageItem, adapterPosition)
            }
            txtTagUser.setOnClickListener {
                itemClickCallback.onItemClick(it, messageItem, adapterPosition)
            }
            txtUserName.setOnClickListener {
                itemClickCallback.onItemClick(it, messageItem, adapterPosition)
            }
            imgProfile.setOnClickListener {
                if (messageItem.messageType == MESSAGE_TYPE_NORMAL)
                    itemClickCallback.onItemClick(it, messageItem, adapterPosition)
            }
        }
    }
}