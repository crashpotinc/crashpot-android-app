package com.crashpot.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.crashpot.R
import com.crashpot.model.ClsUserByLevelResponse
import com.crashpot.util.RANKING_BY_LEVEL
import com.crashpot.util.Utility
import com.crashpot.interfaces.ItemClickCallback
import de.hdodenhof.circleimageview.CircleImageView

class RankingListByLevelAdapter(
    val levelType: String,
    private val clsUserByLevelResponseList: ArrayList<ClsUserByLevelResponse.Data>,
    private val itemClickCallback: ItemClickCallback<ClsUserByLevelResponse.Data>
) : RecyclerView.Adapter<RankingListByLevelAdapter.ObjectHolder>() {

    private lateinit var context: Context

    override fun getItemCount(): Int = if (clsUserByLevelResponseList.isNullOrEmpty()) 0 else clsUserByLevelResponseList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ObjectHolder {
        context = parent.context
        return ObjectHolder(LayoutInflater.from(context).inflate(R.layout.list_item_ranking_by_level, parent, false))
    }

    override fun onBindViewHolder(holder: ObjectHolder, position: Int) {
        if (::context.isInitialized)
            holder.bindItems(levelType, context, clsUserByLevelResponseList[position], itemClickCallback)
    }

    class ObjectHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindItems(levelType: String, context: Context, messageItem: ClsUserByLevelResponse.Data, itemClickCallback: ItemClickCallback<ClsUserByLevelResponse.Data>) {

            val txtPlayers: TextView = itemView.findViewById(R.id.txtPlayers) as TextView
            val txtStarCount: TextView = itemView.findViewById(R.id.txtStarCount) as TextView
            val txtPosition: TextView = itemView.findViewById(R.id.txtPosition) as TextView
            val txtProfit: TextView = itemView.findViewById(R.id.txtProfit) as TextView

            val clRankingRoot: ConstraintLayout = itemView.findViewById(R.id.clRankingRoot) as ConstraintLayout
            val clProgressBar: ConstraintLayout = itemView.findViewById(R.id.clProgressBar) as ConstraintLayout

            val imgProfile: CircleImageView = itemView.findViewById(R.id.imgProfile) as CircleImageView
            val progressBar1: ProgressBar = itemView.findViewById(R.id.progressBar1) as ProgressBar
            val imgPosition: AppCompatImageView = itemView.findViewById(R.id.imgPosition) as AppCompatImageView

            txtPlayers.text = messageItem.userName
            txtStarCount.text = messageItem.rankingByLevel.toString()
            txtPosition.text = (adapterPosition + 1).toString()
            Utility.setCircularImage(context, imgProfile, messageItem.userImage)

            progressBar1.progress = messageItem.remainXP

            if (adapterPosition == 0) {
                imgPosition.visibility = View.VISIBLE
                imgPosition.setBackgroundResource(R.drawable.ic_rank_one)
                txtPosition.setBackgroundResource(0)
                txtPosition.setTextColor(Utility.getColor(context, R.color.textPurple_24114a))
            } else if (adapterPosition == 1) {
                imgPosition.visibility = View.VISIBLE
                imgPosition.setBackgroundResource(R.drawable.ic_rank_two)
                txtPosition.setBackgroundResource(0)
                txtPosition.setTextColor(Utility.getColor(context, R.color.textPurple_24114a))
            } else if (adapterPosition == 2) {
                imgPosition.visibility = View.VISIBLE
                imgPosition.setBackgroundResource(R.drawable.ic_rank_three)
                txtPosition.setBackgroundResource(0)
                txtPosition.setTextColor(Utility.getColor(context, R.color.textPurple_24114a))
            } else {
                imgPosition.visibility = View.GONE
                txtPosition.setBackgroundResource(R.drawable.ic_round_light_purple_bg)
                txtPosition.setTextColor(Utility.getColor(context, R.color.colorWhite))
            }

            if (levelType == RANKING_BY_LEVEL) {
                txtProfit.visibility = View.GONE
                clProgressBar.visibility = View.VISIBLE
            } else {
                txtProfit.visibility = View.VISIBLE
                txtProfit.text = messageItem.profit.toString()
                clProgressBar.visibility = View.GONE
            }

            clRankingRoot.setOnClickListener {
                itemClickCallback.onItemClick(it, messageItem, adapterPosition)
            }
        }
    }
}