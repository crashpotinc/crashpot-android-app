package com.crashpot.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.text.TextUtils
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import com.crashpot.CrashPotApplication
import com.crashpot.R
import com.crashpot.interfaces.OnCallBack
import com.crashpot.util.DecimalDigitsInputFilter
import com.crashpot.util.PREF_MY_BET_VALUE
import com.crashpot.util.PREF_STRATEGY_VALUE
import com.crashpot.util.Utility.Companion.getDashboardUserDetails
import com.crashpot.util.Utility.Companion.showToast
import kotlinx.android.synthetic.main.dialog_strategy_and_amount.*


class StrategyAndAmountDialog(
    context: Context,
    val dialogType: String,
    val onCallBack: OnCallBack
) : AlertDialog(context), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_strategy_and_amount)

        if (dialogType == context.getString(R.string.my_bet)) {
            edtValue.inputType = InputType.TYPE_NUMBER_FLAG_SIGNED + InputType.TYPE_CLASS_NUMBER
            txtTitle.text = context.getString(R.string.my_bet)
            imgCoinIc.visibility = View.VISIBLE
            if (!TextUtils.isEmpty(CrashPotApplication.instance.preferenceData?.getValueFromKey(PREF_MY_BET_VALUE))) {
                edtValue.setText(CrashPotApplication.instance.preferenceData?.getValueFromKey(PREF_MY_BET_VALUE))
            } else {
                edtValue.setText("1000")
            }
        } else {
            edtValue.filters = arrayOf<InputFilter>(DecimalDigitsInputFilter(5, 2))
            txtTitle.text = context.getString(R.string.strategy)
            imgCoinIc.visibility = View.GONE
            if (!TextUtils.isEmpty(CrashPotApplication.instance.preferenceData?.getValueFromKey(PREF_STRATEGY_VALUE))) {
                edtValue.setText(CrashPotApplication.instance.preferenceData?.getValueFromKey(PREF_STRATEGY_VALUE))
            } else {
                edtValue.setText("2500")
            }
        }

        edtValue.requestFocus()
        imgNextCancel.setOnClickListener(this)
        clDialogBg.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        if (window != null) window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
        window!!.clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
        window!!.setDimAmount(0f);
        window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.imgNextCancel -> {
                if (dialogType == context.getString(R.string.my_bet)) {
                    if (checkBetValidation()) {
                        dismiss()
                        onCallBack.onCallBackReturn(edtValue.text.toString())
                    }
                } else {
                    if (checkStrategyValidation()) {
                        dismiss()
                        onCallBack.onCallBackReturn(edtValue.text.toString())
                    }
                }
            }
            R.id.clDialogBg -> {
                dismiss()
            }
        }
    }

    private fun checkStrategyValidation(): Boolean {
        if (TextUtils.isEmpty(edtValue.text.toString())) {
            showToast(context, context.getString(R.string.please_enter_value))
            return false
        }
        if (edtValue.text.toString().toDouble() < 1) {
            showToast(context, context.getString(R.string.enter_value_above_one))
            return false
        }
        return true
    }

    private fun checkBetValidation(): Boolean {
        val value: Double = edtValue.text.toString().toDouble()
        val coins: Double = getDashboardUserDetails()?.totalCoins?.toDouble() ?: 0.0

        if (TextUtils.isEmpty(edtValue.text.toString())) {
            showToast(context, context.getString(R.string.please_enter_value))
            return false
        }
        if (value > coins) {
            showToast(context, context.getString(R.string.enter_coins_below_equal_to_your_coins))
            return false
        }
        return true
    }
}