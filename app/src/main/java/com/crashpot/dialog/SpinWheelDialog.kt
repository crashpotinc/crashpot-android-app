package com.crashpot.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.DecelerateInterpolator
import android.view.animation.RotateAnimation
import androidx.appcompat.app.AlertDialog
import com.crashpot.CrashPotApplication
import com.crashpot.R
import com.crashpot.interfaces.OnSpinWheelDialogCallBack
import com.crashpot.util.ADD_COINS_COIN
import com.crashpot.util.ADD_COINS_XP
import com.crashpot.util.PREF_SPIN_WHEEL_COUNT
import com.crashpot.util.SPIN_WHEEL_DIALOG_MAX_VALUE
import com.crashpot.util.Utility.Companion.getSpinWheelCount
import com.crashpot.util.Utility.Companion.showLog
import kotlinx.android.synthetic.main.dialog_spin_wheel.*
import kotlin.random.Random

class SpinWheelDialog(context: Context, val onSpinWheelDialogCallBack: OnSpinWheelDialogCallBack) : AlertDialog(context), View.OnClickListener, Animation.AnimationListener {

    var buttonRotation: Boolean = false
    var number: Int = 8
    var degree: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_spin_wheel)

        imgClose.setOnClickListener(this)
        imgSpinButton.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        if (window != null) window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
        window!!.clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.imgClose -> {
                dismiss()
                onSpinWheelDialogCallBack.onEmptyCallBak()
            }

            R.id.imgSpinButton -> {
                imgSpinButton.isEnabled = false
                setSpinWheelCount()
                startSpin()
            }
        }
    }

    private fun setSpinWheelCount() {
        var count: Int = getSpinWheelCount()
        if (count < SPIN_WHEEL_DIALOG_MAX_VALUE) {
            count += 1
        } else {
            count = 0
        }
        showLog("@@@", "Spin count:: $count")
        CrashPotApplication.instance.preferenceData?.setValueInt(PREF_SPIN_WHEEL_COUNT, count)
    }

    private fun startSpin() {
        val random: Int = getRandomNumber() + 1800
        showLog("@@@", "Spin :: $random")

        val rotateAnimation: RotateAnimation = RotateAnimation(this.degree.toFloat(), (this.degree + random).toFloat(), 1, 0.5f, 1, 0.5f)

        this.degree = (this.degree + random.toLong()) % 360
        showLog("@@@", "Spin :: ${this.degree}")

        rotateAnimation.duration = random.toLong()
        rotateAnimation.fillAfter = true
        rotateAnimation.interpolator = DecelerateInterpolator()
        rotateAnimation.setAnimationListener(this)
        imgSpinWheel.animation = rotateAnimation
        imgSpinWheel.startAnimation(rotateAnimation)
    }

    private fun getRandomNumber(): Int {
        // todo Logic consider max xalue as 15 please change accordingly
        var random: Int = 1
        /* if (getSpinWheelCount() in 0..6) { // level 1
             random = Random.nextInt(226, 315)

         } else if (getSpinWheelCount() in 7..10) { // level 2

             random = Random.nextInt(136, 315)
         } else if (getSpinWheelCount() in 11..13) { // level 3

             random = Random.nextInt(46, 315)
         } else { // level 4

             random = Random.nextInt(360)
         }*/

        if (getSpinWheelCount() in 1..5) { // level 1
            random = Random.nextInt(46, 315)
        } else { // level 4
            random = Random.nextInt(360)
        }

        showLog("@@@", "Spin random:: $random")
        return random
    }

    override fun onAnimationRepeat(animation: Animation?) {

    }

    override fun onAnimationEnd(animation: Animation?) {
        val position = this.number.toDouble() - Math.floor(degree.toDouble() / (360.0 / this.number))
        //        showLog("@@@", "Spin Position $position")

        /*
            8 = level 4 = 360-316 = Purple 		    = 1.0   = 100,000   coin
            7 = level 1 = 315-271 = Dark Bule 	    = 2.0   = 50 	    Xp
            6 = level 1 = 270-226 = Light Bule 	    = 3.0   = 100 	    coin
            5 = level 2 = 225-181 = Dark Green	    = 4.0   = 100 	    Xp
            4 = level 2 = 180-136 = Light Green     = 5.0   = 1000 	    coin
            3 = level 3 = 135-091 = Light Orange    = 6.0   = 500 	    Xp
            2 = level 3 = 090-046 = Dark Orange     = 7.0   = 10,000    coin
            1 = level 4 = 045-000 = Pink		    = 8.0   = 1000 	    Xp      */
        when (position) {
            1.0 -> {
                showLog("@@@", "Spin Position: $position win: 100,000 coin")
                onSpinWheelDialogCallBack.onCallBakeWithSpinWheelValue("100000", ADD_COINS_COIN)
            }
            2.0 -> {
                showLog("@@@", "Spin Position: $position win: 50 Xp")
                onSpinWheelDialogCallBack.onCallBakeWithSpinWheelValue("50", ADD_COINS_XP)
            }
            3.0 -> {
                showLog("@@@", "Spin Position: $position win: 100 coin")
                onSpinWheelDialogCallBack.onCallBakeWithSpinWheelValue("100", ADD_COINS_COIN)
            }
            4.0 -> {
                showLog("@@@", "Spin Position: $position win: 100 Xp")
                onSpinWheelDialogCallBack.onCallBakeWithSpinWheelValue("100", ADD_COINS_XP)
            }
            5.0 -> {
                showLog("@@@", "Spin Position: $position win: 1000 coin")
                onSpinWheelDialogCallBack.onCallBakeWithSpinWheelValue("1000", ADD_COINS_COIN)
            }
            6.0 -> {
                showLog("@@@", "Spin Position: $position win: 500 Xp")
                onSpinWheelDialogCallBack.onCallBakeWithSpinWheelValue("500", ADD_COINS_XP)
            }
            7.0 -> {
                showLog("@@@", "Spin Position: $position win: 10,000 coin")
                onSpinWheelDialogCallBack.onCallBakeWithSpinWheelValue("10000", ADD_COINS_COIN)
            }
            8.0 -> {
                showLog("@@@", "Spin Position: $position win: 1000 Xp")
                onSpinWheelDialogCallBack.onCallBakeWithSpinWheelValue("1000", ADD_COINS_XP)
            }
        }
        buttonRotation = true
    }

    override fun onAnimationStart(animation: Animation?) {
        buttonRotation = false
    }
}