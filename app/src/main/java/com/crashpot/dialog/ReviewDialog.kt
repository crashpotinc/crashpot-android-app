package com.crashpot.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.crashpot.R
import com.crashpot.interfaces.OnReviewDialogCallBack
import kotlinx.android.synthetic.main.dialog_review.*

class ReviewDialog(context: Context, val onReviewDialogCallBack: OnReviewDialogCallBack)
    : AlertDialog(context), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_review)

        txtReviewNow.setOnClickListener(this)
        txtMayBeLater.setOnClickListener(this)
        txtDontAskAgain.setOnClickListener(this)
        txtCloseBtn.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.txtCloseBtn -> {
                onReviewDialogCallBack.onCallBackReturn("")
            }
            R.id.txtReviewNow -> {
                onReviewDialogCallBack.onReviewNowClick("")
            }
            R.id.txtMayBeLater -> {
                onReviewDialogCallBack.onMayBeLaterClick("")
            }
            R.id.txtDontAskAgain -> {
                onReviewDialogCallBack.onDontAskAgainClick("")
            }
        }
        dismiss()
    }
}