package com.crashpot.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.crashpot.R
import com.crashpot.interfaces.OnInAppPurchaseCallBack
import com.crashpot.util.PURCHASE_ID_BEGINNER_PACK
import com.crashpot.util.PURCHASE_ID_DELUX_PACK
import com.crashpot.util.PURCHASE_ID_VALUE_PACK
import kotlinx.android.synthetic.main.dialog_in_app_purchase.*

class InAppPurchaseDialog(context: Context, val onInAppPurchaseCallBack: OnInAppPurchaseCallBack) : AlertDialog(context), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_in_app_purchase)

        txtCloseBtn.setOnClickListener(this)
        clBeginnerPck.setOnClickListener(this)
        clValuePck.setOnClickListener(this)
        clDeluxePck.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.txtCloseBtn -> {
                dismiss()
            }
            R.id.clBeginnerPck -> {
                dismiss()
                onInAppPurchaseCallBack.onBeginnerPackClick(PURCHASE_ID_BEGINNER_PACK)
            }
            R.id.clValuePck -> {
                dismiss()
                onInAppPurchaseCallBack.onValuePackClick(PURCHASE_ID_VALUE_PACK)
            }
            R.id.clDeluxePck -> {
                dismiss()
                onInAppPurchaseCallBack.onDeluxPackClick(PURCHASE_ID_DELUX_PACK)
            }
        }
    }
}