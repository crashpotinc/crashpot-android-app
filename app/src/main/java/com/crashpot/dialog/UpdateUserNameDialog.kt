package com.crashpot.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import com.crashpot.R
import com.crashpot.interfaces.OnCallBack
import com.crashpot.util.Utility.Companion.showToast
import kotlinx.android.synthetic.main.dialog_strategy_and_amount.*


class UpdateUserNameDialog(context: Context,
                           val userName: String,
                           val onCallBack: OnCallBack)
    : AlertDialog(context), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_update_user_name)

        edtValue.setText(userName)
        edtValue.requestFocus()

        imgNextCancel.setOnClickListener(this)
        clDialogBg.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        if (window != null) window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
        window!!.clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
        window!!.setDimAmount(0f);
        window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.imgNextCancel -> {
                if (!TextUtils.isEmpty(edtValue.text.toString())) {
                    dismiss()
                    onCallBack.onCallBackReturn(edtValue.text.toString())
                } else {
                    showToast(context, "Please enter name")
                }
            }
            R.id.clDialogBg -> {
                dismiss()
            }
        }
    }
}