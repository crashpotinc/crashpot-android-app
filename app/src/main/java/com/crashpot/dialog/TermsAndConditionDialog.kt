package com.crashpot.dialog

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.crashpot.R
import com.crashpot.activity.TosAndPrivacyPolicyActivity
import com.crashpot.interfaces.OnCallBack
import com.crashpot.util.EXTRAS_TOS_NAME
import kotlinx.android.synthetic.main.dialog_terms_and_condition.*

class TermsAndConditionDialog(context: Context, val onCallBack: OnCallBack) : AlertDialog(context), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_terms_and_condition)

        txtTermsAndCondition.setOnClickListener(this)
        txtPrivacyPolicy.setOnClickListener(this)
        txtAcceptBtn.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun onClick(view: View?) {
        when (view?.id) {

            R.id.txtTermsAndCondition -> {
                context.startActivity(Intent(context, TosAndPrivacyPolicyActivity::class.java)
                    .putExtra(EXTRAS_TOS_NAME, context.getString(R.string.terms_of_service)))
            }

            R.id.txtPrivacyPolicy -> {
                context.startActivity(Intent(context, TosAndPrivacyPolicyActivity::class.java)
                    .putExtra(EXTRAS_TOS_NAME, context.getString(R.string.privacy_policy)))
            }

            R.id.txtAcceptBtn -> {
                onCallBack.onCallBackReturn("")
            }
        }
    }
}