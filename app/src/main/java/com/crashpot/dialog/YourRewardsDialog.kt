package com.crashpot.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.crashpot.CrashPotApplication
import com.crashpot.R
import com.crashpot.interfaces.OnShareAndRewardDialogCallBack
import com.crashpot.util.*
import com.crashpot.util.Utility.Companion.showLog
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.RequestConfiguration
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener
import kotlinx.android.synthetic.main.dialog_your_rewards.*


class YourRewardsDialog(
    val isFrom: String, var amount: String, val xpOrCoin: String, context: Context,
    val onShareAndRewardDialogCallBack: OnShareAndRewardDialogCallBack
) : AlertDialog(context), View.OnClickListener {

    // Ad mob
    private lateinit var mRewardedVideoAd: RewardedVideoAd
    var isVideoRewarded: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_your_rewards)

        // Rewarded
        MobileAds.initialize(context, context.getString(R.string.ad_mob_app_id))
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(context)

        txtCoinsValue.text = Utility.changeCoinsFormat(amount)

        if (isFrom == FROM_SPINNER_WHEEL) {
            val time: Long = CrashPotApplication.instance.preferenceData?.getValueLongFromKey(PREF_TIMER_AD_MOB)!!;
            if (time < System.currentTimeMillis()) {
                loadRewardedVideoAd()
            } else {
                clDoubleReward.visibility = View.GONE
            }
        } else {
            clDoubleReward.visibility = View.GONE
        }

        if (xpOrCoin == ADD_COINS_COIN) {
            txtCoinText.text = "coins"
            imgCoinXP.setImageResource(R.drawable.img_coin_large)
        } else {
            txtCoinText.text = "xp"
            imgCoinXP.setImageResource(R.drawable.img_star_3d)
        }

        txtCloseBtn.setOnClickListener(this)
        clFacebook.setOnClickListener(this)
        clDoubleReward.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.txtCloseBtn -> {
                dismiss()
                onShareAndRewardDialogCallBack.onCallBakeWithOneString(amount)
            }

            R.id.clDoubleReward -> {
                if (mRewardedVideoAd.isLoaded) {
                    mRewardedVideoAd.show()
                }
            }

            R.id.clFacebook -> {
                if (xpOrCoin == ADD_COINS_COIN)
                    onShareAndRewardDialogCallBack.onCallBakeWithString(txtCoinsValue.text.toString(), "coins")
                else
                    onShareAndRewardDialogCallBack.onCallBakeWithString(txtCoinsValue.text.toString(), "xp")
            }
        }
    }

    private fun loadRewardedVideoAd() {
        val conf: RequestConfiguration = RequestConfiguration.Builder().setTagForChildDirectedTreatment(RequestConfiguration.TAG_FOR_CHILD_DIRECTED_TREATMENT_TRUE).build()
        MobileAds.setRequestConfiguration(conf)

        if (!mRewardedVideoAd.isLoaded) {
            val adRequest = AdRequest.Builder()
                .addTestDevice("FE31293CFC5B36E317886BA12AF676F7") // Asus max pro M2
                .build()
            mRewardedVideoAd.loadAd(context.getString(R.string.ad_rewarded_unit_id), adRequest)
        }

        mRewardedVideoAd.rewardedVideoAdListener = object : RewardedVideoAdListener {
            override fun onRewardedVideoAdClosed() {
                showLog("Rewarded AD", "onRewardedVideoAdClosed")
                if (!isVideoRewarded)
                    loadRewardedVideoAd()
                clDoubleReward.visibility = View.GONE
            }

            override fun onRewardedVideoAdLeftApplication() {
                showLog("Rewarded AD", "onRewardedVideoAdLeftApplication")
            }

            override fun onRewardedVideoAdLoaded() {
                showLog("Rewarded AD", "onRewardedVideoAdLoaded")
                BubbleAnimationUtils.startDoubleRewardBubbleAnimation(clDoubleReward)
                clDoubleReward.visibility = View.VISIBLE
            }

            override fun onRewardedVideoAdOpened() {
                showLog("Rewarded AD", "onRewardedVideoAdOpened")
            }

            override fun onRewardedVideoCompleted() {
                showLog("Rewarded AD", "onRewardedVideoCompleted")
            }

            override fun onRewarded(reward: RewardItem?) {
//                var time: Long = CrashPotApplication.instance.preferenceData?.getValueLongFromKey(PREF_TIMER_AD_MOB)!!;
//                if (time < System.currentTimeMillis()) {
//                    CrashPotApplication.instance.preferenceData?.setValueLong(PREF_TIMER_AD_MOB, (System.currentTimeMillis() + MILLISECONDS_10_sec))
//                }

                clDoubleReward.visibility = View.GONE
                amount = (amount.toInt() * 2).toString()
                txtCoinsValue.text = Utility.changeCoinsFormat(amount)
                isVideoRewarded = true
                showLog("Rewarded AD", "onRewarded! currency: ${reward?.type} amount: ${reward?.amount}")
//                setRewardedVideoAlarm(context, (System.currentTimeMillis() + MILLISECONDS_3_HOURS))
            }

            override fun onRewardedVideoStarted() {
                showLog("Rewarded AD", "onRewardedVideoStarted")
            }

            override fun onRewardedVideoAdFailedToLoad(errorCode: Int) {
                showLog("Rewarded AD", "onRewardedVideoAdFailedToLoad: $errorCode")
            }
        }
    }
}