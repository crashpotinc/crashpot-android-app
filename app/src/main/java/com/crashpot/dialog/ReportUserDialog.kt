package com.crashpot.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import com.crashpot.R
import com.crashpot.interfaces.OnCallBack
import com.crashpot.model.ClsGetChatHistoryResponse
import com.crashpot.util.REPORT_TYPE_MUTE_AND_REPORT
import com.crashpot.util.REPORT_TYPE_REPORT_ONLY
import com.crashpot.util.Utility
import kotlinx.android.synthetic.main.dialog_report_user.*

class ReportUserDialog(context: Context, var selectedItem: ClsGetChatHistoryResponse.Data, val onCallBack: OnCallBack) : AlertDialog(context), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_report_user)

        txtUserName.text = selectedItem.name
        Utility.setCircularImage(context, imgProfile, selectedItem.userImage.toString())

        txtCloseBtn.setOnClickListener(this)
        txtReportAndMute.setOnClickListener(this)
        txtOnlyReport.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        if (window != null) window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
        window!!.clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
        window!!.setDimAmount(0.5f);
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.txtCloseBtn -> {
                onCallBack.onCallBackReturn("")
                dismiss()
            }
            R.id.txtReportAndMute -> {
                onCallBack.onCallBackReturn(REPORT_TYPE_MUTE_AND_REPORT)
            }
            R.id.txtOnlyReport -> {
                onCallBack.onCallBackReturn(REPORT_TYPE_REPORT_ONLY)
            }
        }
    }
}