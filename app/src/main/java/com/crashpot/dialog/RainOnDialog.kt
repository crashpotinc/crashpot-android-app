package com.crashpot.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import com.crashpot.R
import com.crashpot.interfaces.OnCallBack
import com.crashpot.model.ClsGetChatHistoryResponse
import com.crashpot.util.Utility
import com.crashpot.util.Utility.Companion.getDashboardUserDetails
import com.crashpot.util.Utility.Companion.showToast
import kotlinx.android.synthetic.main.dialog_rain_on.*

class RainOnDialog(context: Context, var selectedItem: ClsGetChatHistoryResponse.Data, val onCallBack: OnCallBack) : AlertDialog(context), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_rain_on)

        txtUserName.text = selectedItem.name
        Utility.setCircularImage(context, imgProfile, selectedItem.userImage.toString())

        txtSendRainBtn.setOnClickListener(this)
        txtCloseBtn.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        if (window != null) window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        window!!.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE)
        window!!.clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
        window!!.setDimAmount(0.5f);
        window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.txtSendRainBtn -> {
                if (checkValidation()) {
                    onCallBack.onCallBackReturn(edtValue.text.toString().trim())
                    dismiss()
                }
            }
            R.id.txtCloseBtn -> {
                dismiss()
            }
        }
    }

    private fun checkValidation(): Boolean {
        val coins: String = edtValue.text.toString().trim()
        if (TextUtils.isEmpty(coins)) {
            showToast(context, "Please enter coins.")
            return false
        }
        if (coins.toInt() < 500) {
            showToast(context, "Minimum coin amount should be 500 or higher.")
            return false
        }
        if (coins.toInt() > getDashboardUserDetails()?.totalCoins?.toInt() ?: 0) {
            showToast(context, "Your coins are not sufficient.")
            return false
        }
        return true
    }
}