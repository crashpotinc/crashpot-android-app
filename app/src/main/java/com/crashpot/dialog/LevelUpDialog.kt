package com.crashpot.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.crashpot.R
import com.crashpot.interfaces.OnShareDialogCallBack
import com.crashpot.util.ADD_COINS_COIN
import com.crashpot.util.Utility.Companion.changeCoinsFormat
import kotlinx.android.synthetic.main.dialog_level_up.*

class LevelUpDialog(val rankingByLevel: Int?, context: Context, val onShareDialogCallBack: OnShareDialogCallBack) : AlertDialog(context), View.OnClickListener {

    var levelUpAmount: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_level_up)

        levelUpAmount = (rankingByLevel!! * 1000)
        txtStarCount.text = rankingByLevel.toString()
        txtTotalCoins.text = changeCoinsFormat(levelUpAmount.toString())

        txtCloseBtn.setOnClickListener(this)
        clFacebook.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.txtCloseBtn -> {
                dismiss()
                onShareDialogCallBack.onEmptyCallBak()
            }

            R.id.clFacebook -> {
                onShareDialogCallBack.onCallBakeWithString(changeCoinsFormat(levelUpAmount.toString()), ADD_COINS_COIN)
            }
        }
    }
}