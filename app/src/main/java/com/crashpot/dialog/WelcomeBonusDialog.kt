package com.crashpot.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.crashpot.R
import com.crashpot.interfaces.OnCallBack
import kotlinx.android.synthetic.main.dialog_welcome_bonus.*

class WelcomeBonusDialog(context: Context, val onCallBack: OnCallBack)
    : AlertDialog(context), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_welcome_bonus)

        txtCloseBtn.setOnClickListener(this)
        clFacebook.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.txtCloseBtn -> {
                dismiss()
            }

            R.id.clFacebook -> {
                onCallBack.onCallBackReturn("")
            }
        }
    }
}