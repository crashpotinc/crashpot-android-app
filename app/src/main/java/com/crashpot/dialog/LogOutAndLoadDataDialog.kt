package com.crashpot.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.crashpot.R
import com.crashpot.interfaces.OnCallBack
import kotlinx.android.synthetic.main.dialog_logout.*

class LogOutAndLoadDataDialog(context: Context, val onCallBack: OnCallBack) : AlertDialog(context), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_logout)

        txtCancelBtn.setOnClickListener(this)
        txtLogOutBtn.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun onClick(view: View?) {
        when (view?.id) {

            R.id.txtCancelBtn -> {
                dismiss()
            }

            R.id.txtLogOutBtn -> {
                onCallBack.onCallBackReturn("")
                dismiss()
            }
        }
    }
}