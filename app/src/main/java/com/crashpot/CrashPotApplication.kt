package com.crashpot

import android.app.Activity
import android.app.Application
import android.os.Bundle
import android.util.Log
import com.crashpot.util.PREF_FCM_TOKEN
import com.crashpot.util.PreferenceData
import com.crashpot.util.Utility.Companion.showLog
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.vanniktech.emoji.EmojiManager
import com.vanniktech.emoji.ios.IosEmojiProvider

class CrashPotApplication : Application(), Application.ActivityLifecycleCallbacks {

    var preferenceData: PreferenceData? = null
        private set

    private var activityReferences = 0
    private var isActivityChangingConfigurations = false

    private var gson: Gson? = null

    companion object {
        private var sInstance: CrashPotApplication? = null

        val instance: CrashPotApplication
            @Throws(RuntimeException::class)
            get() {
                if (sInstance == null) {
                    throw RuntimeException(
                        (sInstance as CrashPotApplication)
                            .getString(R.string.no_instance_found)
                    )
                }
                return sInstance as CrashPotApplication
            }
    }

    fun getGson(): Gson {
        if (gson == null) {
            gson = GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation().create()
        }
        return gson!!
    }

    override fun onCreate() {
        super.onCreate()
        EmojiManager.install(IosEmojiProvider())

        registerActivityLifecycleCallbacks(this)

        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)

        if (sInstance == null) {
            sInstance = this
            if (preferenceData == null) {
                preferenceData = PreferenceData(this)
            }
        }

        refreshedToken()
    }

    private fun refreshedToken() {
        FirebaseInstanceId
            .getInstance()
            .instanceId
            .addOnSuccessListener { instanceIdResult: InstanceIdResult ->
                val newToken: String = instanceIdResult.token
                showLog("newToken", newToken)
                preferenceData?.setValue(PREF_FCM_TOKEN, newToken)
            }
    }

    override fun onActivityPaused(activity: Activity) {
    }

    override fun onActivityStarted(activity: Activity) {
        if (++activityReferences == 1 && !isActivityChangingConfigurations) {
            // App enters foreground
            showLog("App Status", "App enters foreground")
        }
    }

    override fun onActivityDestroyed(activity: Activity) {
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
    }

    override fun onActivityStopped(activity: Activity) {
        isActivityChangingConfigurations = activity.isChangingConfigurations();
        if (--activityReferences == 0 && !isActivityChangingConfigurations) {
            // App enters background
            showLog("App Status", "App enters background")
        }
    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
    }

    override fun onActivityResumed(activity: Activity) {
    }
}