package com.crashpot.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.crashpot.R
import com.crashpot.util.NOTIFICATION_TYPE_REWARDED_VIDEO
import com.crashpot.util.NotificationUtils
import com.crashpot.util.Utility.Companion.showLog

class AdMobBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        showLog("Receiver", "Your Rewarded video is available........!!!!")
        NotificationUtils.instance.showLocalNotification(
            context,
            NOTIFICATION_TYPE_REWARDED_VIDEO,
            true,
            "",
            context.getString(R.string.app_name),
            "Your Rewarded video is available........!!!!"
        )
    }
}